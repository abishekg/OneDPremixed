
namespace Flame{

//#define safe_zero 1.e-25   // for log operations
#define safe_zero 1.e-35   // for log operations
//#define safe_zero 1.e-10   // for log operations
#define temp_tol  0.01

namespace PhysicalConstants{

 const double R_si = 8.3144621;
 const double R_kJ = R_si/1000.0;
 const double R_cal = 1.9872041;

 const double OneAtm = 1.0e5;
 const double gamma_air = 1.4;

};

}
