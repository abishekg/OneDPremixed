/*
 * FiniteVolume.h
 *
 *  Created on: Jul 17, 2014
 *      Author: abishekg
 */

#ifndef FINITEVOLUME_H_
#define FINITEVOLUME_H_
#include "ClassDefs.h"
#include "Numerics.h"
//#include "Fluid.h"
#include "IdealGas.h"
#include "Grid.h"
#include "GridOperations.h"
#include "FaceData.h"

namespace Flame {



class FiniteVolume: public Numerics {
public:
	FiniteVolume(IdealGas* fluid,Grid* grid,GridOperations *gridops,FaceData *inlet,FaceData *outlet);
	void estimateDiffusiveFluxes(double* rho, double* u,double* T, double* W_face, double* W_k, double** Xk, double** del_Xk_Xk_face,double* mu, double* lambda, double** D_k,double** V_k, double** h_k, double** Yk);
	void estimateConvectiveFluxes(double** qn, double* e_t, double* u, double* rho,double *P, double* V_c, double** Yk);
	void Discretize(double**qn,double**rhs);
	void formRHS(double **omega_k,double **qn,double **rhs);
	virtual ~FiniteVolume();
	double** getConvFlux() const;
	double** getDiffFlux() const;
	double** getSrcTerm() const;
	double** getTmpFace() const;

private:
	double **conv_flux;
	double **diff_flux;
	double **src_term;
	double **tmp_face;

	double *interp_f;

	double *u_face;
	double *P_face;
	double *rho_face;
	double *et_face;
	double *mu_face;
    double *lambda_face;
	double *stress_face;

	//double * node_pos;
	//double * face_pos;
	//double * dx_face;
	//double * one_over_dx;
	double * one_over_node_dx;
	double * one_over_face_dx;

	int num_faces;
	int num_nodes;
	int num_species;
	int num_srcs;
	//double dx_WP;
	//double dx_PE;
	//double dx;

	Grid* grid_obj;
	GridOperations *grid_ops;
	//FluidsClass* fluid_obj;
	IdealGas* fluid_obj;
	FaceData *inletBC;
	FaceData *outletBC;

};

} /* namespace Flame */

#endif /* FINITEVOLUME_H_ */
