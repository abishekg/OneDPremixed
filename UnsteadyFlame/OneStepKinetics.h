/*
 * OneStepKinetics.h
 *
 *  Created on: Jun 14, 2014
 *      Author: abishekg
 */

#ifndef ONESTEPKINETICS_H_
#define ONESTEPKINETICS_H_

#include "Kinetics.h"

namespace Flame {

class OneStepKinetics: public Kinetics {
public:
	OneStepKinetics(int nx,int nk);
	virtual ~OneStepKinetics();
	void getProductSourceTerm_rhoYkT(double *rho,double **Yk,double *T,double *W_k);

private:
	int num_pts;
	int num_species;
};

} /* namespace Flame */

#endif /* ONESTEPKINETICS_H_ */
