/*
 * CanteraTransport.h
 *
 *  Created on: Jul 8, 2014
 *      Author: abishekg
 */

#ifndef CANTERATRANSPORT_H_
#define CANTERATRANSPORT_H_

#include "Transport.h"
#include "CanteraWrapper.h"
#include "FaceData.h"
#include "GridOperations.h"

namespace Flame {

class CanteraTransport : public Transport{
public:
	CanteraTransport(int nx,CanteraWrapper *cantera_obj);
	virtual ~CanteraTransport();
	void getTransportProperties(int num_low_T,int num_high_T,int *low_T_indices,int *high_T_indices,double* T_ptr,double *P_ptr,double **Xk_ptr,double **Yk_ptr);
	//void getTransportProperties(int num_low_T,int num_high_T,int *low_T_indices,int *high_T_indices,double* T_ptr);
	void getDiffusionProperties(GridOperations *grid_ops,double *lambda,double *rho,double *Cp,double *Le_k,double *Wk,double *W,double **del_Xk_Xk,double **Xk);
	void estimateBoundaryProperties(double *Wk,double **del_Xk_Xk,FaceData *inlet,FaceData *outlet,double *Le_k);

	void processTemperature();

	void estimateSpeciesViscosity(double T);
	void estimateSpeciesConductivity(double T);
	void estimateSpeciesDiffusivity(double T,double P,double *Yk,double *Xk);

	//void estimateSpeciesViscosityPoly(int num_low_T,int num_high_T,int *low_T_indices,int *high_T_indices,double* T_ptr);
	//void estimateSpeciesConductivityPoly(int num_low_T,int num_high_T,int *low_T_indices,int *high_T_indices,double* T_ptr);
	void estimateSpeciesMolecularDiffusivity(double *lambda,double *rho,double *Cp,double *Le_k);
	void estimateSpeciesDiffusivityVelocity(GridOperations *grid_ops,double *Wk,double *W,double **del_Xk_Xk,double **Xk);



	/*double** getDk() const;
	double** getLambdak() const;
	double** getMuK() const;
	double* getVc() const;
	double** getVk() const;
*/
private:
	/*int num_pts;
	int num_species;*/
	int num_coeffs;
	int ic_tot;

	double T_low_fit;
	double T_med_fit;
	double T_high_fit;

	double *T_prev;

	double **bin_diff_temp;

	/*
	double **lambda_k;
	double **mu_k;
	double **D_k;
	double **V_k;
	double *V_c;

	double **V_low_k;
	double **V_high_k;

	double **C_low_k;
	double **C_high_k;
*/
	inline double dot5(double * x, double * y)
	{
	    return x[0]*y[0] + x[1]*y[1] + x[2]*y[2] + x[3]*y[3] +
	           x[4]*y[4];
	}

};

} /* namespace Flame */

#endif /* CANTERATRANSPORT_H_ */
