/*
 * Numerics.h
 *
 *  Created on: Jun 11, 2014
 *      Author: abishekg
 */

#ifndef NUMERICS_H_
#define NUMERICS_H_
//#include "Fluid.h"
namespace Flame {

class Numerics {
public:
	Numerics();
	virtual ~Numerics();

	void virtual Discretize(double**qn,double**rhs) = 0;
};

} /* namespace Flame */

#endif /* NUMERICS_H_ */
