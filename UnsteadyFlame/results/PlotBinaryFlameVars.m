function [ output_args ] = PlotBinaryFlameVars( filename )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
 


    clc;
    clear all;
    close all;
    
    
linestyles = cellstr(char('-',':','-.','--','-',':','-.','--','-',':','-',':',...
'-.','--','-',':','-.','--','-',':','-.'));
 

Markers=['o','x','+','*','s','d','v','^','<','>','p','h','.',...
'+','*','o','x','^','<','h','.','>','p','s','d','v',...
'o','x','+','*','s','d','v','^','<','>','p','h','.'];
    
    
    if nargin < 1
      %filename = 'flamevars.dat';
      %filename = '/home/abishekg/simulations/OneDPremixed/test_stretch/test1/flamevars.dat';
      filename = '/home/abishekg/simulations/OneDPremixed/test_AR_phi0.6/unstetched/flamevars.dat';
      %filename = '/home/abishekg/simulations/OneDPremixed/test_AR_phi0.6/test/flamevars.dat';
      %filename = '/home/abishekg/simulations/OneDPremixed/test_diff/test_hno/flamevars.dat';
     
    end
   
    %smooth = 'off';
    smooth = 'on';
    
    %num_pts = 4096;
    %num_pts = 512;
    num_pts = 2048;
    %num_pts = 1024;
    %num_species = 3;
    num_species = 9;
    %num_species = 18;
    
    num_base = 6;
    
    num_rows = num_base+ num_species+ num_species + num_species;
    
    MarkerEdgeColors=jet(num_species);  % n is the number of different items you have
    
    
    fileID = fopen(filename);
    %A = fread(fileID,'double')
    A = fread(fileID,[num_pts num_rows],'double');
    
    %A = importdata(filename,delimiterIn,headerlinesIn);


    %get(0,'DefaultAxesColorOrder')

    figure('name','Flame Variables','units','normalized','outerposition',[0 0 1 1]);
    grid on;
        %A.colheaders{1, k}
    subplot(2,2,1);
    %plot_syms={':','--','-'};
    
    cc = hsv(num_species);
    %cc = distinguishable_colors(num_species);
    
    %plot_syms={'r','b','k'};
    %species_sym = {'H2','O2','H2O'};
    plot_syms={'-b','-g','-r','-y','-d','--','-',':','o','x'};
  %  species_sym = {'H2','H','O','O2','OH','H2O','HO2','H2O2','AR'};
    
    species_sym = {'N2','H','O2','OH','O','H2','H2O','HO2','H2O2'};
 

    
    %species_sym = {'H2','H','O','O2','OH','H2O','HO2','H2O2','N','NH','NH2','NH3','NNH','NO','NO2','N2O','HNO','N2'};

    
    %
%     for ki=num_base+1:num_base+num_species, %TODO: change later
%         %plot(A(:,1),A(:,ki));
%         %plot(A(:,1),A(:,ki),plot_syms{ki-num_base},'LineWidth',1,'LineSmoothing',smooth,'Color',cc(ki,:));
%         plot(A(:,1),A(:,ki),'LineWidth',1.0,'LineSmoothing',smooth,'Color',cc(ki-num_base,:));
%         hold on;
%     end
%     legend(species_sym);
%     grid on;
%     hold off;
    
    plot(A(:,1),A(:,2)/A(20,2),'LineWidth',1,'LineSmoothing',smooth);
    xlabel('Position(m)');
    ylabel('Density (kg/m^3)');
     grid on;
    hold off;

    subplot(2,2,2); %T
    plot(A(:,1),A(:, 4),'LineWidth',1.2,'LineSmoothing',smooth,'Color',cc(1,:));
    xlabel('Position(m)');
    ylabel('Temperature (Kelvin)');
    grid on;

    subplot(2,2,3); %u
    plot(A(:,1),A(:,3),'LineWidth',1,'LineSmoothing',smooth);
    xlabel('Position(m)');
    ylabel('Velocity (m/s)');
     grid on;

    subplot(2,2,4); %P
    plot(A(:,1),A(:,6),'LineWidth',1,'LineSmoothing',smooth);
    xlabel('Position(m)');
    ylabel('Pressure (Pa)');
     grid on;
    %plot(x,rho);
    
    figure(2);
    plot(A(:,1),A(:,2),'LineWidth',1,'LineSmoothing',smooth);
    grid on;

    figure(3);
    plot(A(:,1),A(:,5),'LineWidth',1,'LineSmoothing',smooth);
    grid on;
    
    figure(4);
    for ki=num_base+num_species+1:num_base+num_species+num_species, %TODO: change later
        %plot(A(:,1),A(:,ki));
        %plot(A(:,1),A(:,ki),plot_syms{ki-num_base-num_species},'LineWidth',1,'LineSmoothing',smooth,'Color',cc(ki,:));
        plot(A(:,1),A(:,ki),'LineWidth',1,'LineSmoothing',smooth,'Color',cc(ki-num_base-num_species,:));
        hold on;
    end
    legend(species_sym);
    grid on;
    hold off;
    
    figure(5);
    for ki=num_base+num_species+num_species+1:num_base+num_species+num_species+num_species, %TODO: change later
        %plot(A(:,1),A(:,ki));
        %plot(A(:,1),A(:,ki),plot_syms{ki-num_base-num_species-num_species},'LineWidth',1,'LineSmoothing',smooth,'Color',cc(ki,:));
        plot(A(:,1),A(:,ki),'LineWidth',1,'LineSmoothing',smooth,'Color',cc(ki-num_base-num_species-num_species,:));
        hold on;
    end
    legend(species_sym);
    grid on;
    hold off;

    
    figure(6);
    [haxes,hline1,hline2]= plotyy(A(:,1),A(:,7),A(:,1),A(:,13),'plot','plot');
    ylabel(haxes(1),'Xk - H2');
    ylabel(haxes(2),' Del(Xk)/Xk - H2');
    xlabel(haxes(2),' X');
    set(hline2,'LineStyle','-');
    %plot(A(:,1),A(:,4));
    %plot(A(:,1),A(:,13));
    
    figure(7);
    plot(A(:,1),A(:,2).*A(:,3),'LineWidth',1,'LineSmoothing',smooth);
    grid on;
    
    
    species_sym = {'H2','H','O','O2','OH','H2O','HO2','H2O2','AR'};
    maj_species_sym = {'H2','O2','H2O'};
    min_species_sym = {'H','O','OH','HO2','H2O2'};
    maj_species = [1.0,0,0,1.0,0,1.0,0.0,0.0,0.0];
    min_species = [0.0,1.0,1.0,0.0,1.0,0.0,25.0,25.0,0.0];
    
     cc_maj = hsv(3);
     cc_min = hsv(6);
     
    figure(8);
    start_ki = num_base;
    %species_sym = {'H2','H','O','O2','OH','H2O','HO2','H2O2','AR'};
    subplot(2,2,1);
    maj_cnt = 0;
    for ki=num_base+1:num_base+num_species, %TODO: change later
        %plot(A(:,1),A(:,ki));
        %plot(A(:,1),A(:,ki),plot_syms{ki-num_base},'LineWidth',1,'LineSmoothing',smooth,'Color',cc(ki,:));
        if(maj_species(ki-num_base)>0.0)
        maj_cnt = maj_cnt +1;
        %plot(A(:,1),A(:,ki),'LineWidth',1.5,'LineSmoothing',smooth,'Color',cc(ki-num_base,:));
        plot(A(:,1),A(:,ki),'LineWidth',1.5,'LineSmoothing',smooth,'Color',cc_maj(maj_cnt,:));
        end
        hold on;
    end
     xlim([0.002 0.005]);
     xlabel('Position(m)');
    ylabel('Mole Fraction (X_k)');
    legend(maj_species_sym);
    grid on;
    hold off;
    
    subplot(2,2,2);
    min_cnt = 0;
    for ki=num_base+1:num_base+num_species, %TODO: change later
        %plot(A(:,1),A(:,ki));
        %plot(A(:,1),A(:,ki),plot_syms{ki-num_base},'LineWidth',1,'LineSmoothing',smooth,'Color',cc(ki,:));
        if(min_species(ki-num_base)>0.0)
             min_cnt = min_cnt +1;
        %plot(A(:,1),min_species(ki-num_base)*A(:,ki),'LineWidth',1.0,'LineSmoothing',smooth,'Color',cc(ki-num_base,:));
        plot(A(:,1),min_species(ki-num_base)*A(:,ki),'LineWidth',1.0,'LineSmoothing',smooth,'Color',cc_min(min_cnt,:));
        end
        hold on;
    end
    xlim([0.002 0.005]);
    xlabel('Position(m)');
    ylabel('Mole Fraction (X_k)');
    legend(min_species_sym);
    grid on;
    hold off;
    
    
    subplot(2,2,3);
     maj_cnt = 0;
    for ki=num_base+num_species+1:num_base+num_species+num_species, %TODO: change later
        %plot(A(:,1),A(:,ki));
        %plot(A(:,1),A(:,ki),plot_syms{ki-num_base-num_species},'LineWidth',1,'LineSmoothing',smooth,'Color',cc(ki,:));
        if(maj_species(ki-num_base-num_species)>0.0)
        maj_cnt = maj_cnt +1;
        plot(A(:,1),A(:,ki),'LineWidth',1,'LineSmoothing',smooth,'Color',cc_maj(maj_cnt,:));
        hold on;
        end
    end
    xlim([0.002 0.005]);
    xlabel('Position(m)');
    ylabel('Species production rate (kg/m^3-s)');
    legend(maj_species_sym);
    grid on;
    hold off;
    
    
    subplot(2,2,4);
    min_cnt = 0;
    for ki=num_base+num_species+1:num_base+num_species+num_species, %TODO: change later
        %plot(A(:,1),A(:,ki));
        %plot(A(:,1),A(:,ki),plot_syms{ki-num_base-num_species},'LineWidth',1,'LineSmoothing',smooth,'Color',cc(ki,:));
        if(min_species(ki-num_base-num_species)>0.0)
             min_cnt = min_cnt +1;
        %plot(A(:,1),A(:,ki),'LineWidth',1,'LineSmoothing',smooth,'Color',cc(ki-num_base-num_species,:));
        plot(A(:,1),A(:,ki),'LineWidth',1,'LineSmoothing',smooth,'Color',cc_min(min_cnt,:));
        %plot(A(:,1),A(:,ki),[linestyles{ki-num_base-num_species} Markers(ki-num_base-num_species)],'LineWidth',1,'LineSmoothing','off','Color',MarkerEdgeColors(ki-num_base-num_species,:));
        hold on;
        end
    end
    xlim([0.002 0.005]);
    xlabel('Position(m)');
    ylabel('Species production rate (kg/m^3-s)');
    legend(min_species_sym);
    grid on;
    hold off;

end

