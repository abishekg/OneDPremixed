 clc;
    clear all;
    close all;
    
   
     filename = 'flamevars.dat';
     
     
      num_pts = 1024;
    %num_species = 3;
    num_species = 9;
    
    num_base = 6+4;
    
    num_rows = num_base+ num_species+ num_species + num_species;
     
       fileID = fopen(filename);
    %A = fread(fileID,'double')
    A = fread(fileID,[num_pts num_rows],'double');
    
    
    x = A(:,1);
    T = A(:,4);
    %T = A(:,6);
    
    figure(1);
    plot(x,T);
    
    d2T_dx2 = zeros(num_pts);
    
   for i = 2:num_pts-1
    h = (x(i+1)-x(i-1))/2.0;   
    d2T_dx2(i) = (T(i+1)-2.0*T(i)+T(i-1))/(h*h);
   end
   
   d2T_dx2(1) = d2T_dx2(2);
   d2T_dx2(num_pts) = d2T_dx2(num_pts-1);
   
   
   %semilogy(x,abs(d2T_dx2));
   
   
   dT_dx = zeros(num_pts);
    
   for i = 2:num_pts-1
    h = (x(i+1)-x(i-1));   
    dT_dx(i) = (T(i+1)-T(i-1))/(h);
   end
   
   dT_dx(1) = (T(2)-T(1))/(x(2)-x(1));
   dT_dx(num_pts) = (T(num_pts)-T(num_pts-1))/(x(num_pts)-x(num_pts-1));
   
%plot(x,abs(dT_dx));

figure(2);
   subplot(3,1,1)
   plot(x,T);
   subplot(3,1,2)
   plot(x,dT_dx);
   subplot(3,1,3)
   plot(x,d2T_dx2);


x_start = num_pts -50;

m2 = (d2T_dx2(x_start+1)-d2T_dx2(x_start-1))/(x(x_start+1)-x(x_start-1));
c2 = d2T_dx2(x_start);

c1 = dT_dx(x_start);
c  = T(x_start);

% find where dT/dx drops to zero.


x_inf1 = -c1
x_inf2 = -2.0/m2*(c1+c2)   

x_inf = x_inf2;

T_ad = m2*x_inf^3.0/6.0 + c2*x_inf*x_inf/2 + c1*x_inf + c


ref_max = 1.e7;
max_found = 0;
loc_mac = 0.0;

li = -1, ri = -1; 

for i = 1:num_pts-1
    if(max_found ==0 && d2T_dx2(i+1) < d2T_dx2(i))
        if(d2T_dx2(i) > ref_max) 
        max_found = 1;
        end       
    elseif(max_found && d2T_dx2(i+1) < 0.0)
        li = i; ri = i+1;
        break;
    end
end
T1 = d2T_dx2(li);
T2 = d2T_dx2(ri);
x1 = x(li);
x2 = x(ri);
T_fc = 0.0;
x_fc = (T_fc - T1)*(x2-x1)/(T2-T1) + x1


    