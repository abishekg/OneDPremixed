%L = 0.01;
L = 0.007;
num_nodes =2048;
N = num_nodes+1;
%flame_pos_frac = 0.35;
flame_pos_frac = 0.17;
refine_frac  = 0.28;
refine_pt_frac = 0.50;

Lx = refine_frac*L;

Nx = int32(refine_pt_frac*N);



dx_high = Lx/double(Nx-1);

dx_init = dx_high;

%rl = 1.01;
%rl =1.0619594648880756951079597942283;
%rl= 1.014911928887127;

%Ll = dx_init*(rl^(Nl+1) - rl)/(rl -1.0);


Lfp = flame_pos_frac*L;

Ll = (Lfp-Lx/2.0);
%Ll = (L-Lx)/2.0;
Lr = L - Lx - Ll;


length_ratio = Ll/(Ll+Lr);


Nl = int32(length_ratio*double(N-Nx));
%Nl = int32((N-Nx)/2);
Nr = N - Nx - Nl;



%syms xa;
%solve(xa*(Ll+dx_high) -dx_high*xa^(Nl+1) == Ll, xa, 'Real', true)
%srs = solve(xa*(Ll+dx_high) -dx_high*xa^(Nl+1) == Ll, xa)

fun = @(y)( double(y)*(Ll+dx_high) -dx_high*double(y)^double(Nl+1) - Ll); % function
%fun = @(y) dx_high*double(y)^double(Nl+1) - double(y)*(Ll+dx_high) + Ll % function
x0 = [1.000001 1.1]; % initial interval
%x0 = 1.01;
[roots,fval,exitflag] = fzero(fun,x0)

rl = roots(1)

fun = @(y)( double(y)*(Lr+dx_high) -dx_high*double(y)^double(Nr+1) - Lr); % function
[roots,fval,exitflag] = fzero(fun,x0)

%solve(xa*(Lr+dx_high) -dx_high*xa^(Nr+1) == Lr, xa, 'Real', true)

%rr = 1.0619594648880756951079597942283;
%rr = 1.014911928887127;


rr = roots(1)

%Lr = dx_init*(rr^(Nr+1) - rr)/(rr -1.0);


%Lx = L - Ll - Lr;
%dx_high = Lx/double(Nx-1);


%Ll = (L-Lx)/2.0;
%Lr = L - Lx - Ll;

x = zeros(N,1);
dx_arr = zeros(N,1);

%for i=Nl+1:Nl+Nx+1
for i=Nl+1:Nl+Nx

    x(i) = Ll + double(i-(Nl+1))*dx_high;
    dx_arr(i) = dx_high;
end

%dxl = Ll/double(Nl);
dxl = dx_init;

%for i=1:Nl
for i=Nl:-1:1
    dxl = dxl*rl;
    x(i) = x(i+1) - dxl;
    dx_arr(i) = dxl;

end

%dxr = Lr/double(Nr);
dxr = dx_init;
%cnt=0;
for i=Nl+Nx+1:N
    dxr = dxr*rr;
    %cnt = cnt+1;
    %ab = Ll+Lx + dxr;
    x(i) = x(i-1) + dxr;
    dx_arr(i) = dxr;
end

x(1) = 0.0;
x(N) = L;

%cnt
%plot(x,'.');
%plot(dx_arr,'.');
%plot(x,[1:N],'.');


fig=figure; 
hax=axes; 
%x=0:0.1:10; 
hold on 
%plot(x,sin(x)) 

for i=1:10:N
    SP=x(i); %your point goes here 
line([SP SP],get(hax,'YLim'),'Color',[0 0 0])
end

%for i=1:N
%hx = graph2d.constantline(0.005, 'LineStyle',':', 'Color',[.7 .7 .7]);
%end
%changedependvar(hx,'x');
hold off;
figure(2);
plot(x,dx_arr,'.');

