function [ output_args ] = AnimateBinaryFlameVars( filename )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
 
    clc;
    clear all;
    close all;
    
    ti = 0;
    
    if nargin < 1
      filename = 'timehistory.dat';
    end
   
    smooth = 'off';
    %smooth = 'on';
    
    %num_pts = 4096;
    %num_pts = 512;
    %num_pts = 2048;
    num_pts = 1024;
    num_species = 3;
    %num_species = 9;
    
    num_base = 6;
    
    species_sym = {'H2','O2','H2O'};
    cc = hsv(num_species);
    
    %num_rows = num_base+ num_species+ num_species + num_species;
    num_rows = num_base;
    
    
    %Frame(100) = struct('cdata',[],'colormap',[]);
    
    writerObj = VideoWriter('mov_outlet_relaxed.avi');
    writerObj.Quality = 100;
    writerObj.FrameRate = 10;
    open(writerObj);
    
    h =figure('name','Flame Variables','units','normalized','outerposition',[0 0 1 1]);
    %set(gca,'NextPlot','replaceChildren');
    fileID = fopen(filename);
    %A = fread(fileID,'double')
    
    while ~feof(fileID)
    A = fread(fileID,[num_pts num_rows],'double');
    dim  = size(A);
    if(dim(1)<num_pts)
        break;
    end
    ti = ti +1
    %get(0,'DefaultAxesColorOrder')

    %figure('name','Flame Variables','units','normalized','outerposition',[0 0 1 1]);
    
        %A.colheaders{1, k}
    %subplot(2,2,1);
    %plot_syms={':','--','-'};
    
    
    %cc = distinguishable_colors(num_species);
    
    %plot_syms={'r','b','k'};
    
    %plot_syms={'-b','-g','-r','-y','-d','--','-',':','o','x'};
    %species_sym = {'H2','H','O','O2','OH','H2O','HO2','H2O2','AR'};
    
    figure(h);
    
    subplot(2,2,1); %P
    plot(A(:,1),A(:, 2),'LineWidth',1.5,'LineSmoothing',smooth,'Color',cc(1,:));
    xlabel('x (m)');
    ylabel('Density (kg/m3)');
     xlim([0.0 0.01]);
    %ylim([100.0 2300.0]);
    grid on;

    

    subplot(2,2,2); %T
    plot(A(:,1),A(:, 4),'LineWidth',1.5,'LineSmoothing',smooth,'Color',cc(1,:));
    xlabel('x (m)');
    ylabel('Temperature (K)');
     xlim([0.0 0.01]);
    ylim([100.0 5000.0]);
    grid on;

    subplot(2,2,3); %u
    plot(A(:,1),A(:,3),'LineWidth',1,'LineSmoothing',smooth);
    xlabel('x (m)');
    ylabel('Velocity (m/s)');
     xlim([0.0 0.01]);
    ylim([-8.0 30.0]);
     grid on;

    subplot(2,2,4); %P
    
    plot(A(:,1),A(:,6)/1.e5,'LineWidth',1,'LineSmoothing',smooth);
    xlabel('x (m)');
    ylabel('Pressure (atm)');
    xlim([0.0 0.01]);
    ylim([0.99 1.02]);
    %ylim([9.5e4 1.5e5]);
     grid on;
    %plot(x,rho);
    
%     figure(2);
%     plot(A(:,1),A(:,2),'LineWidth',1,'LineSmoothing',smooth);
%     grid on;
% 
%     figure(3);
%     plot(A(:,1),A(:,5),'LineWidth',1,'LineSmoothing',smooth);
%     grid on;
%     
%     figure(4);
%     for ki=num_base+num_species+1:num_base+num_species+num_species, %TODO: change later
%         %plot(A(:,1),A(:,ki));
%         %plot(A(:,1),A(:,ki),plot_syms{ki-num_base-num_species},'LineWidth',1,'LineSmoothing',smooth,'Color',cc(ki,:));
%         plot(A(:,1),A(:,ki),'LineWidth',1,'LineSmoothing',smooth,'Color',cc(ki-num_base-num_species,:));
%         hold on;
%     end
%     legend(species_sym);
%     grid on;
%     hold off;
%     
%     figure(5);
%     for ki=num_base+num_species+num_species+1:num_base+num_species+num_species+num_species, %TODO: change later
%         %plot(A(:,1),A(:,ki));
%         %plot(A(:,1),A(:,ki),plot_syms{ki-num_base-num_species-num_species},'LineWidth',1,'LineSmoothing',smooth,'Color',cc(ki,:));
%         plot(A(:,1),A(:,ki),'LineWidth',1,'LineSmoothing',smooth,'Color',cc(ki-num_base-num_species-num_species,:));
%         hold on;
%     end
%     legend(species_sym);
%     grid on;
%     hold off;
% 
%     
%     figure(6);
%     [haxes,hline1,hline2]= plotyy(A(:,1),A(:,7),A(:,1),A(:,13),'plot','plot');
%     ylabel(haxes(1),'Xk - H2');
%     ylabel(haxes(2),' Del(Xk)/Xk - H2');
%     xlabel(haxes(2),' X');
%     set(hline2,'LineStyle','-');
    %plot(A(:,1),A(:,4));
    %plot(A(:,1),A(:,13));
    
    %Frame(ti)  = getframe;
    
    Frame = getframe(gcf);
    writeVideo(writerObj,Frame);
    
    %Frame(ti)  = getframe(gcf);
    
    end
    
    
    fclose(fileID);
    
    %fig= figure;
    %movie(fig,Frame,4,1,[0 0 0 1]);
    
    close(writerObj);
    
    %movie(Frame,2,1);
    
    %movie2avi(Frame,'test',FPS,'1');

end

