function [ output_args ] = PlotRecordVars( filename )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    clc;
    clear all;
    close all;
    

    if nargin < 1
      %filename = 'records.bin'
      %filename = '/home/abishekg/simulations/OneDPremixed/test_AR_phi4.5/records.bin';
      %filename = '/home/abishekg/simulations/OneDPremixed/test_AR_phi0.6/test/records.bin';
      %filename = '/home/abishekg/runs/test_stretch/test2/records.bin';
      %filename = '/home/abishekg/simulations/OneDPremixed/test_AR_phi0.6/unstetched/records.bin';
      filename = '/home/abishekg/runs/half_domain/records.bin';
    end

    u_target = 2.983;
    P_target = 1.0e5;
    T_target = 300.0;
    
    num_record = 7;
    %record_int = 200000;
    %record_int = 340000;
    %record_int = 800000;
    %record_int = 1600000;
    record_int = 2000000;
    %record_int = 250000;
    
    headerlinesIn = 1;
    
    fileID = fopen(filename);
    A = fread(fileID,[num_record record_int],'double')
    
     
    %A = importdata(filename,delimiterIn,headerlinesIn);


    figure(1);
    plot(A(1,:),A(3,:),'-b');
    hold on;
    plot(A(1,:),A(4,:),'-r');
    title('Ku = 8.0e-4; Pu = 2.0e-4s; K_p = 0.45*Ku; K_i = 1.2*K_p/Pu;');
    ylabel(' velocity (m/s)');
    xlabel('time(seconds)');
    
    legend('Flame speed','Inlet velocity');
    ylim([-100 100]);
    
    hold off;
    
        %SP=x(i); %your point goes here 
%line([SP SP],get(hax,'YLim'),'Color',[0 0 0])
    
    figure(2);
    
    plot(A(2,:),A(1,:));
    hold on;
  
    xlim([0.0 0.01]);
    ylabel(' time(seconds)');
    xlabel('position(m)');
    
    figure(3);
    subplot(4,1,1);
    plot(A(1,:),A(4,:));
    %semilogy(A(1,:),abs(A(4,:)-2.97));
    hold on;
   
    %hline = refline([0 u_target]);
    %set(hline,'Color','r');
    hold off;
    xlabel(' time(seconds)');
    ylabel('inlet velocity (m/s)');
    
    subplot(4,1,2);
    plot(A(1,:),A(5,:));
    %semilogy(A(1,:),abs(A(5,:)-T_target));
    hold on;
  
    hline = refline([0 T_target]);
    set(hline,'Color','r');
    hold off;
    xlabel(' time(seconds)');
    ylabel('inlet temperature (K)');
    
    subplot(4,1,3);
    plot(A(1,:),A(6,:));
    %semilogy(A(1,:),abs(A(6,:)-P_target));
    hold on;
   
    hline = refline([0 P_target]);
    set(hline,'Color','r');
    hold off;
    xlabel(' time(seconds)');
    ylabel('outlet pressure (Pa)');
    
    subplot(4,1,4);
    plot(A(1,:),A(7,:));
    hold on;
  
    xlabel(' time(seconds)');
    ylabel('outlet temperature (K)');
    
    
        %A.colheaders{1, k}
    %subplot(2,2,1);
    %plot_syms={':','--','-'};
    
    %
    %for ki=7:9, %TODO: change later
    %    plot(A.data(:, 1),A.data(:, ki),plot_syms{ki-6});
    %    hold on;
    %end
    %legend(species_sym);
    %hold off;

    %subplot(2,2,2); %T
    %plot(A.data(:, 1),A.data(:, 4));

    %subplot(2,2,3); %u
    %plot(A.data(:, 1),A.data(:, 3));

    %subplot(2,2,4);
    %plot(A.data(:, 1),A.data(:, 10));
    %plot(x,rho);




end
