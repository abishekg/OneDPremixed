function [ output_args ] = PlotFlameVars( filename )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
 
    clc;
    clear all;
    close all;
    
    if nargin < 1
     %filename = 'flamevars.dat';
     %filename = '/Users/abishekg/etna_remote/simulations/OneDPremixed/test_pressure/high/flamevars.dat';

      filename = '/home/abishekg/simulations/OneDPremixed/grid_conv/test3/flamevars.dat';
      %filename = '/home/abishekg/simulations/OneDPremixed/half_domain/flamevars.dat';
     % filename = '/home/abishekg/Downloads/flamevars.dat';
    end
   
    filename2 = 'flamespeed.csv'
    
    smooth = 'off';
    %smooth = 'on';
    
    %x_add = 1.145e-3;
    %x_add = 1.158e-3;
    x_add = -0.3497e-3;
    
    %num_pts = 4096;
    %num_pts = 512;
    %num_pts = 2048;
    num_pts = 1024;
    %num_species = 3;
    num_species = 9;
    
    num_base = 6+4;
    
    num_rows = num_base+ num_species+ num_species + num_species;
    
    delimiterIn = ',';
    headerlinesIn = 1;
    A2 = importdata(filename2,delimiterIn,headerlinesIn);
    
    dat = A2.data;
    num_pts2 = size(dat,1);
    max_x = max(dat(:,1));
    
    %num_cols = 11 + num_species + num_species;
    num_cols = size(dat,2);
    
    fileID = fopen(filename);
    %A = fread(fileID,'double')
    A = fread(fileID,[num_pts num_rows],'double');
    
    %A = importdata(filename,delimiterIn,headerlinesIn);


    %get(0,'DefaultAxesColorOrder')

    figure('name','Flame Variables','units','normalized','outerposition',[0 0 1 1]);
    grid on;
        %A.colheaders{1, k}
    subplot(2,2,1);
    %plot_syms={':','--','-'};
    
    cc = hsv(num_species);
    %cc = distinguishable_colors(num_species);
    
    %plot_syms={'r','b','k'};
    %species_sym = {'H2','O2','H2O'};
    %plot_syms={'-b','-g','-r','-y','-d','--','-',':','o','x'};
    species_sym = {'H2','H','O','O2','OH','H2O','HO2','H2O2','AR'};
    species_sym2 = {'C H2','C H','C O','C O2','C OH','C H2O','C HO2','C H2O2','C AR'};
    %
%     for ki=num_base+1:num_base+num_species, %TODO: change later
%         %plot(A(:,1),A(:,ki));
%         %plot(A(:,1),A(:,ki),plot_syms{ki-num_base},'LineWidth',1,'LineSmoothing',smooth,'Color',cc(ki,:));
%         plot(A(:,1),A(:,ki),'LineWidth',1.5,'LineSmoothing',smooth,'Color',cc(ki-num_base,:));
%         hold on;
%     end
%     legend(species_sym);
%     grid on;
%     hold off;
    
    plot(A(:,1),A(:, 2),'LineWidth',1.5,'LineSmoothing',smooth,'Color',cc(1,:));
    hold on;
    plot(dat(:,1)+x_add,dat(:,7),'LineWidth',1,'LineSmoothing',smooth);
    xlim([0.0 max_x]);
    hold off;
    grid on;
    

    subplot(2,2,2); %T
    plot(A(:,1),A(:, 4),'LineWidth',1.5,'LineSmoothing',smooth,'Color',cc(1,:));
    hold on;
    plot(dat(:,1)+x_add,dat(:,2),'LineWidth',1.2,'LineSmoothing',smooth);
     xlim([0.0 max_x]);
    hold off;
    grid on;

    subplot(2,2,3); %u
    plot(A(:,1),A(:,3),'LineWidth',1,'LineSmoothing',smooth,'Color',cc(1,:));
    hold on;
    plot(dat(:,1)+x_add,dat(:,3),'LineWidth',1,'LineSmoothing',smooth);
     xlim([0.0 max_x]);
    hold off;
     grid on;

    subplot(2,2,4); %P
    plot(A(:,1),A(:,6)-1.e5,'LineWidth',1,'LineSmoothing',smooth,'Color',cc(1,:));
    hold on;
    plot(dat(:,1)+x_add,dat(:,6)-1.e5,'LineWidth',1,'LineSmoothing',smooth);
    hold off;
     xlim([0.0 0.009]);
     grid on;
    %plot(x,rho);
    
    figure(2);
    plot(A(:,1),A(:,2),'LineWidth',1,'LineSmoothing',smooth);
    grid on;

    figure(3);
    subplot(2,2,1); %cp
    plot(A(:,1),A(:,7),'LineWidth',1,'LineSmoothing',smooth,'Color',cc(1,:));
    hold on;
    plot(dat(:,1)+x_add,dat(:,9),'LineWidth',1,'LineSmoothing',smooth);
    hold off;
    ylabel('Cp');
    grid on;
    
    subplot(2,2,2); %lambda
    plot(A(:,1),A(:, 8),'LineWidth',1.5,'LineSmoothing',smooth,'Color',cc(1,:));
    hold on;
    plot(dat(:,1)+x_add,dat(:,11),'LineWidth',1,'LineSmoothing',smooth);
    hold off;
    ylabel('lambda');
    grid on;

    subplot(2,2,3); %mu
    plot(A(:,1),A(:,9),'LineWidth',1,'LineSmoothing',smooth,'Color',cc(1,:));
    hold on;
    plot(dat(:,1)+x_add,dat(:,10),'LineWidth',1.2,'LineSmoothing',smooth);
    hold off;
    ylabel('mu');
     grid on;

    subplot(2,2,4); %w
    plot(A(:,1),A(:,10),'LineWidth',1,'LineSmoothing',smooth,'Color',cc(1,:));
    hold on;
    plot(dat(:,1)+x_add,dat(:,8)/1000.0,'LineWidth',1.2,'LineSmoothing',smooth);
    hold off;
    ylabel('W');
     grid on;
    
    %leg_sym 
     
    figure(4);
    %for ki=num_base+num_species+1:num_base+num_species+num_species, %TODO: change later
        ki=num_base+num_species+1 + 4;
        loc_ind = ki-num_base-num_species;
        %plot(A(:,1),A(:,ki));
        %plot(A(:,1),A(:,ki),plot_syms{ki-num_base-num_species},'LineWidth',1,'LineSmoothing',smooth,'Color',cc(ki,:));
        %plot(A(:,1),A(:,ki),'LineWidth',1,'LineSmoothing',smooth,'Color',cc(loc_ind,:));
        plot(A(:,1),A(:,ki),'LineWidth',1,'LineSmoothing',smooth,'Color',cc(1,:));
        hold on;
        %plot(dat(:,1)+x_add,dat(:,11+loc_ind),'LineWidth',1,'LineSmoothing',smooth,'Color',cc(num_species -loc_ind +1,:));%,'Marker','+','MarkerSize',1);
        plot(dat(:,1)+x_add,dat(:,11+loc_ind),'LineWidth',1,'LineSmoothing',smooth);%,'Marker','+','MarkerSize',1);
        leg_sym{2*(loc_ind-4)-1} = species_sym{loc_ind};
        leg_sym{2*(loc_ind-4)} = species_sym2{loc_ind};
    %end
    legend(leg_sym);
    grid on;
    hold off;
    
    figure(5);
    %for ki=num_base+num_species+num_species+1:num_base+num_species+num_species+num_species, %TODO: change later
        ki=num_base+num_species+num_species+1 + 4;
        loc_ind = ki-num_base-num_species-num_species;
        %plot(A(:,1),A(:,ki));
        %plot(A(:,1),A(:,ki),plot_syms{ki-num_base-num_species-num_species},'LineWidth',1,'LineSmoothing',smooth,'Color',cc(ki,:));
        %plot(A(:,1),A(:,ki),'LineWidth',1,'LineSmoothing',smooth,'Color',cc(loc_ind,:));
        plot(A(:,1),A(:,ki),'LineWidth',1,'LineSmoothing',smooth,'Color',cc(1,:));
        hold on;
        %plot(dat(:,1)+x_add,dat(:,11+num_species+loc_ind),'LineWidth',1,'LineSmoothing',smooth,'Color',cc(loc_ind,:),'Marker','+','MarkerSize',3);
        plot(dat(:,1)+x_add,dat(:,11+num_species+loc_ind),'LineWidth',1,'LineSmoothing',smooth);
       
    %end
    legend(leg_sym);
    grid on;
    hold off;

    
 myDk = A(:,8)./(A(:,2).*A(:,7));
 
 cantDk = dat(:,11)./(dat(:,7).*dat(:,9));
 figure(6);

 plot(A(:,1),myDk,'LineWidth',1,'LineSmoothing',smooth,'Color',cc(1,:));
 hold on;
plot(dat(:,1)+x_add,cantDk,'LineWidth',1,'LineSmoothing',smooth);
hold off;
end

