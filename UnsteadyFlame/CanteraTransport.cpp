/*
 *  CanteraTransport.cpp
 *
 *  Created on: Jul 8, 2014
 *      Author: abishekg
 */

#include "CanteraTransport.h"
#include "cantera/base/ct_defs.h"
#include "cantera/base/utilities.h"
#include "PhysicalConstants.h"
#include <cmath>
#include <iostream>

using namespace std;
using namespace Cantera;
namespace Flame {

CanteraTransport::CanteraTransport(int nx,CanteraWrapper *cantera_obj) {

	num_pts = nx;
	num_species = cantera_obj->getNumSpecies();
	num_coeffs = 5;

	T_prev =  new double[num_pts];
	for(int i=0;i<num_pts;i++)
		T_prev[i] = -1.0;

	lambda_k = new double*[num_pts];
	for(int i=0;i<num_pts;i++)
		lambda_k[i] = new double[num_species];

	mu_k = new double*[num_pts];
	for(int i=0;i<num_pts;i++)
		mu_k[i] = new double[num_species];

	mu_sqroot_k = new double*[num_pts];
	for(int i=0;i<num_pts;i++)
		mu_sqroot_k[i] = new double[num_species];

	D_k = new double*[num_pts];
	for(int i=0;i<num_pts;i++)
		D_k[i] = new double[num_species];

	//V_k = new double*[num_pts];
	V_k = new double*[num_pts+1];
	for(int i=0;i<num_pts+1;i++)
		V_k[i] = new double[num_species];

	//V_c = new double[num_pts];
	V_c = new double[num_pts+1];

	V_low_k  = new double*[num_species];
	for(int i=0;i<num_species;i++)
		V_low_k[i] = new double[num_coeffs];

	V_high_k  = new double*[num_species];
	for(int i=0;i<num_species;i++)
		V_high_k[i] = new double[num_coeffs];

	C_low_k  = new double*[num_species];
	for(int i=0;i<num_species;i++)
		C_low_k[i] = new double[num_coeffs];

	C_high_k  = new double*[num_species];
	for(int i=0;i<num_species;i++)
		C_high_k[i] = new double[num_coeffs];

	//int ic = num_species*num_species;

	int ic = 0;
	for (int  i = 0; i < num_species; i++)
	  for (int j = i; j < num_species; j++)
		ic++;

	ic_tot = ic;

	D_bin_low_k  = new double*[ic_tot];
	for(int i=0;i<ic_tot;i++)
		D_bin_low_k[i] = new double[num_coeffs];

	D_bin_high_k  = new double*[ic_tot];
	for(int i=0;i<ic_tot;i++)
		D_bin_high_k[i] = new double[num_coeffs];

	D_bin_k = new double*[num_pts];
	for(int i=0;i<num_pts;i++)
		D_bin_k[i] = new double[num_species*num_species];



	bin_diff_temp = new double*[num_species];
	for(int i=0;i<num_species;i++)
		bin_diff_temp[i] = new double[num_species];

	cantera_obj->copyTransportFits(V_low_k,V_high_k,C_low_k,C_high_k,D_bin_low_k,D_bin_high_k);

	T_low_fit = cantera_obj->getLowFit();
	T_med_fit = cantera_obj->getMedFit();
	T_high_fit = cantera_obj->getHighFit();

}

CanteraTransport::~CanteraTransport() {

/*	for(int i=0;i<num_pts;i++)
		delete [] lambda_k[i];
	delete lambda_k;

	for(int i=0;i<num_pts;i++)
		delete [] mu_k[i];
	delete mu_k;

	for(int i=0;i<num_pts;i++)
		delete [] mu_sqroot_k[i];
	delete mu_sqroot_k;

	for(int i=0;i<num_pts;i++)
		delete [] D_k[i];
	delete D_k;


	//for(int i=0;i<num_pts;i++)
	for(int i=0;i<num_pts+1;i++)
		delete [] V_k[i];
	delete V_k;

	delete [] V_c;
	//delete V_c;

	for(int i=0;i<num_species;i++)
		delete [] V_low_k[i];
	delete V_low_k;

	for(int i=0;i<num_species;i++)
		delete [] V_high_k[i];
	delete V_high_k;

	for(int i=0;i<num_species;i++)
		delete [] C_low_k[i];
	delete C_low_k;

	for(int i=0;i<num_species;i++)
		delete [] C_high_k[i];
	delete C_high_k;*/

	//int ic = num_species*num_species;

	delete [] T_prev;

	for(int i=0;i<ic_tot;i++)
		delete [] D_bin_low_k[i];
	delete [] D_bin_low_k;

	for(int i=0;i<ic_tot;i++)
		delete [] D_bin_high_k[i];
	delete [] D_bin_high_k;


	for(int i=0;i<num_species;i++)
		delete [] bin_diff_temp[i];
	delete [] bin_diff_temp;

	cout<<"Deallocating CanteraTransport objects.."<<endl;
	// TODO Auto-generated destructor stub
}


void CanteraTransport::estimateSpeciesViscosity(double T) {

	 double m_temp = T;
	 double m_logt = log(m_temp);
	 double m_sqrt_t = sqrt(m_temp);
	 double m_t14 = sqrt(m_sqrt_t);

	double m_polytempvec[5];
	// compute powers of log(T)
	m_polytempvec[0] = 1.0;
	m_polytempvec[1] = m_logt;
	m_polytempvec[2] = m_logt*m_logt;
	m_polytempvec[3] = m_logt*m_logt*m_logt;
	m_polytempvec[4] = m_logt*m_logt*m_logt*m_logt;

	double **visccoeffs;
	if(T<=T_med_fit)
		visccoeffs = V_low_k;
	else if(T>T_med_fit)
		visccoeffs = V_high_k;

	for (int k = 0; k < num_species; k++) {
		// the polynomial fit is done for sqrt(visc/sqrt(T))
	    //m_sqvisc[k]
		double tmp11 = m_t14 * dot5(&m_polytempvec[0], visccoeffs[k]);
		//m_visc[k] = (m_sqvisc[k] * m_sqvisc[k]);
		cout<< "Species viscosity: "<<tmp11*tmp11<<std::endl;
	}


}

void CanteraTransport::estimateSpeciesConductivity(double T) {

	double m_temp = T;
	double m_logt = log(m_temp);
	double m_sqrt_t = sqrt(m_temp);
	double m_t14 = sqrt(m_sqrt_t);

	double m_polytempvec[5];
	// compute powers of log(T)
	m_polytempvec[0] = 1.0;
	m_polytempvec[1] = m_logt;
	m_polytempvec[2] = m_logt*m_logt;
	m_polytempvec[3] = m_logt*m_logt*m_logt;
	m_polytempvec[4] = m_logt*m_logt*m_logt*m_logt;

	double **condcoeffs;
	if(T<=T_med_fit)
		condcoeffs = C_low_k;
	else if(T>T_med_fit)
		condcoeffs = C_high_k;

	for (int k = 0; k < num_species; k++) {
		// the polynomial fit is done for sqrt(visc/sqrt(T))
		//m_sqvisc[k]
		double tmp11 = m_sqrt_t * dot5(&m_polytempvec[0],condcoeffs[k]);
		cout<< "Species Lambda: "<<tmp11<<std::endl;
	}

}


void CanteraTransport::estimateSpeciesDiffusivity(double T,double P,double *Yk,double *Xk) {

	double m_temp = T;
	double m_logt = log(m_temp);
	double m_sqrt_t = sqrt(m_temp);
	double m_t14 = sqrt(m_sqrt_t);

	double m_polytempvec[5];
	// compute powers of log(T)
	m_polytempvec[0] = 1.0;
	m_polytempvec[1] = m_logt;
	m_polytempvec[2] = m_logt*m_logt;
	m_polytempvec[3] = m_logt*m_logt*m_logt;
	m_polytempvec[4] = m_logt*m_logt*m_logt*m_logt;

	double **diffcoeffs;
	if(T<=T_med_fit)
		diffcoeffs = D_bin_low_k;
	else if(T>T_med_fit)
		diffcoeffs = D_bin_high_k;


	int kk = num_species*num_species;
	double bin_diff[num_species][num_species];
	double D_k_temp[num_species];

	double rp = 1.0/P;
	double temp = 0.0;

	int ic = 0;

	for (size_t i = 0; i < num_species; i++)
		for (size_t j = i; j < num_species; j++) {
			temp = rp*m_temp * m_sqrt_t*dot5(m_polytempvec,diffcoeffs[ic]);
			//bin_diff[i][j] = rp*m_temp * m_sqrt_t*dot5(m_polytempvec,diffcoeffs[ic]);
			bin_diff[i][j] = Xk[i]/temp;
			bin_diff[j][i] = Xk[j]/temp;
			//bin_diff[j][i] = bin_diff[i][j];
			++ic;
	}
	for (int k = 0; k < num_species; k++){
		bin_diff[k][k] = 0.0;
		temp = 0.0;
		for (int j = 0; j < num_species; j++)
			temp += bin_diff[j][k];

		D_k_temp[k] = (1.0-Yk[k])/temp;
	}



/*	doublereal rp = 1.0/m_thermo->pressure();
	    for (size_t i = 0; i < m_nsp; i++)
	        for (size_t j = 0; j < m_nsp; j++) {
	            d[ld*j + i] = rp * m_bdiff(i,j);
	        }
*/
	for (int k = 0; k < num_species; k++) {
		// the polynomial fit is done for sqrt(visc/sqrt(T))
		//m_sqvisc[k]
		//double tmp11 = m_sqrt_t * dot5(&m_polytempvec[0],condcoeffs[k]);
		cout<< "Species Lambda: "<<D_k_temp[k]<<std::endl;
	}

}



void CanteraTransport::getTransportProperties(int num_low_T,int num_high_T,int *low_T_indices,int *high_T_indices,double* T_ptr,double *P_ptr,double **Xk_ptr,double **Yk_ptr) {

	double T;
	double m_temp;
	double m_logt;
	double m_sqrt_t;
	double m_t14;
	double m_polytempvec[5];
	double visc_sqrt;
	int i ;

	double *Xk;
	double rp = 0.0;
	double temp = 0.0;

	double **condcoeffs;
	double **visccoeffs;

	double **diffcoeffs;

	condcoeffs = C_low_k;
	visccoeffs = V_low_k;
	diffcoeffs = D_bin_low_k;

	for(int j=0;j<num_low_T;j++){

		i = low_T_indices[j];
		T = T_ptr[i];

		//if(T!=T_prev[i]){  // Only update quantities when Temp changes
		if(abs(T-T_prev[i])>temp_tol){  // Only update quantities when Temp changes

		T_prev[i] = T;
		m_temp = T;
		m_logt = log(m_temp);
		m_sqrt_t = sqrt(m_temp);
		m_t14 = sqrt(m_sqrt_t);


		// compute powers of log(T)
		m_polytempvec[0] = 1.0;
		m_polytempvec[1] = m_logt;
		m_polytempvec[2] = m_logt*m_logt;
		m_polytempvec[3] = m_logt*m_logt*m_logt;
		m_polytempvec[4] = m_logt*m_logt*m_logt*m_logt;

		for(int k=0;k<num_species;k++){
			visc_sqrt  =  m_t14 * dot5(&m_polytempvec[0], visccoeffs[k]);
			mu_sqroot_k[i][k] = visc_sqrt;
			mu_k[i][k] = visc_sqrt*visc_sqrt;

		}

		for(int k=0;k<num_species;k++){
			lambda_k[i][k]= m_sqrt_t * dot5(&m_polytempvec[0],condcoeffs[k]);
		}



		rp = 1.0/P_ptr[i];
		Xk = Xk_ptr[i];

		int ic = 0;

		for (size_t j = 0; j < num_species; j++)
			for (size_t k = j; k < num_species; k++) {
				temp = rp*m_temp * m_sqrt_t*dot5(m_polytempvec,diffcoeffs[ic]);
				//bin_diff[i][j] = rp*m_temp * m_sqrt_t*dot5(m_polytempvec,diffcoeffs[ic]);
				bin_diff_temp[j][k] = Xk[j]/temp;
				bin_diff_temp[k][j] = Xk[k]/temp;
				//bin_diff[j][i] = bin_diff[i][j];
				++ic;
		}
		for (int k = 0; k < num_species; k++){
			bin_diff_temp[k][k] = 0.0;
			temp = 0.0;
			for (int j = 0; j < num_species; j++)
				temp += bin_diff_temp[j][k];

			D_k[i][k] = (1.0-Yk_ptr[i][k])/temp;
		}

		}

	}

	condcoeffs = C_high_k;
	visccoeffs = V_high_k;
	diffcoeffs = D_bin_high_k;

	for(int j=0;j<num_high_T;j++){

		i = high_T_indices[j];
		T = T_ptr[i];

		//if(T!=T_prev[i]){  // Only update quantities when Temp changes
		if(abs(T-T_prev[i])>temp_tol){  // Only update quantities when Temp changes

		T_prev[i] = T;
		m_temp = T;
		m_logt = log(m_temp);
		m_sqrt_t = sqrt(m_temp);
		m_t14 = sqrt(m_sqrt_t);


		// compute powers of log(T)
		m_polytempvec[0] = 1.0;
		m_polytempvec[1] = m_logt;
		m_polytempvec[2] = m_logt*m_logt;
		m_polytempvec[3] = m_logt*m_logt*m_logt;
		m_polytempvec[4] = m_logt*m_logt*m_logt*m_logt;

		for(int k=0;k<num_species;k++){
			visc_sqrt  =  m_t14 * dot5(&m_polytempvec[0], visccoeffs[k]);
			mu_sqroot_k[i][k] = visc_sqrt;
			mu_k[i][k] = visc_sqrt*visc_sqrt;
		}

		for(int k=0;k<num_species;k++){
			lambda_k[i][k]= m_sqrt_t * dot5(&m_polytempvec[0],condcoeffs[k]);
		}



		rp = 1.0/P_ptr[i];
		Xk = Xk_ptr[i];

		int ic = 0;

		for (size_t j = 0; j < num_species; j++)
			for (size_t k = j; k < num_species; k++) {
				temp = rp*m_temp * m_sqrt_t*dot5(m_polytempvec,diffcoeffs[ic]);
				//bin_diff[i][j] = rp*m_temp * m_sqrt_t*dot5(m_polytempvec,diffcoeffs[ic]);
				bin_diff_temp[j][k] = Xk[j]/temp;
				bin_diff_temp[k][j] = Xk[k]/temp;
				//bin_diff[j][i] = bin_diff[i][j];
				++ic;
		}
		for (int k = 0; k < num_species; k++){
			bin_diff_temp[k][k] = 0.0;
			temp = 0.0;
			for (int j = 0; j < num_species; j++)
				temp += bin_diff_temp[j][k];

			D_k[i][k] = (1.0-Yk_ptr[i][k])/temp;
		}

		}
	}

}


void CanteraTransport::getDiffusionProperties(GridOperations *grid_ops,double *lambda,double *rho,double *Cp,double *Le_k,double *Wk,double *W_face,double **del_Xk_Xk,double **Xk) {
//void CanteraTransport::getDiffusionProperties(double *lambda,double *rho,double *Cp,double *Le_k,double *Wk,double *W_face,double **del_Xk_Xk,double **Xk_face) {

	//estimateSpeciesMolecularDiffusivity(lambda,rho,Cp,Le_k);


	//Hack to enforce unity Lewis numbers
	/*for(int i=0;i<num_pts;i++)
		for(int k=0;k<num_species;k++)
			D_k[i][k]=lambda[i]/(rho[i]*Cp[i]*1.0);*/

	estimateSpeciesDiffusivityVelocity(grid_ops,Wk,W_face,del_Xk_Xk,Xk);

	//estimateSpeciesDiffusivityVelocity(Wk,W_face,del_Xk_Xk,Xk_face);
}


void CanteraTransport::estimateSpeciesMolecularDiffusivity(double *lambda,double *rho,double *Cp,double *Le_k) {

	for(int i=0;i<num_pts;i++)
		for(int k=0;k<num_species;k++)
			D_k[i][k]=lambda[i]/(rho[i]*Cp[i]*Le_k[k]);

}

//void CanteraTransport::estimateSpeciesDiffusivityVelocity(double *Wk,double *W,double **del_Xk_Xk,double **Xk) {
void CanteraTransport::estimateSpeciesDiffusivityVelocity(GridOperations *grid_ops,double *Wk,double *W_face,double **del_Xk_Xk,double **Xk) {

	double W_inv = 0.0;
	double local_sum = 0.0;
	double **Dk_ptr = getDk();
	double *Vc_ptr = getVc();
	double **Vk_ptr = getVk();

	double *interp_f = grid_ops->getInterpCoeffs();

	double l_coeff = 0.0;
	double r_coeff = 0.0;

	double tmp1,tmp2;
	//for(int i=0;i<num_pts;i++){
	for(int i=1;i<num_pts;i++){
		local_sum = 0.0;
		//W_inv = 1/W[i];
		r_coeff = interp_f[i];
		l_coeff = (1.0 - r_coeff);
		W_inv = 1/W_face[i];
		for(int k=0;k<num_species;k++){
			tmp1 = (l_coeff*Dk_ptr[i-1][k]+r_coeff*Dk_ptr[i][k])*del_Xk_Xk[i][k];
			tmp2 = l_coeff*Xk[i-1][k]+r_coeff*Xk[i][k];

			local_sum += tmp1*Wk[k]*W_inv*tmp2;
			//local_sum += 0.25*(Dk_ptr[i][k]+Dk_ptr[i-1][k])*Wk[k]*W_inv*del_Xk_Xk[i][k]*(Xk[i][k]+Xk[i-1][k]);

			//local_sum += Dk_ptr[i][k]*Wk[k]*W_inv*del_Xk_Xk[i][k]*Xk[i][k];
			//Vk_ptr[i][k] = -Dk_ptr[i][k]*del_Xk_Xk[i][k] + Vc_ptr[i];
			Vk_ptr[i][k] = -tmp1;
		}
		Vc_ptr[i]=local_sum;
	}

	//for(int i=0;i<num_pts;i++)
    /*for(int i=1;i<num_pts;i++)
		for(int k=0;k<num_species;k++)
            Vk_ptr[i][k] += Vc_ptr[i];*/
			//Vk_ptr[i][k] = -0.5*(Dk_ptr[i][k]+Dk_ptr[i-1][k])*del_Xk_Xk[i][k] + Vc_ptr[i];
			//Vk_ptr[i][k] = -Dk_ptr[i][k]*del_Xk_Xk[i][k] + Vc_ptr[i];

}


//void CanteraTransport::estimateBoundaryProperties(double *Wk,double *W_face,double **del_Xk_Xk,FaceData *inlet,FaceData *outlet) {
void CanteraTransport::estimateBoundaryProperties(double *Wk,double **del_Xk_Xk,FaceData *inlet,FaceData *outlet,double *Le_k) {


	//for(int k=0;k<num_species;k++)
	//	D_k[i][k]=lambda[i]/(rho[i]*Cp[i]*Le_k[k]);

	double local_sum = 0.0;

	double W_inv = 1.0/inlet->W;
	for(int k=0;k<num_species;k++)
		//local_sum += (inlet->lambda/(inlet->rho*inlet->Cp*Le_k[k]))*Wk[k]*W_inv*del_Xk_Xk[0][k]*inlet->Xk[k];
		local_sum += (D_k[0][k])*Wk[k]*W_inv*del_Xk_Xk[0][k]*inlet->Xk[k];

	V_c[0]=local_sum;

	for(int k=0;k<num_species;k++)
		//V_k[0][k] = -((inlet->lambda/(inlet->rho*inlet->Cp*Le_k[k])))*del_Xk_Xk[0][k] + V_c[0];
        V_k[0][k] = -(D_k[0][k])*del_Xk_Xk[0][k];
        //V_k[0][k] = -(D_k[0][k])*del_Xk_Xk[0][k] + V_c[0];


	local_sum = 0.0;
	W_inv = 1.0/outlet->W;
	for(int k=0;k<num_species;k++)
		//local_sum += ((outlet->lambda/(outlet->rho*outlet->Cp*Le_k[k])))*Wk[k]*W_inv*del_Xk_Xk[num_pts][k]*outlet->Xk[k];
		local_sum += (D_k[num_pts-1][k])*Wk[k]*W_inv*del_Xk_Xk[num_pts][k]*outlet->Xk[k];

	V_c[num_pts]=local_sum;

	for(int k=0;k<num_species;k++)
		//V_k[num_pts][k] = -((outlet->lambda/(outlet->rho*outlet->Cp*Le_k[k])))*del_Xk_Xk[num_pts][k] + V_c[num_pts];
        V_k[num_pts][k] = -(D_k[num_pts-1][k])*del_Xk_Xk[num_pts][k];
        //V_k[num_pts][k] = -(D_k[num_pts-1][k])*del_Xk_Xk[num_pts][k] + V_c[num_pts];


}
/*
double** CanteraTransport::getDk() const {
	return D_k;
}

double** CanteraTransport::getLambdak() const {
	return lambda_k;
}

double** CanteraTransport::getMuK() const {
	return mu_k;
}

double* CanteraTransport::getVc() const {
	return V_c;
}


double** CanteraTransport::getVk() const {
	return V_k;
}*/


} /* namespace Flame */
