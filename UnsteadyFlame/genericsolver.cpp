#include "genericsolver.h"
#include "PhysicalConstants.h"
#include <iostream>
namespace Flame{


GenericSolver::GenericSolver()
{
	start_time = std::clock();
}

}

int Flame::GenericSolver::getNumPts() const {
	return num_pts;
}

void Flame::GenericSolver::setNumPts(int numPts) {
	num_pts = numPts;
}

int Flame::GenericSolver::getNumIter() const {
	return num_iter;
}

void Flame::GenericSolver::setNumIter(int numIter) {
	num_iter = numIter;
}

double Flame::GenericSolver::getSolnTime() const {
	return soln_time;
}

void Flame::GenericSolver::setSolnTime(double solnTime) {
	soln_time = solnTime;
}

double Flame::GenericSolver::getTimeStep() const {
	return time_step;
}

void Flame::GenericSolver::setTimeStep(double timeStep) {
	time_step = timeStep;
}

double Flame::GenericSolver::getDomainLength() const {
	return domain_length;
}

void Flame::GenericSolver::setDomainLength(double domainLength) {
	domain_length = domainLength;
}

double Flame::GenericSolver::getGridSpacing() const {
	return grid_spacing;
}

void Flame::GenericSolver::setGridSpacing(double gridSpacing) {
	grid_spacing = gridSpacing;
}

int Flame::GenericSolver::getElapsedIter() const {
	return elapsed_iter;
}

void Flame::GenericSolver::setElapsedIter(int elapsedIter) {
	elapsed_iter = elapsedIter;
}

Flame::GenericSolver::~GenericSolver() {

	std::cout<<"Generic solver's destructor called"<<std::endl;
	std::cout<<"Deleting objects.."<<std::endl;

	for(int k=0;k<num_dim1;k++)
		delete [] qn[k];
	delete qn;

	for(int k=0;k<num_dim1;k++)
		delete [] rhs[k];
	delete rhs;

	delete fluid;
	delete integrator;
	delete numerics;
	delete grid_obj;
	delete grid_ops;

	std::cout<<"objects Deallocated.."<<std::endl;
}
