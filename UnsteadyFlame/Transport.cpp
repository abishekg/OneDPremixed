/*
 * Transport.cpp
 *
 *  Created on: Jun 11, 2014
 *      Author: abishekg
 */

#include "Transport.h"
#include <cmath>
#include <iostream>
using namespace std;

namespace Flame {


Transport::Transport() {
}


Transport::Transport(int nx,int nk) {

	num_pts = nx;
	num_species = nk;

	lambda_k = new double*[num_pts];
	for(int i=0;i<num_pts;i++)
		lambda_k[i] = new double[num_species];

	mu_k = new double*[num_pts];
	for(int i=0;i<num_pts;i++)
		mu_k[i] = new double[num_species];

	mu_sqroot_k = new double*[num_pts];
	for(int i=0;i<num_pts;i++)
		mu_sqroot_k[i] = new double[num_species];


	D_k = new double*[num_pts];
	for(int i=0;i<num_pts;i++)
		D_k[i] = new double[num_species];

	V_k = new double*[num_pts+1];
	for(int i=0;i<num_pts+1;i++)
		V_k[i] = new double[num_species];

	V_c = new double[num_pts+1];

	V_low_k  = new double*[num_species];
	for(int i=0;i<num_species;i++)
		V_low_k[i] = new double[4];

	V_high_k  = new double*[num_species];
	for(int i=0;i<num_species;i++)
		V_high_k[i] = new double[4];

	C_low_k  = new double*[num_species];
	for(int i=0;i<num_species;i++)
		C_low_k[i] = new double[4];

	C_high_k  = new double*[num_species];
	for(int i=0;i<num_species;i++)
		C_high_k[i] = new double[4];

	initializePolynomials();


	// TODO Auto-generated constructor stub

}

Transport::~Transport() {

	for(int i=0;i<num_pts;i++)
		delete [] lambda_k[i];
	delete lambda_k;

	for(int i=0;i<num_pts;i++)
		delete [] mu_k[i];
	delete mu_k;

	for(int i=0;i<num_pts;i++)
		delete [] mu_sqroot_k[i];
	delete mu_sqroot_k;


	for(int i=0;i<num_pts;i++)
		delete [] D_k[i];
	delete D_k;


	for(int i=0;i<num_pts+1;i++)
		delete [] V_k[i];
	delete V_k;

	delete [] V_c;
	//delete V_c;

	for(int i=0;i<num_species;i++)
		delete [] V_low_k[i];
	delete V_low_k;

	for(int i=0;i<num_species;i++)
		delete [] V_high_k[i];
	delete V_high_k;

	for(int i=0;i<num_species;i++)
		delete [] C_low_k[i];
	delete C_low_k;

	for(int i=0;i<num_species;i++)
		delete [] C_high_k[i];
	delete C_high_k;

	cout<<"Deallocating Base Transport objects.."<<endl;
	// TODO Auto-generated destructor stub
}

void Transport::estimateSpeciesViscosityPoly(int num_low_T,int num_high_T,int *low_T_indices,int *high_T_indices,double* T_ptr) {

	double T;
	double T_inv;

	for(int j=0;j<num_low_T;j++){
		int i = low_T_indices[j];
		T = T_ptr[i];
		T_inv = 1.0/T;
		for(int k=0;k<num_species;k++){
			mu_k[i][k] = 1.0e-7*exp(V_low_k[k][0]*log(T) +V_low_k[k][1]*T_inv + V_low_k[k][2]*T_inv*T_inv + V_low_k[k][3]);
			mu_sqroot_k[i][k] = sqrt(mu_k[i][k]);
		}
	}


	for(int j=0;j<num_high_T;j++){
		int i = high_T_indices[j];
		T = T_ptr[i];
		T_inv = 1.0/T;
		for(int k=0;k<num_species;k++){
			mu_k[i][k] = 1.0e-7*exp(V_high_k[k][0]*log(T) +V_high_k[k][1]*T_inv + V_high_k[k][2]*T_inv*T_inv + V_high_k[k][3]);
			mu_sqroot_k[i][k] = sqrt(mu_k[i][k]);
		}
	}
}

void Transport::getTransportProperties(int num_low_T,int num_high_T,int *low_T_indices,int *high_T_indices,double* T_ptr) {

	estimateSpeciesViscosityPoly(num_low_T,num_high_T,low_T_indices,high_T_indices,T_ptr);
	estimateSpeciesConductivityPoly(num_low_T,num_high_T,low_T_indices,high_T_indices,T_ptr);
}


void Transport::getDiffusionProperties(double *lambda,double *rho,double *Cp,double *Le_k,double *Wk,double *W,double **del_Xk_Xk,double **Xk) {

	estimateSpeciesMolecularDiffusivity(lambda,rho,Cp,Le_k);
	estimateSpeciesDiffusivityVelocity(Wk,W,del_Xk_Xk,Xk);
}


void Transport::estimateSpeciesConductivityPoly(int num_low_T,int num_high_T,int *low_T_indices,int *high_T_indices,double* T_ptr) {

	double T;
	double T_inv;

	for(int j=0;j<num_low_T;j++){
		int i = low_T_indices[j];
		T = T_ptr[i];
		T_inv = 1.0/T;
		for(int k=0;k<num_species;k++){
			lambda_k[i][k]= 1.0e-4*exp( C_low_k[k][0]*log(T)+C_low_k[k][1]*T_inv + C_low_k[k][2]*T_inv*T_inv + C_low_k[k][3] );
		}
	}


	for(int j=0;j<num_high_T;j++){
		int i = high_T_indices[j];
		T = T_ptr[i];
		T_inv = 1.0/T;
		for(int k=0;k<num_species;k++){
			lambda_k[i][k]= 1.0e-4*exp( C_high_k[k][0]*log(T)+C_high_k[k][1]*T_inv + C_high_k[k][2]*T_inv*T_inv + C_high_k[k][3] );
		}
	}
}

void Transport::estimateSpeciesMolecularDiffusivity(double *lambda,double *rho,double *Cp,double *Le_k) {

	for(int i=0;i<num_pts;i++)
		for(int k=0;k<num_species;k++)
			D_k[i][k]=(lambda[i]/(rho[i]*Cp[i]))/Le_k[k];

}

void Transport::initializePolynomials() {

	//double high_coeffs[3][6];
	//double low_coeffs[3][6];

	double low_coeffs[3][4]  ={ {0.74553182,43.555109,-3257.9340,0.13556243},
								{0.60916180 ,-52.244847, -599.74009, 2.0410801},
								{0.49966928, -697.84297, 88274.722,  3.0878979}};


	/*low_coeffs[0]  = {0.74553182,43.555109,-3257.9340,0.13556243};
	low_coeffs[1] = {0.60916180 ,-52.244847, -599.74009, 2.0410801};
	low_coeffs[2] = {0.49966928, -697.84297, 88274.722,  3.0878979};*/

	for(int k=0;k<3;k++)
		for(int i=0;i<4;i++)
			V_low_k[k][i]=low_coeffs[k][i];

	double high_coeffs[3][4]  ={{0.96730605, 679.31897, -210251.79, -1.8251697},
							   {0.72216486, 175.50839, -57974.816,  1.0901044},
							   {0.58963330, -538.75152, 54745.230, 2.3409644}};

	/*high_coeffs[0] = {0.96730605, 679.31897, -210251.79, -1.8251697};
	high_coeffs[1] = {0.72216486, 175.50839, -57974.816,  1.0901044};
	high_coeffs[2] = {0.58963330, -538.75152, 54745.230, 2.3409644};*/

	for(int k=0;k<3;k++)
		for(int i=0;i<4;i++)
			V_high_k[k][i]=high_coeffs[k][i];

	double low_coeffs1[3][4] = {{1.0240124,  297.09752,   -31396.363, 1.0560824},
				  {0.77238828,   6.9293259, -5900.8518, 1.2202965},
				  {1.1322991,  -512.13867,  99913.498 , -0.52900911}};


	/*low_coeffs[0] = {1.0240124,  297.09752,   -31396.363, 1.0560824};
	low_coeffs[1] = {0.77238828,   6.9293259, -5900.8518, 1.2202965};
	low_coeffs[2] = {1.1322991,  -512.13867,  99913.498 , -0.52900911};*/

	for(int k=0;k<3;k++)
		for(int i=0;i<4;i++)
			C_low_k[k][i]=low_coeffs1[k][i];

	double high_coeffs1[3][4] = {{1.0611992 , 258.85783 , 6316.3191, 0.79973205},
					{0.90875998, 289.86028, -79180.433, 0.068622859},
					{0.50036257, -1719.4289, 387590.61, 4.7558670}};

	/*high_coeffs[0] = {1.0611992 , 258.85783 , 6316.3191, 0.79973205};
	high_coeffs[1] = {0.90875998, 289.86028, -79180.433, 0.068622859};
	high_coeffs[2] = {0.50036257, -1719.4289, 387590.61, 4.7558670};*/

	for(int k=0;k<3;k++)
		for(int i=0;i<4;i++)
			C_high_k[k][i]=high_coeffs1[k][i];

}

//void Transport::estimateSpeciesDiffusivityVelocity(double *Wk,double *W,double **del_Xk_Xk,double **Xk) {
void Transport::estimateSpeciesDiffusivityVelocity(double *Wk,double *W_face,double **del_Xk_Xk,double **Xk) {

	double W_inv = 0.0;
	double local_sum = 0.0;
	double **Dk_ptr = getDk();
	double *Vc_ptr = getVc();
	double **Vk_ptr = getVk();
	for(int i=1;i<num_pts;i++){
		local_sum = 0.0;
		W_inv = 1/W_face[i];
		for(int k=0;k<num_species;k++){
			local_sum += 0.25*(Dk_ptr[i][k]+Dk_ptr[i-1][k])*Wk[k]*W_inv*del_Xk_Xk[i][k]*(Xk[i][k]+Xk[i-1][k]);
			//local_sum += Dk_ptr[i][k]*Wk[k]*W_inv*del_Xk_Xk[i][k]*Xk[i][k];
			//Vk_ptr[i][k] = -Dk_ptr[i][k]*del_Xk_Xk[i][k] + Vc_ptr[i];
		}
		Vc_ptr[i]=local_sum;
	}

	for(int i=1;i<num_pts;i++)
		for(int k=0;k<num_species;k++)
			Vk_ptr[i][k] = -0.5*(Dk_ptr[i][k]+Dk_ptr[i-1][k])*del_Xk_Xk[i][k] + Vc_ptr[i];
			//Vk_ptr[i][k] = -Dk_ptr[i][k]*del_Xk_Xk[i][k] + Vc_ptr[i];

}

double** Transport::getDk() const {
	return D_k;
}

double** Transport::getLambdak() const {
	return lambda_k;
}

double** Transport::getMuK() const {
	return mu_k;
}

double** Transport::getMuSqrootK() const {
	return mu_sqroot_k;
}

double* Transport::getVc() const {
	return V_c;
}

double** Transport::getVk() const {
	return V_k;
}



void Transport::estimateBoundaryProperties(double* Wk,
		double** del_Xk_Xk, FaceData* inlet, FaceData* outlet,double *Le_k) {

	double local_sum = 0.0;

	double W_inv = 1.0/inlet->W;
	for(int k=0;k<num_species;k++)
		local_sum += (inlet->lambda/(inlet->rho*inlet->Cp*Le_k[k]))*Wk[k]*W_inv*del_Xk_Xk[0][k]*inlet->Xk[k];

	V_c[0]=local_sum;

	for(int k=0;k<num_species;k++)
		V_k[0][k] = -((inlet->lambda/(inlet->rho*inlet->Cp*Le_k[k])))*del_Xk_Xk[0][k] + V_c[0];


	local_sum = 0.0;
	W_inv = 1.0/outlet->W;
	for(int k=0;k<num_species;k++)
		local_sum += ((outlet->lambda/(outlet->rho*outlet->Cp*Le_k[k])))*Wk[k]*W_inv*del_Xk_Xk[num_pts][k]*outlet->Xk[k];

	V_c[num_pts]=local_sum;

	for(int k=0;k<num_species;k++)
		V_k[num_pts][k] = -((outlet->lambda/(outlet->rho*outlet->Cp*Le_k[k])))*del_Xk_Xk[num_pts][k] + V_c[num_pts];

}


} /* namespace Flame */
