#include "solver.h"
#include "Config.h"
#include "Mixture.h"
#include "ArrayUtils.h"
//#include "ClassDefs.h"
//#include "ClassDefs2.h"
#include "IdealGas.h"
//#include "LaxFriedrichsFluxSplit.h"
#include "FiniteVolume.h"
//#include "OneStepKinetics.h"
#include "rk4integrator.h"
#include "PhysicalConstants.h"
#include "DataIO.h"
#include <numeric>
#include <iostream>
#include <iterator>
//#include <memory>
#include <cmath>
//#include <limits>
#include <algorithm>

//#define safe_zero 1.e-60   // for log operations

//#define<limits>
//#define safe_zero std::numeric_limits<double>::min()


namespace Flame {

Solver::Solver()
{
	record_index = 0;
	configFile = "Flame.ini";



}

Solver::Solver(string fileName,bool genSettings)
{
	record_index = 0;


	if(genSettings){
		Config cfg(fileName);
		cfg.writeSettingsFile("");
	}else
		configFile = fileName;



}


void Solver::updateVariables() {


	//cout<<"Updating variables"<<endl;

	getPrimitiveVars();
	fluid->updateFluidProperties(grid_ops);
}


void Solver::evalPreTimeAdvance() {

	//cout<<"eval pre time advance"<<endl;

	//updateVariables();
	estimateTimeStepSize();
	//estimateFlameParams(time_step);
}



void Solver::evalRHS(int substep,double tstep_frac){


	//cout<<"eval rhs"<<endl;
	/*getPrimitiveVars();

	fluid->updateFluidProperties(grid_ops);

	if(substep==1){
		estimateTimeStepSize();
		estimateFlameParams(time_step);
	}*/

	//kinetics->getProductSourceTerm();
	setBoundaryConditions(tstep_frac*time_step);

	numerics->Discretize(qn,rhs);
}


void Solver::evalPreSubstep() {

	//cout<<"eval pre sub step"<<endl;
	//estimateFlameParams(time_step);

	if(soln_time>5.0e-5)
		//estimateFlameParams3(time_step);
		estimateFlameParams(time_step);
	//estimateFlameParams2(time_step);
}


void Solver::evalPostSubstep() {

	//cout<<"eval post sub step"<<endl;
	updateVariables();
}



void Solver::getPrimitiveVars(){

	double *rho_ptr = fluid->getRho();
	//Set density
	for(int i=0;i<num_pts;i++)
		rho_ptr[i]=qn[0][i];
		//qn[0][i]=*(varptr++);

	//if(substep==1)
	//	for(int i=0;i<num_pts;i++) qn[1][i] += rho_ptr[i]*u_add;

	double *u_ptr = fluid->getU();
	//Set convective velocity
	for(int i=0;i<num_pts;i++)
		u_ptr[i]=qn[1][i]/qn[0][i];

	//if(substep==1)
	for(int i=0;i<num_pts;i++) u_ptr[i] += u_add;

	double **Ykptr = fluid->getMixtureObj()->getYk();

	// Set Mass fraction
	for(int k=0;k<num_species;k++)
		for(int i=0;i<num_pts;i++)
			Ykptr[i][k]=min(max(qn[2+k][i]/qn[0][i],safe_zero),1.0);

	// Normalize??
	double norm = 0.0;
	for(int i=0;i<num_pts;i++){
		norm=accumulate(num_species,Ykptr[i]);
		divide_each(num_species,Ykptr[i],norm);
	}

	/*for(int k=0;k<num_species;k++)
		for(int i=0;i<num_pts;i++)
			Ykptr[i][k]=min(Ykptr[i][k],1.0);*/

	/// 2nd approach

	/*for(int k=0;k<num_species-1;k++)
		for(int i=0;i<num_pts;i++)
			Ykptr[i][k]=qn[2+k][i]/qn[0][i];

	for(int k=0;k<num_species-1;k++)
		for(int i=0;i<num_pts;i++)
			if(Ykptr[i][k]<0.0)Ykptr[i][k]=0.0;

	for(int i=0;i<num_pts;i++)
		Ykptr[i][num_species-1]= max(1.0 - std::accumulate(Ykptr[i],Ykptr[i]+num_species-1,0.0),0.0);
*/

	double *et_ptr = fluid->getE_t();
	double *e_ptr = fluid->getE();

	//Set total energy
	for(int i=0;i<num_pts;i++)
		et_ptr[i]=qn[num_species+2][i]/qn[0][i];

	//Set internal energy ( Sensible + Chemical)
	for(int i=0;i<num_pts;i++)
		e_ptr[i] = et_ptr[i] - 0.5*u_ptr[i]*u_ptr[i];

	fluid->getMixtureObj()->estimateMixtureMolecularWeight(grid_ops);

	fluid->getTemperaturefromInternalEnergy();

	fluid->getPresurefromDensityTemp();

	// Wrong because it doesn't have recent value of Cp or Cp_m
	//fluid->getSpeedofSound();

	//if(elapsed_iter>0){
		//estimateFlameParams();
		//superposeVelocity();
	//}

}

void Solver::constructConservedVars(){

}

void Solver::Initialize() {

	Config cfg;
	ptree config_ptree = cfg.readSettingsFile(configFile);
	settings_str = cfg.getSettingsStr();

	T_fit_low = config_ptree.get<double>("Transport.T_fit_low");
	T_fit_mid = config_ptree.get<double>("Transport.T_fit_mid");
	T_fit_high = config_ptree.get<double>("Transport.T_fit_high");

	num_pts = config_ptree.get<int>("Grid.numNodes");
	domain_length = config_ptree.get<double>("Grid.domainLength");
	flame_init_pos = config_ptree.get<double>("Grid.flameInitPos");

	int istretch =  config_ptree.get<int>("Grid.stretch");
	bool stretch=false;

	if(istretch==1) stretch=true;
	cout<<"Grid stretching :"<<stretch<<std::endl;

	double flame_pos_frac = config_ptree.get<double>("Grid.flamePosLFrac");
	double refine_len_frac = config_ptree.get<double>("Grid.refineLenFrac");
	double refine_pts_frac = config_ptree.get<double>("Grid.refinePtsFrac");
	double rl = config_ptree.get<double>("Grid.leftRefineRatio");
	double rr = config_ptree.get<double>("Grid.rightRefineRatio");

	//num_iter = config_ptree.get<int>("Solver.nTimesteps");
	max_sim_time = config_ptree.get<double>("Solver.maxSimTime");
	FlameVarsFile = config_ptree.get<string>("Solver.FlameVarsFile");
	RecordFile = config_ptree.get<string>("Solver.RecordFile");
	AnimFile = config_ptree.get<string>("Solver.AnimFile");
	ctml_file = config_ptree.get<string>("Solver.ctmlFile");
	canteramechid = config_ptree.get<string>("Solver.canteraMechanismId");

	//grid_spacing= domain_length/(num_pts - 1);
	soln_time = 0.0;
	elapsed_iter = 0;

	cantera_obj = new CanteraWrapper(ctml_file,canteramechid);
	cantera_obj->initializeChemistry();
	cantera_obj->initializeTransportFits(T_fit_low,T_fit_mid,T_fit_high);
	//cantera_obj->initializeTransportFits(200.0,1000.0,5000.0);

	num_species = cantera_obj->getNumSpecies();

	num_dim1 = num_species+3;
	num_dim2 = num_pts;

	num_record = 7;

	u_err = 0.0;

	InitializeArrays();

	//int num_elements = num_dim1*num_dim2;
	//integrator = new IntegratorClass(num_dim1,num_dim2);
	integrator = new RK4Integrator(num_dim1,num_dim2);
	//integrator = new EulerIntegrator(num_dim1,num_dim2);

	//fluid  = new IdealGas(num_pts,num_species);
	fluid  = new IdealGas(num_pts,cantera_obj);
	//fluid  = new FluidsClass(num_pts,cantera_obj);

	grid_obj = new Grid(num_pts,domain_length,stretch,flame_pos_frac,refine_len_frac,refine_pts_frac,rl,rr);

	grid_spacing = *min_element(grid_obj->getDxPtr(),grid_obj->getDxPtr()+num_pts);
	cout<<"Smallest grid spacing :"<<grid_spacing<<std::endl;
	//grid_ops = new GridOperations(grid_obj);

	//inlet = new FaceData(num_species,num_pts-1);
	//outlet = new FaceData(num_species,0);


	inlet = new FaceData(num_species,0);
	outlet = new FaceData(num_species,num_pts+1);

	inlet->u = config_ptree.get<double>("Inlet.u");
	inlet->P = config_ptree.get<double>("Inlet.P");
	inlet->T = config_ptree.get<double>("Inlet.T");

	T_u_init = inlet->T;

	outlet->u = config_ptree.get<double>("Outlet.u");
	outlet->P = config_ptree.get<double>("Outlet.P");
	outlet->T = config_ptree.get<double>("Outlet.T");

	T_b_init = outlet->T;

	k_T = config_ptree.get<double>("BoundaryConditions.k_T");
	k_u = config_ptree.get<double>("BoundaryConditions.k_u");
	k_P = config_ptree.get<double>("BoundaryConditions.k_P");

	phi = config_ptree.get<double>("Chemistry.phi");
	ox_air_frac = config_ptree.get<double>("Chemistry.oxidizerAirFraction");
	n_fuel_ox_st = config_ptree.get<double>("Chemistry.fuelOxFractionStoich");
	fuel_species = config_ptree.get<string>("Chemistry.fuelSpecies");
	ox_species = config_ptree.get<string>("Chemistry.oxidizerSpecies");
	mbody_species = config_ptree.get<string>("Chemistry.airSpecies");


	grid_ops = new GridOperations(grid_obj,inlet,outlet);

	//numerics = new LaxFriedrichsFluxSplit(num_pts,num_species,grid_spacing);
	numerics = new FiniteVolume(fluid,grid_obj,grid_ops,inlet,outlet);
	//numerics = new NumericsClass(fluid,grid_obj,grid_ops,inlet,outlet);

	setInitialConditions();
	//kinetics_obj = new OneStepKinetics();
	getPrimitiveVars();

	fluid->getMixtureObj()->estimateXkfromYk(grid_ops);
}

void Solver::setInitialConditions() {

    /*double phi = 1.0;
	string fuel_species = "H2";
	string ox_species = "O2";
	string mbody_species = "AR";*/


	double *W_k = fluid->getMixtureObj()->getW_k();


	double num_moles_1[num_species];
	double num_moles_2[num_species];

	cantera_obj->initializeStoichiometry(phi,ox_air_frac,n_fuel_ox_st,&num_moles_1[0],&num_moles_2[0],W_k,fuel_species,ox_species,mbody_species);


	//std::copy(num_moles_1,num_moles_1+num_species,std::ostream_iterator<double>(std::cout,","));

	std::cout<<" Num moles 1:"<<std::endl;
	printdoubleArray(&num_moles_1[0],num_species);
	std::cout<<" Num moles 2:"<<std::endl;
	printdoubleArray(&num_moles_2[0],num_species);


	/*//double ER = 1.0;
	//double ER = 0.7;
	//double FA_st = 0.12599;
	double FA_st = 2.0;  //for H2-O2
	double ER = 1.5;
	double N_Ox = 0.5;
	double N_F = ER*N_Ox*FA_st;

	double N_OxB = safe_zero;
	double N_FB = safe_zero;

	if(ER>1.0)
		N_FB = N_F - (FA_st)*N_Ox;
	else if(ER<1.0)
		N_OxB = N_Ox - N_F/FA_st;

	// H2,O2,H2O
	//double num_moles_1[3] = {0.3,0.4,0.8};
	//double num_moles_2[3] = {0.3,0.4,0.8};
	double num_moles_1[3] = {N_F,N_Ox,safe_zero};
	double num_moles_2[3] = {N_FB,N_OxB,1.0};*/

	//H2 H O O2 OH H2O HO2 H2O2 AR    - h2o2.xml
	//double num_moles_1[9] = {ER,0.0,0.0,0.5,0.0,0.0,0.0,0.0,3.0};
	//double num_moles_2[9] = {0.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0,3.0};

	double sum_moles_1 = 0.0;
	double sum_moles_2 = 0.0;


    for(int i=0;i<num_species;i++){
    	sum_moles_1 += num_moles_1[i];
    	sum_moles_2 += num_moles_2[i];
    }


	//double Xk_1[] = std::transform(num_moles_1,num_moles_1+3)
    double Xk_1[num_species];
	double Xk_2[num_species];

	for(int i=0;i<num_species;i++){
		Xk_1[i] = max(num_moles_1[i]/sum_moles_1,safe_zero);
		Xk_2[i] =  max(num_moles_2[i]/sum_moles_2,safe_zero);

		inlet->Xk[i] = Xk_1[i];
		outlet->Xk[i] = Xk_2[i];
	}
	//std::copy(inlet->Xk,inlet->Xk+num_species,&Xk_2[0]);
	//std::copy(outlet->Xk,inlet->Xk+num_species,&Xk_1[0]);




	/*W_k[0]=2.01588*0.001;
	W_k[1]=31.99880*0.001;
	W_k[2]=18.01528*0.001;*/

	double local_sum =0.0;
	for(int k=0;k<num_species;k++)
	   local_sum += W_k[k]*Xk_1[k];
	double W1 = local_sum;

	local_sum = 0.0;
	for(int k=0;k<num_species;k++)
	   local_sum += W_k[k]*Xk_2[k];
	double W2 = local_sum;

	inlet->W = W1;
	outlet->W = W2;

	double Yk_1[num_species],Yk_2[num_species];
	for(int k=0;k<num_species;k++){
		Yk_1[k] = max(W_k[k]*num_moles_1[k]/(W1*sum_moles_1),safe_zero);
		Yk_2[k] = max(W_k[k]*num_moles_2[k]/(W2*sum_moles_2),safe_zero);

		inlet->Yk[k] = Yk_1[k];
		outlet->Yk[k] = Yk_2[k];

	}


	//double T1 = 500.0;
	//double T2 = 500.0;

	//double T1 = 300.0;
	//double T2 =2000.0;

	//T_inf_in = T1;
	//inlet->T = T1;
	//outlet->T = T2;

	T_inf_in = inlet->T;


	//double P1 = 1.0*PhysicalConstants::OneAtm;
	//double P1 = P_inf;

	P_inf = outlet->P;
	P_imp = P_inf;
	P_in = P_inf;


	//double P2 = P1;

	//double u2=-1.0,u1=-1.0;
	//double u2=1.0,u1=2.0;
	//double u2=3.0,u1=3.0;
	//double u2=0.0,u1=0.0;
	//double R = PhysicalConstants::R_si;

	//double rho1 = fluid->EstimateRhofromPTW(P1,T1,W1);

	//double rho2 = fluid->EstimateRhofromPTW(P2,T2,W2);

	inlet->rho = fluid->EstimateRhofromPTW(inlet->P,inlet->T,inlet->W);
	outlet->rho = fluid->EstimateRhofromPTW(outlet->P,outlet->T,outlet->W);

	//double P2 = rho2*R/W2*T2;

	/*rho_inf = rho2;
	rho_inf_in = rho1;
	rho_in = rho1;*/

	rho_inf = outlet->rho;
	rho_inf_in = inlet->rho;
	rho_in = inlet->rho;


	//u_inf = 20.0;

	u_inf_in = 1.0;
	u_inf = 0.1;

	//u_inf = 0.1;
	//u_inf = 5.0;

	//u_in = u1;
	u_in = inlet->u;

	u_imp = u_inf;

	rho_imp = rho_inf;

	u_imp_in = u_in;
	rho_imp_in = rho_in;
	P_imp_in = P_in;



	//inlet->P = P1;
	//outlet->P = P2;

	//inlet->rho = rho1;
	//outlet->rho = rho2;

	//inlet->u = u1;
	//outlet->u = u2;


    //int fl_ind = (int)(0.45*(float)num_pts);
	//int fl_ind = (int)(0.5*(float)num_pts);
    //int fl_ind = (int)(0.25*(float)num_pts);
	int fl_ind = (int)(flame_init_pos*(float)num_pts);

	 x_fc = grid_obj->getXPosition(fl_ind);
	 u_fc = 0.0;
	 u_add = 0.0;
	 u_err_prod = 0.0;

	 //double e1  = fluid->EstimateInternalEnergyFromTYk(T1,Yk_1,W1,P1,rho1);
	 //double e2  = fluid->EstimateInternalEnergyFromTYk(T2,Yk_2,W2,P2,rho2);

	 inlet->e = fluid->EstimateEnthalpyInternalEnergyFromTYk(inlet->T,inlet->Yk,inlet->W,inlet->P,inlet->rho,inlet->hk);
 	 outlet->e = fluid->EstimateEnthalpyInternalEnergyFromTYk(outlet->T,outlet->Yk,outlet->W,outlet->P,outlet->rho,outlet->hk);


	 //double Cp1 = fluid->EstimateSpecificHeatFromTYk(T1,Yk_1);
	 //double Cp2 = fluid->EstimateSpecificHeatFromTYk(T2,Yk_2);

	 inlet->Cp =fluid->EstimateSpecificHeatFromTYk(inlet->T,inlet->Yk);
	 outlet->Cp = fluid->EstimateSpecificHeatFromTYk(outlet->T,outlet->Yk);

	 inlet->c = fluid->getSpeedofSound(inlet->T,inlet->W,inlet->Cp);
	 outlet->c = fluid->getSpeedofSound(outlet->T,outlet->W,outlet->Cp);

	 c_inf = outlet->c;
	 c_imp = c_inf;

	 c_in = inlet->c;
	 c_imp_in = c_in;

	 //outlet->w3 = u_inf - P_inf/(rho_inf*c_inf);
	 outlet->w3 = outlet->u - outlet->P/(outlet->rho*outlet->c);
	 //w3_init = outlet->w3;
	 w3_init = u_inf - P_inf/(rho_inf*c_inf);

	 w3_imp = w3_init;
	 std::cout<<"w3 init::"<<w3_init<<std::endl;

	 w1_err_prod = 0.0;
	 w2_err_prod = 0.0;
	 w3_err_prod = 0.0;


	double *rho_ptr= fluid->getRho();
	double *u_ptr= fluid->getU();
	double *T_ptr= fluid->getT();
	double *P_ptr= fluid->getP();
	double *e_ptr= fluid->getE();
	double *W_ptr= fluid->getMixtureObj()->getW();
	double **Xk_ptr = fluid->getMixtureObj()->getXk();
	double **Yk_ptr = fluid->getMixtureObj()->getYk();
	double *Cp_ptr = fluid->getMixtureObj()->getCp();

	for(int i=0;i<fl_ind;i++)
		rho_ptr[i] = inlet->rho;

	for(int i=0;i<fl_ind;i++)
		u_ptr[i] = inlet->u;

	for(int i=0;i<fl_ind;i++)
		T_ptr[i] = inlet->T;

	for(int i=0;i<fl_ind;i++)
		P_ptr[i] = inlet->P;

	for(int i=0;i<fl_ind;i++)
		e_ptr[i] = inlet->e;

	for(int i=0;i<fl_ind;i++)
		Cp_ptr[i] = inlet->Cp;

	for(int k=0;k<num_species;k++)
		for(int i=0;i<fl_ind;i++)
			Xk_ptr[i][k]   = inlet->Xk[k];

	for(int k=0;k<num_species;k++)
		for(int i=0;i<fl_ind;i++)
			Yk_ptr[i][k]   = inlet->Yk[k];

	for(int i=0;i<fl_ind;i++)
		W_ptr[i] = inlet->W;

	////////////////////////////////////////////////////////
	int offset = num_pts/10;
	//int offset = num_pts/20;
	//int offset = 200;

	double *node_pos= grid_obj->getX();

	//double fac[offset];
	double sigma[offset];
	double x0 = node_pos[fl_ind+offset/2];


	//another one : s(x) = 0.5+0.5 tanh((x-a)/b)
	//sigmoid function
	for(int i=0;i<offset;i++)
		//sigma[i] = 0.5+0.5*tanh((node_pos[fl_ind+i]-1.0)/1.0);
		sigma[i] =  1./(1.0+exp(-(node_pos[fl_ind+i] - x0)/(0.01*(node_pos[fl_ind+offset]-node_pos[fl_ind]))));
		//sigma[i] =  1./(1.0+exp(-(node_pos[fl_ind+i] - x0)/(0.05*(node_pos[fl_ind+offset]-node_pos[fl_ind]))));
		//fac[i] = (node_pos[fl_ind+i]-node_pos[fl_ind])/(node_pos[fl_ind+offset]-node_pos[fl_ind]);




	for(int i=0;i<offset;i++){
		//sigma[i] =  1./(1+exp(-(node_pos[fl_ind+i] - x0)/(u2-u1)));
		u_ptr[fl_ind+i] = (1.0-sigma[i])*inlet->u + sigma[i]*outlet->u;
	}
		//u_ptr[fl_ind+i] =  u1 + (u2-u1)*fac[i];// T2;

	for(int i=0;i<offset;i++){
		//sigma[i] =  1./(1+exp(-(node_pos[fl_ind+i] - x0)/(T2-T1)));
		T_ptr[fl_ind+i] = (1.0-sigma[i])*inlet->T + sigma[i]*outlet->T;
	}
		//T_ptr[fl_ind+i] =  T1 + (T2-T1)*fac[i];// T2;



	double sum_moles_mid = 0.0;
	double num_moles_mid[num_species];

	for(int i=0;i<offset;i++){
		sum_moles_mid = 0.0;
		for(int k=0;k<num_species;k++){
			//sigma[i] =  1./(1+exp(-(node_pos[fl_ind+i] - x0)/(num_moles_2[k]-num_moles_1[k])));
			num_moles_mid[k] =  (1.0-sigma[i])*num_moles_1[k] + sigma[i]*num_moles_2[k];
			//num_moles_mid[k] = num_moles_1[k] + (num_moles_2[k]-num_moles_1[k])*fac[i];
			sum_moles_mid += num_moles_mid[k];
		}
		local_sum = 0.0;
		for(int k=0;k<num_species;k++){
			Xk_ptr[fl_ind+i][k] = min(max(num_moles_mid[k]/sum_moles_mid,safe_zero),1.0);
			local_sum += W_k[k]*Xk_ptr[fl_ind+i][k];
		}
		W_ptr[fl_ind+i] = local_sum;
		for(int k=0;k<num_species;k++)
			Yk_ptr[fl_ind+i][k] = W_k[k]*num_moles_mid[k]/(W_ptr[fl_ind+i]*sum_moles_mid);
	}

	for(int i=0;i<offset;i++)
		P_ptr[fl_ind+i] = inlet->P;
		//P_ptr[fl_ind+i] = rho_ptr[fl_ind+i]*R/W_ptr[fl_ind+i]*T_ptr[fl_ind+i];

	for(int i=0;i<offset;i++)
		rho_ptr[fl_ind+i] = fluid->EstimateRhofromPTW(P_ptr[fl_ind+i],T_ptr[fl_ind+i],W_ptr[fl_ind+i]);
		//rho_ptr[fl_ind+i] =  rho1*(W_ptr[fl_ind+i]/W1)*(T1/T_ptr[fl_ind+i]);

	for(int i=0;i<offset;i++)
		Cp_ptr[fl_ind+i] =fluid->EstimateSpecificHeatFromTYk(T_ptr[fl_ind+i],Yk_ptr[fl_ind+i]);

	for(int i=0;i<offset;i++)
		e_ptr[fl_ind+i] =  fluid->EstimateInternalEnergyFromTYk(T_ptr[fl_ind+i],Yk_ptr[fl_ind+i],W_ptr[fl_ind+i],P_ptr[fl_ind+i],rho_ptr[fl_ind+i]);


	fl_ind += offset;
//////////////////////////////////////////////////////////


	for(int i=fl_ind;i<num_pts;i++)
		rho_ptr[i] = outlet->rho;

	for(int i=fl_ind;i<num_pts;i++)
		u_ptr[i] = outlet->u;

	for(int i=fl_ind;i<num_pts;i++)
		T_ptr[i] = outlet->T;

	for(int i=fl_ind;i<num_pts;i++)
		P_ptr[i] = outlet->P;

	for(int i=fl_ind;i<num_pts;i++)
		e_ptr[i] = outlet->e;

	for(int i=fl_ind;i<num_pts;i++)
		Cp_ptr[i] = outlet->Cp;

	for(int k=0;k<num_species;k++)
		for(int i=fl_ind;i<num_pts;i++)
			Xk_ptr[i][k]   = outlet->Xk[k];

	for(int k=0;k<num_species;k++)
		for(int i=fl_ind;i<num_pts;i++)
			Yk_ptr[i][k]   = outlet->Yk[k];

	for(int i=fl_ind;i<num_pts;i++)
		W_ptr[i] = outlet->W;


	rho_min = *std::min_element(rho_ptr,rho_ptr+num_pts);
	rho_max = *std::max_element(rho_ptr,rho_ptr+num_pts);




	for(int i=0;i<num_pts;i++)
		qn[0][i] = rho_ptr[i];

	for(int i=0;i<num_pts;i++)
		qn[1][i] = rho_ptr[i]*u_ptr[i];

	for(int k=0;k<num_species;k++)
		for(int i=0;i<num_pts;i++)
			qn[2+k][i] = rho_ptr[i]*Yk_ptr[i][k];

	for(int i=0;i<num_pts;i++)
		qn[2+num_species][i] = rho_ptr[i]*(e_ptr[i] +0.5*u_ptr[i]*u_ptr[i]);




}

void Solver::setBoundaryConditions(double local_tstep) {


		//double K_p = 0.000001;
	    //double K = 0.0001;
	    //double Ti = 0.0005;
	    //double Ti = 0.25e-4;
	    //double Ti = 0.25e-3;

		//double K_p = 0.01;
		//double K_i = 0.0001/soln_time;
		//double err = u_add - u_fc;


		//double Ku = 1.6e-4;
		//double Ku = 1.6e-1;
		//double Ku = 3.6e-4;
		//double Pu = 0.38e-2;
		//double Pu = 0.38e-4;
		//double Pu = 2.55e-4;

		//double K_i = 1.2*K_p/Pu;

		//New params
		//double Ku = 1.6e-4;
		//double Ku = 7.2e-5;
		//double Pu = 3.14e-5;
		//was still oscillating

		//double Ku = 7.2e-2;
		//double Pu = 2.66e-3;

		//double K_p = 0.45*Ku;
		//double K_i = 0.0;
		//double K_i = 1.2*K_p/Pu;

/*
	    //double Ku = 1.0e-4;  3 osci
		double Ku = 1.0e-4;
		//double Ku = 5.0e-5;  no oscc
	    double Pu = 4.8e-4;
	    //double K_p = Ku;
	    double K_p = 0.45*Ku;
	    //double K_i = 0.0;
	    double K_i = 1.2*K_p/Pu;
*/
	    u_err_prev = u_err;
	    u_err = - u_fc;
		//u_err = inlet->u - u_fc;
		du_err_dt = (u_err-u_err_prev)/time_step;





		//if(elapsed_iter>10000)
		//	u_add = K_p*u_err + 1.0/Ti*u_err_prod + K_d*du_err_dt;
		//else
		//	u_add = 1.0e-4*u_err;


		//inlet->rho = inlet->rho;
		//inlet->rho =  qn[0][0];

		//last
		//outlet->rho = qn[0][num_pts-1];

		// last working
		/*if(elapsed_iter>20000){
			u_err_prod += u_err*time_step;
			u_add = K_p*u_err + K_i*u_err_prod;
			//inlet->u = inlet->u + u_add;
			u_imp_in = u_imp_in + u_add;
		}*/


		//if(elapsed_iter>100000)
		//	inlet->u = 2.4;



		//inlet->u = K_p*u_err + K_i*u_err_prod;
		//inlet->u = inlet->u + u_add;
		//std::cout<<"u :"<<inlet->u<<std::endl;

		//inlet->u = 2.0;
		//inlet->u = 0.0;
		//inlet->u = 3.0;
		//inlet->u = qn[1][0]/qn[0][0];

		// for direct
		//u_inf = outlet->u;

		//outlet->u = qn[1][num_pts-1]/qn[0][num_pts-1];

		double *P_ptr= fluid->getP();
		//double P_av = std::accumulate(P_ptr,P_ptr+num_pts, 0.0) / double(num_pts);

		//inlet->P = P_av;
		inlet->P = P_ptr[0];
		//inlet->P = fluid->EstimatePfromTRhoW(inlet->T,inlet->rho,inlet->W);

		//outlet->P = PhysicalConstants::OneAtm;

		double R = PhysicalConstants::R_si;

		double *W_ptr = fluid->getMixtureObj()->getW();
		//inlet->W = W_ptr[0];
		outlet->W = W_ptr[num_pts-1];

		double **Yk_ptr = fluid->getMixtureObj()->getYk();
		//double *Yk_1 = inlet->Yk;
		//double *Yk_2 = Yk_ptr[num_pts-1];

		for(int k=0;k<num_species;k++){
			//inlet->Yk[k] = Yk_ptr[0][k];
			outlet->Yk[k] = Yk_ptr[num_pts-1][k];
		}

		double **Xk_ptr = fluid->getMixtureObj()->getXk();
		for(int k=0;k<num_species;k++){
			//inlet->Xk[k] = Xk_ptr[0][k];
			outlet->Xk[k] = Xk_ptr[num_pts-1][k];
		}

		//double *T_ptr= fluid->getT();
		//inlet->T= fluid->EstimateTfromPRhoW(inlet->P,inlet->rho,inlet->W);
		//inlet->T = 300.0;
		//outlet->T = T_ptr[num_pts-1];
		//outlet->T= fluid->EstimateTfromPRhoW(P_ptr[num_pts-1],outlet->rho,outlet->W);

		//outlet->T= fluid->EstimateTfromPRhoW(outlet->P,outlet->rho,outlet->W);
		//inlet->T = inlet->P*inlet->W/(R*inlet->rho);
		//outlet->T = outlet->P*outlet->W/(R*outlet->rho);

		//double T2 = inlet->T;
		//double P2 = rho2*R*T2/W2;



		 double *T_ptr =fluid->getT();

		 //inlet->Cp = fluid->EstimateSpecificHeatFromTYk(inlet->T,inlet->Yk);

		 //inlet->c = fluid->getSpeedofSound(inlet->T,inlet->W,inlet->Cp);


		 double rho_1 = qn[0][0];
		 double u_1 = qn[1][0]/qn[0][0];
		 double cp_1 = fluid->EstimateSpecificHeatFromTYk(T_ptr[0],Yk_ptr[0]);
		 double c_1 = fluid->getSpeedofSound(T_ptr[0],W_ptr[0],cp_1);
		 double P_1 = P_ptr[0];
		 double T_1 = T_ptr[0];

		 /*double w1_n = -rho_n*u_n/(2.0*c_n) + P_n/(2.0*c_n*c_n);
		 double w2_n = rho_n - P_n/(c_n*c_n);
		 double w3_n = rho_n*u_n/(2.0*c_n) + P_n/(2.0*c_n*c_n);*/

		 double w1_1 = rho_1 - P_1/(c_1*c_1);
		 double w2_1 = u_1 + P_1/(rho_1*c_1);
		 double w3_1 = u_1 - P_1/(rho_1*c_1);

		 /*w1_imp = rho_imp - P_imp/(c_1*c_1);
		 w2_imp = u_imp + P_imp/(rho_1*c_1);
		 w3_imp = u_imp - P_imp/(rho_1*c_1);*/

		 //double P_inf_in = fluid->EstimatePfromTRhoW(T_ptr[0],rho_inf_in,inlet->W);
		 //double P_inf_in = fluid->EstimatePfromTRhoW(inlet->T,rho_inf_in,inlet->W);
		 double P_inf_in = fluid->EstimatePfromTRhoW(T_inf_in,rho_inf_in,inlet->W);

		 w1_imp = rho_inf_in - P_inf_in/(c_1*c_1);
		 w2_imp = u_inf_in + P_inf_in/(rho_1*c_1);
		 w3_imp = u_inf_in - P_inf_in/(rho_1*c_1);


		 double kT_local = 0.0;
		 double ku_local = 0.0;
		 double kP_local = 0.0;


		 //if(elapsed_iter>20000){
		 //if(elapsed_iter>4000){
		 //if(soln_time>1.1e-5){
		 if(soln_time>5.7e-5){
		 //if(soln_time>2.2e-5){

			 kT_local = k_T;
			 ku_local = k_u;
			 kP_local = k_P;

			 //kT_local *= 3.0;
			 //ku_local *= 3.0;
			 //kP_local *= 3.0;


			// k_T = 10.0;
			 //k_u = 100.0;
			// k_u = 300.0;
			// k_P = 950.0;
		 }

		 double sigma_T = kT_local*u_inf_in/domain_length*(2.0*P_inf_in*inlet->W/(R*inlet->T*inlet->T) + rho_inf_in*R/(inlet->W*c_1*c_1));
		 //double sigma_T = 10.0*u_inf_in/domain_length*(2.0*P_inf_in*inlet->W/(R*inlet->T*inlet->T) + rho_inf_in*R/(inlet->W*c_1*c_1));
		 //double sigma_T = 0.0*u_inf_in/domain_length*(2.0*P_inf_in*inlet->W/(R*inlet->T*inlet->T) + rho_inf_in*R/(inlet->W*c_1*c_1));

		 w1_err_prod += sigma_T*(inlet->T - T_inf_in)*local_tstep;
		 //w1_err_prod += sigma_T*(inlet->T - T_inf_in)*time_step;


		 /*double sigma_rho = 10.0*u_inf_in/domain_length;
		 //double sigma_rho = 0.0*u_inf_in/domain_length;
		 w1_err_prod += sigma_rho*inlet->W/(R*P_inf_in)*(inlet->T - T_1)*time_step;*/
		 //w1_err_prod += sigma_rho*(rho_inf_in - rho_1)*time_step;
		 w1_imp = w1_imp + w1_err_prod;



		 double sigma_u = ku_local*u_inf_in/domain_length*rho_inf_in*R/(rho_1*c_1*inlet->W);
		 //double sigma_u = 100.0*u_inf_in/domain_length*rho_inf_in*R/(rho_1*c_1*inlet->W);
		 //double sigma_u = 0.0*u_inf_in/domain_length*rho_inf_in*R/(rho_1*c_1*inlet->W);

		 w2_err_prod += sigma_u*(0.0 - u_fc)*local_tstep;
		 //w2_err_prod += sigma_u*(0.0 - u_fc)*time_step;


		 /*double sigma_u = 40.0*u_inf_in/domain_length;
		 //double sigma_u = 0.0*u_inf_in/domain_length;
		 w2_err_prod += sigma_u*(u_inf_in - u_1)*time_step;*/
		 w2_imp = w2_imp + w2_err_prod;


		 double w1_in = w1_imp;
		 double w2_in = w2_imp;
		 double w3_in = w3_1;

		 inlet->P   = c_1*rho_1/2.0*(w2_in - w3_in);
		 inlet->rho = w1_in + rho_1/(2.0*c_1)*(w2_in - w3_in);
		 inlet->u   = 0.5*(w2_in+w3_in);


		 //inlet->P   = P_ptr[0];
		 //inlet->rho = w1_in + rho_1/(2.0*c_1)*(w2_in - w3_in);
		 //inlet->u   = 0.5*(w2_in+w3_in);


		 //double Cp0_in = fluid->EstimateSpecificHeatFromTYk(T_ptr[0],Yk_ptr[0]);
		 //double c0_in = fluid->getSpeedofSound(T_ptr[0],W_ptr[0],Cp0_in);

		 /*double Cp0_in = inlet->Cp;
		 double c0_in = inlet->c;


		 double rho0_in = qn[0][0];


		 P_imp_in += -u_add*rho0_in*c0_in;
		 rho_imp_in += -u_add*rho0_in/c0_in;

		 double u_1 = qn[1][0]/qn[0][0];

		 double w1_in = rho_imp_in - P_imp_in/(c0_in*c0_in);
		 double w2_in = u_imp_in + P_imp_in/(rho0_in*c0_in);
		 double w3_in = u_1 - P_ptr[0]/(rho0_in*c0_in);

		 inlet->P   = c0_in*rho0_in/2.0*(w2_in - w3_in);
		 inlet->rho = w1_in + rho0_in/(2.0*c0_in)*(w2_in - w3_in);
		 inlet->u   = 0.5*(w2_in+w3_in);*/

		 inlet->T = fluid->EstimateTfromPRhoW(inlet->P,inlet->rho,inlet->W);

		 inlet->Cp = fluid->EstimateSpecificHeatFromTYk(inlet->T,inlet->Yk);

		 inlet->c = fluid->getSpeedofSound(inlet->T,inlet->W,inlet->Cp);



		 //double *P_ptr = fluid->getP();

		 double rho_n = qn[0][num_pts-1];
		 double u_n = qn[1][num_pts-1]/qn[0][num_pts-1];
		 double cp_n = fluid->EstimateSpecificHeatFromTYk(T_ptr[num_pts-1],Yk_ptr[num_pts-1]);
		 double c_n = fluid->getSpeedofSound(T_ptr[num_pts-1],W_ptr[num_pts-1],cp_n);
		 double P_n = P_ptr[num_pts-1];



		 /*double w1_n = -rho_n*u_n/(2.0*c_n) + P_n/(2.0*c_n*c_n);
		 double w2_n = rho_n - P_n/(c_n*c_n);
		 double w3_n = rho_n*u_n/(2.0*c_n) + P_n/(2.0*c_n*c_n);*/

		 double w1_n = rho_n - P_n/(c_n*c_n);
		 double w2_n = u_n + P_n/(rho_n*c_n);
		 double w3_n = u_n - P_n/(rho_n*c_n);



		 /*w1_imp = -rho_imp*u_imp/(2.0*c_imp) + P_imp/(2.0*c_imp*c_imp);
		 w2_imp = rho_imp - P_imp/(c_imp*c_imp);
		 w3_imp = rho_imp*u_imp/(2.0*c_imp) + P_imp/(2.0*c_imp*c_imp);*/

		 w1_imp = rho_imp - P_imp/(c_n*c_n);
		 w2_imp = u_imp + P_imp/(rho_n*c_n);
		 w3_imp = u_imp - P_imp/(rho_n*c_n);


		 //double sigma = 10.0/(rho_inf*c_inf)*domain_length/u_inf;
		 //double sigma = 1.0/(rho_n*c_n)*c_imp/domain_length;   check later

		 //last working
		 //double sigma = 150.0/(rho_n*c_n)*u_imp/domain_length;
		 double sigma = kP_local/(rho_n*c_n)*u_imp/domain_length;
		 //double sigma = 0.0/(rho_n*c_n)*u_imp/domain_length;

		 w3_err_prod +=  sigma*(P_inf - P_n)*local_tstep;
		 //w3_err_prod +=  sigma*(P_inf - P_n)*time_step;

		 //w3_imp = w3_imp - sigma*(P_inf - P_n)*time_step;

		 //w3_imp = w3_imp - sigma*w3_err_prod;
		 w3_imp = w3_imp - w3_err_prod;

		 // u-c, u , u+c

		 /*double w1_f = w1_imp;
		 double w2_f = w2_n;
		 double w3_f = w3_n;*/

		 // u+c, u , u-c

		 double w1_f = w1_n;
		 double w2_f = w2_n;
		 double w3_f = w3_imp;


		 /*outlet->rho = w1_f + w2_f + w3_f;
		 outlet->u   = c_n/outlet->rho*(w3_f - w1_f);
		 outlet->P   = c_n*c_n*(w1_f+w3_f);*/


		 outlet->P   = c_n*rho_n/2.0*(w2_f - w3_f);
		 outlet->rho = w1_f + rho_n/(2.0*c_n)*(w2_f - w3_f);
		 outlet->u   = 0.5*(w2_f+w3_f);


		// outlet->P   = P_inf;
		// outlet->rho = rho_n;
		// outlet->u   = u_n;



		 //outlet->u   = outlet->c/outlet->rho*(w3_f - w1_f);
		 //outlet->P   = outlet->c*outlet->c*(w1_f+w3_f);

		 //old 2
		 /*double P_n = P_ptr[num_pts-1];

		 outlet->P = P_inf + outlet->u*outlet->rho*outlet->c;*/

		 //outlet->P = P_inf + (outlet->u - u_inf)*outlet->rho*outlet->c;
		 //outlet->P = P_n + (outlet->u - u_inf)*outlet->rho*outlet->c;


		 // old 1
		 /*double w3=outlet->u - outlet->P/(outlet->rho*outlet->c);


		 //double sigma = 1.0/(rho_inf*c_inf)*domain_length/u_inf;
		 double sigma = 1.0/(rho_inf*c_inf)*domain_length/outlet->u;

		 //std::cout<<"sigma :"<<sigma<<std::endl;
		 double P_n = P_ptr[num_pts-1];

		 w3_err_prod +=  (P_inf - P_n)*time_step;


		 outlet->w3 = w3_init + sigma*w3_err_prod;
		 //std::cout<<"w3 :"<<outlet->w3<<std::endl;
		 //outlet->w3 = u_inf - P_n/(r*c_inf)

		 outlet->P = (outlet->u - outlet->w3)*(outlet->rho*outlet->c);*/
		 //std::cout<<"P :"<<outlet->P<<std::endl;

		 //inlet->T = 300.0;
		 //outlet->T = T_ptr[num_pts-1];
		 outlet->T= fluid->EstimateTfromPRhoW(outlet->P,outlet->rho,outlet->W);
		 outlet->Cp = fluid->EstimateSpecificHeatFromTYk(outlet->T,outlet->Yk);
		 outlet->c = fluid->getSpeedofSound(outlet->T,outlet->W,outlet->Cp);

		 //double e1  = fluid->EstimateInternalEnergyFromTYk(T1,Yk_1,W1,P1,rho1);
		inlet->e = fluid->EstimateEnthalpyInternalEnergyFromTYk(inlet->T,inlet->Yk,inlet->W,inlet->P,inlet->rho,inlet->hk);

		//double e2  = fluid->EstimateInternalEnergyFromTYk(T2,Yk_2,W2,P0,rho2);
		outlet->e = fluid->EstimateEnthalpyInternalEnergyFromTYk(outlet->T,outlet->Yk,outlet->W,outlet->P,outlet->rho,outlet->hk);



		fluid->updateBoundaryProperties(grid_ops,inlet,outlet);
}

void Solver::Run() {

	DataIO io;

	//string FlameVarsFile = "results/flamevars.dat";
	//string RecordFile = "results/records.bin";
	//string AnimFile = "results/timehistory.dat";
	// Note: Use only for writeVarsX
	//io.openFileWrite(AnimFile);
	updateVariables();


	io.writeFlameVarsX(FlameVarsFile,fluid,grid_obj);
#if RECORD_TIME_VARS
	io.openRecordFileWrite(RecordFile);
#endif


#if ANIMATE_FLAME_VARS
	io.openAnimFileWrite(AnimFile);
	io.writeAnimVarsX(fluid,grid_obj);
#endif
	//io.writeVarsX(fluid,grid_obj);

	time_step = grid_spacing/3000;
	//for(elapsed_iter=0;elapsed_iter<num_iter;elapsed_iter++){
	elapsed_iter = 0;
	while(soln_time < max_sim_time){

		integrator->advance(this,qn,rhs,time_step);

		soln_time += time_step;

		//estimateTimeStepSize();

		//if(elapsed_iter==0)io.writeVarsXDebug(fluid,grid_obj);
		//if(elapsed_iter==20)io.writeVarsX(fluid,grid_obj);
		//std::cout<<"Index :"<<elapsed_iter<<" time_step: "<<time_step<<" Elapsed time: "<<soln_time<<std::endl;

		//superposeVelocity();
		//std::cout<<"Index :"<<elapsed_iter<<" time_step: "<<time_step<<" Elapsed time: "<<std::scientific<<soln_time<<" Flame pos: "<<fc_ind<<" Err: "<<(u_add - u_fc)<<std::endl;
		std::cout<<"Index :"<<elapsed_iter<<" time_step: "<<time_step<<" Elapsed time: "<<std::scientific<<soln_time<<" Flame pos: "<<fc_ind<<" u_fc: "<<u_fc<<std::endl;
		//io.writeFlameVarsX(FlameVarsFile,fluid,grid_obj);
		recordVars();
		if(record_index==record_interval){
			//updateVariables();
			std::cout<<"Record Write"<<std::endl;
			#if RECORD_TIME_VARS
				io.RecordVarsTime(num_record,record_interval,record_data);
			#endif

			//io.writeFlameVarsX(FlameVarsFile,fluid,grid_obj);

			#if ANIMATE_FLAME_VARS
			io.writeAnimVarsX(fluid,grid_obj);
			#endif

			//io.writeVarsX(fluid,grid_obj);
			//io.writeAnimVarsX(fluid,grid_obj);
			record_index=0;
		}

		++elapsed_iter;
	}

	//updateVariables();
	double flamespeed = inlet->u - u_fc;
	//io.RecordVarsTime(num_record,record_interval,record_data);
	estimateFlameThickness();
	//std::cout<<std::endl<<"Steady-state Flame Thickness : "<< std::scientific<<fl_thickness<<" m"<< std::endl;
	//std::cout<<"Steady-state Flame Velocity  : "<<inlet->u<<" m/s"<<std::endl<<std::endl;
	std::cout<<std::endl<<"\033[1;31mSteady-state Flame Thickness : "<< std::scientific<<fl_thickness<<" m \033[0m"<< std::endl;
	std::cout<<std::endl<<"\033[1;31mSteady-state Flame Velocity  : "<< std::scientific<<flamespeed<<" m/s \033[0m"<< std::endl<<std::endl;
	std::cout<<std::endl<<"\033[1;31mAdiabatic Flame Temperature : "<< std::scientific<<T_ad<<" K \033[0m"<< std::endl<<std::endl;

	//io.writeVarsX(fluid,grid_obj);

	#if ANIMATE_FLAME_VARS
		io.writeAnimVarsX(fluid,grid_obj);
		io.closeAnimFileWrite();
	#endif
	//io.writeAnimVarsX(fluid,grid_obj);
	#if RECORD_TIME_VARS
		io.closeRecordFileWrite();
	#endif

	solver_runtime = ( std::clock() - start_time)/((double) CLOCKS_PER_SEC*60.0);
	cout<<"1D Flame: Solver concluded . Total Runtime: "<<std::fixed<<solver_runtime<<" minutes"<<endl;

	io.writeResultFile(flamespeed,fl_thickness,solver_runtime,soln_time,settings_str);

	io.writeFlameVarsX(FlameVarsFile,fluid,grid_obj);

	//io.closeFileWrite();


}

void Solver::InitializeArrays() {

	qn = new double*[num_dim1];
	for(int k=0;k<num_dim1;k++)
	  qn[k] = new double[num_dim2];

	rhs = new double*[num_dim1];
	for(int k=0;k<num_dim1;k++)
	  rhs[k] = new double[num_dim2];


	record_data = new double[num_record*record_interval];
	//record_data = new double*[num_record];
	//for(int k=0;k<num_record;k++)
	//	record_data[k] = new double[record_interval];

	T_tmp = new double[num_pts];

	//qn_contiguous = new double[(num_species+3)*num_pts];
}

void Solver::Finalize() {

	std::auto_ptr<Solver> bye_bye(this);
	//TODO: call all the destructors and make sure everything is deallocated!!!!!
}

int Solver::getNumSpecies() const {
	return num_species;
}

void Solver::setNumSpecies(int numSpecies) {
	num_species = numSpecies;
}

void Solver::estimateTimeStepSize() {

	//time_step = 2.0*grid_spacing/3000;
	//time_step = 4.2e-9;
	//time_step = 6.4e-9;

	//time_step = grid_spacing/3000;
	//time_step = 2.8/(4.0*0.014)*grid_spacing*grid_spacing;
	time_step = 2.4/(4.0*0.017)*grid_spacing*grid_spacing;
	//time_step = 2.3/(4.0*0.019)*grid_spacing*grid_spacing;

	//time_step = grid_spacing/6000;
	//time_step = grid_spacing/10000;

	/*if(elapsed_iter<10000)  //30000
		time_step = grid_spacing/5000;
		//time_step = grid_spacing/3000;
	else
		time_step = grid_spacing/3000;
		//time_step = grid_spacing/2000;
*/

	/* last working  for L=0.025, npts =2048
	 * (if(elapsed_iter<10000)  //30000
		//time_step = grid_spacing/5000;
		time_step = grid_spacing/4000;
	else
		time_step = grid_spacing/2000;
*/

	/* last working
	 * if(elapsed_iter<5000)
		time_step = grid_spacing/8000;
	else
		time_step = grid_spacing/3000;
*/
		//time_step = grid_spacing/3000;

	//time_step = grid_spacing/10000;

	//time_step = grid_spacing/10000; //for L=0.001


#if DYNAMIC_TIME_STEP
			//double inviscid_sr_dx[3];
			double *u  		= fluid->getU();
			double *c  		= fluid->getC();
			double *one_over_dx = grid_obj->getFacialDxPtr();
			double *Vc      = fluid->getMixtureObj()->getTransportObj()->getVc();
			double **Vk      = fluid->getMixtureObj()->getTransportObj()->getVk();
			double *lambda      = fluid->getMixtureObj()->getLambda();
			double *cp     = fluid->getMixtureObj()->getCp();
			//double *rho 	= fluid->getRho();
			double *T 	= fluid->getT();
			double **Yk      = fluid->getMixtureObj()->getYk();
			double **hk      = fluid->getMixtureObj()->getThermoObj()->geth_k();
			double **omega_k = fluid->getMixtureObj()->getKineticsObj()->getOmegaK();


			//cout<<"abs max Vc::::"<<absmax(Vc,num_pts) <<std::endl;
			//cout<<"abs max  u::::"<<absmax(u,num_pts) <<std::endl;
			//cout<<"abs max  c::::"<<absmax(c,num_pts) <<std::endl;

			//double c_max = absmax(u,num_pts);


/*			for(int i=0;i<num_pts;i++)
				T_tmp[i]= absmax_xplusy(u,c,num_pts);

			inviscid_sr_dx[0] = max(absmax_xplusy(u,c,num_pts),absmax_xminusy(u,c,num_pts));*/

			//cout<<"inviscid sr1::::"<<inviscid_sr_dx[0] <<std::endl;

			/*for(int i=1;i<num_pts-1;i++)
				T_tmp[i]= abs(u[i]+0.5*(Vc[i+1]+Vc[i]));

			inviscid_sr_dx[1] = *max_element(T_tmp,T_tmp+num_pts) + c_max;*/

			//for(int i=1;i<num_pts-1;i++)
			for(int i=0;i<num_pts;i++)
				T_tmp[i]=u[i]+Vc[i];
				//T_tmp[i]=u[i]+0.5*(Vc[i+1]+Vc[i]);

			//inviscid_sr_dx[1] =absmax(T_tmp,num_pts) + c_max;
			inviscid_sr_dx = max(absmax_xplusy(T_tmp,c,num_pts),absmax_xminusy(T_tmp,c,num_pts));

			 /*double i_tmp = 0.0;
			 inviscid_sr_dx[1] = 0.0;
			for(int i=0;i<num_pts;i++)
				for(int k=0;k<num_species;k++){
					i_tmp = abs((u[i]+0.5*(Vc[i+1]+Vc[i]))/(Yk[i][k]));
					//s_tmp = abs(omega_k[i][k]/(rho[i]*Yk[i][k]));
					if(i_tmp>inviscid_sr_dx[1]) inviscid_sr_dx[1] = i_tmp;

				}*/
			//cout<<"inviscid sr2::::"<<inviscid_sr_dx[1] <<std::endl;



			/*for(int i=1;i<num_pts-1;i++){
				T_tmp[i]= u[i];
				for(int k=0;k<num_species;k++)
					T_tmp[i] += 0.5*(Vk[i+1][k]+Vk[i][k]);
				T_tmp[i] = abs(T_tmp[i]);
			}

			inviscid_sr_dx[2] = *max_element(T_tmp,T_tmp+num_pts) + c_max;*/


			/*double t1=0.0;
			double t2=0.0;
			for(int i=0;i<num_pts;i++){
							//T_tmp[i]= u[i];  // or zero??
				t1 = 0.0;
				t2 =0.0;
				T_tmp[i] = 0.0;
				for(int k=0;k<num_species;k++){
					t1 += hk[i][k]*Yk[i][k];
					t2 += hk[i][k]*Yk[i][k]*Vk[i][k];
					//T_tmp[i] +=t2;
				}
				T_tmp[i] = u[i] + t2/t1;
			}*/


			/*double vk_max =0.0;
			int maxii = -1;
			for(int i=0;i<num_pts;i++){
							//T_tmp[i]= u[i];  // or zero??
				vk_max =0.0;
				maxii = -1;
				T_tmp[i] = 0.0;
				for(int k=0;k<num_species;k++)
					if(abs(u[i]+ Vk[i][k])>vk_max){
						vk_max = abs(u[i]+ Vk[i][k]);
						maxii = k;
					}

				T_tmp[i] = u[i] + Vk[i][maxii];

			}*/


			//for(int i=1;i<num_pts-1;i++){
			/*for(int i=0;i<num_pts;i++){
				//T_tmp[i]= u[i];  // or zero??
				T_tmp[i] =abs(u[i])+ absmax(Vk[i],num_species);

				//for(int k=0;k<num_species;k++)
				//	T_tmp[i] += Yk[i][k]*Vk[i][k];


					//T_tmp[i] += 0.5*(Vk[i+1][k]+Vk[i][k]);
				//T_tmp[i] = abs(T_tmp[i]);
			}*/

			//T_tmp[i] =abs(u[i])+ absmax(T_tmp[i],num_species);
			//inviscid_sr_dx[2] = absmax(T_tmp,num_pts) + c_max;

			//inviscid_sr_dx[2] = max(absmax_xplusy(T_tmp,c,num_pts),absmax_xminusy(T_tmp,c,num_pts));

			//cout<<"inviscid sr3::::"<<inviscid_sr_dx[2] <<std::endl;


	        //inviscid_sr_dx(2) = max(abs(u+V_c)) + max(c);
	        //inviscid_sr_dx(3) = max(abs(sum(V_k))) + max(c);

			//double inviscidmax = max(max(inviscid_sr_dx[0],inviscid_sr_dx[1]),inviscid_sr_dx[2]);
			//cout<<"inviscid max::::"<<inviscidmax <<std::endl;
			//double inviscidmax = inviscid_sr_dx[2];
	        //inviscid_sr = max(inviscid_sr_dx)/dx;

			//inviscid_sr = inviscidmax/grid_spacing;
			inviscid_sr = inviscid_sr_dx/grid_spacing;

			//cout<<"inviscid sr::::"<<inviscid_sr <<std::endl;
			//double  inviscid_sr = inviscid_sr_dx[0]/grid_spacing;


			double u_tmp = max(absmax_xplusy(u,Vc,num_pts),20.0);

			double *rho  		= fluid->getRho();
			double *mu = fluid->getMixtureObj()->getMu();
			for(int i=0;i<num_pts;i++)
				T_tmp[i]= mu[i]/rho[i];
				//T_tmp[i]= mu[i]/rho[i]*u_tmp;
				//T_tmp[i]= mu[i]/rho[i]*20.0;


			viscous_sr_dx2[0]= *max_element(T_tmp,T_tmp+num_pts);
	        //double viscous_sr_dx2 = *max_element(mu,mu+num_pts);

			//cout<<"viscous sr1::::"<<viscous_sr_dx2[0] <<std::endl;

			double **Dk      = fluid->getMixtureObj()->getTransportObj()->getDk();

			for(int i=0;i<num_pts;i++)
			{
				T_tmp[i] = 0.0;
				//T_tmp[i] = *max_element(Dk[i],Dk[i]+num_species);
				for(int k=0;k<num_species;k++)
					T_tmp[i] += Dk[i][k];
			}
				//T_tmp[i] = *max_element(Dk[i],Dk[i]+num_species);

			viscous_sr_dx2[1]= *max_element(T_tmp,T_tmp+num_pts);

			//cout<<"viscous sr2::::"<<viscous_sr_dx2[1] <<std::endl;


			//add to Dk[i]??
			for(int i=0;i<num_pts;i++)
				T_tmp[i] =  lambda[i]/(rho[i]*cp[i]);
				//T_tmp[i] += ( (lambda[i]/(rho[i]*cp[i])) + mu[i]/rho[i]);


			viscous_sr_dx2[2]= *max_element(T_tmp,T_tmp+num_pts);
			//viscous_sr_dx2[2]= 0.0;

			//cout<<"viscous sr3::::"<<viscous_sr_dx2[2] <<std::endl;

	        //viscous_sr_dx2(2) =  max(abs(W_k(1)*rho(:)'.*D_k(1,:)./W(:)'));
	        //viscous_sr_dx2(3) =  max(abs(W_k(2)*rho(:)'.*D_k(2,:)./W(:)'));
	        //viscous_sr_dx2(4) =  max(abs(W_k(3)*rho(:)'.*D_k(3,:)./W(:)'));

			double viscous_max =max(max(viscous_sr_dx2[0],viscous_sr_dx2[1]),viscous_sr_dx2[2]);

			//cout<<"viscous max::::"<<viscous_max <<std::endl;
	        viscous_sr  = 4.0*(viscous_max)/(grid_spacing*grid_spacing);

	        //cout<<"viscous sr::::"<<viscous_sr <<std::endl;


	        source_sr = 1.e-60;
	        double s_tmp = 0.0;

			for(int i=0;i<num_pts;i++)
				for(int k=0;k<num_species;k++){
					s_tmp = abs(omega_k[i][k]/(rho[i]));
				    //s_tmp = abs(omega_k[i][k]/(rho[i]*Yk[i][k]));
				    if(s_tmp>source_sr) source_sr = s_tmp;

				}

			//cout<<"source sr::::"<<source_sr <<std::endl;


	        /*for(int k=0;k<num_species;k++){
				for(int i=0;i<num_pts;i++)
					T_tmp[i]= omega_k[i][k]/(rho[i]*Yk[i][k]);

				source_sr = max(*max_element(T_tmp,T_tmp+num_pts),source_sr);

	        }*/
	        //double source_sr = max(max(abs(omega_k)));

	        //double spectral_radius =  viscous_sr;
	        //double spectral_radius =  max(inviscid_sr,viscous_sr);
	        double spectral_radius =  max(max(inviscid_sr,viscous_sr),source_sr);
	        //cout<<"sr::::"<<spectral_radius <<std::endl;
	        double dt_max = 2.8/spectral_radius;
	        //double dt_max = 4.0/spectral_radius;
	       //cout<<"Estimated time step::::"<<dt_max<<std::endl;

	       //if(elapsed_iter>100)
	        time_step = dt_max;

#endif

}

void Solver::estimateFlameParams(double tstep) {

	   double *T_ptr = fluid->getT();

		//double T_min = *std::min_element(T_ptr,T_ptr+num_pts);
		double T_min = T_u_init;

		//double T_max = T_min+2000.0;
		//double T_max = *std::max_element(T_ptr,T_ptr+num_pts);

		//double T_fc = T_min + 0.5*(T_max-T_min);
		//double T_fc = T_min + 700.0;
		double T_fc = T_min + 100.0;

		for(int i=0;i<num_pts;i++)
			T_tmp[i]=std::abs(T_ptr[i] - T_fc);

		double T1 = 10000.0;
		int index_1 = -1;
		int index_2 = -1;
		for(int i=0;i<num_pts;i++)
			if(T_tmp[i]<=T1){
				T1 = T_tmp[i];
				index_1 = i;
			}
		T1 = T_ptr[index_1];

		if(T1 > T_fc && T_ptr[index_1+1]< T_fc ) index_2 = index_1+1;
		else if(T1 > T_fc && T_ptr[index_1-1]< T_fc ) index_2 = index_1-1;
		else if(T1 < T_fc && T_ptr[index_1+1]> T_fc ) index_2 = index_1+1;
		else if(T1 < T_fc && T_ptr[index_1-1]> T_fc ) index_2 = index_1-1;

		double T2 = T_ptr[index_2];
		double x1 = grid_obj->getXPosition(index_1);
		double x2 = grid_obj->getXPosition(index_2);


		x_fc_prev = x_fc;
		x_fc = (T_fc - T1)*(x2-x1)/(T2-T1) + x1;

		if(tstep>0.0){
			u_fc_prev = u_fc;
			u_fc = (x_fc - x_fc_prev)/tstep;
		}
		fc_ind = index_1;


}


void Solver::estimateFlameParams2(double tstep) {

	double *T_ptr = fluid->getT();

	//double T_min = *std::min_element(T_ptr,T_ptr+num_pts);
	double T_min = T_u_init;



	double *dT_dx = fluid->getdT_Dx();
	double *d2T_dx2 = fluid->getD2T_Dx2();
	//double *x = grid_obj->getX();

	double ref_max = 1.e8;
	bool max_found = false;

	int li = -1;
	int ri = -1;

	//int i_start = 175;
	int i_start = 0;

	for(int i = i_start;i<num_pts;++i){
	    if(!max_found && T_ptr[i] > (T_min + 10.0) && d2T_dx2[i+1] < d2T_dx2[i]){
	        if(d2T_dx2[i] > ref_max)
	        	max_found = true;
	    }
	    else if(max_found && d2T_dx2[i+1] < 0.0){
	        li = i;
	        ri = i+1;
	        break;
	    }
	}
	double T1 = d2T_dx2[li];
	double T2 = d2T_dx2[ri];
	double x1 = grid_obj->getXPosition(li);
	double x2 = grid_obj->getXPosition(ri);


	double T_fc = 0.0;
	x_fc_prev = x_fc;
	x_fc = (T_fc - T1)*(x2-x1)/(T2-T1) + x1;

	if(tstep>0.0){
		u_fc_prev = u_fc;
		u_fc = (x_fc - x_fc_prev)/tstep;
	}
	fc_ind = li;
//	fc_ind = index_1;


}



void Solver::estimateFlameParams3(double tstep) {

	   double *T_ptr = fluid->getRho();

		//double T_min = *std::min_element(T_ptr,T_ptr+num_pts);
		double T_min = rho_min;

		double T_max = rho_max;
		//double T_max = *std::max_element(T_ptr,T_ptr+num_pts);

		double T_fc = T_min + 0.80*(T_max-T_min);
		//double T_fc = T_min + 0.5*(T_max-T_min);
		//double T_fc = T_min + 700.0;
		//double T_fc = T_min + 100.0;

		for(int i=0;i<num_pts;i++)
			T_tmp[i]=std::abs(T_ptr[i] - T_fc);

		double T1 = 10000.0;
		int index_1 = -1;
		int index_2 = -1;
		for(int i=0;i<num_pts;i++)
			if(T_tmp[i]<=T1){
				T1 = T_tmp[i];
				index_1 = i;
			}
		T1 = T_ptr[index_1];

		if(T1 > T_fc && T_ptr[index_1+1]< T_fc ) index_2 = index_1+1;
		else if(T1 > T_fc && T_ptr[index_1-1]< T_fc ) index_2 = index_1-1;
		else if(T1 < T_fc && T_ptr[index_1+1]> T_fc ) index_2 = index_1+1;
		else if(T1 < T_fc && T_ptr[index_1-1]> T_fc ) index_2 = index_1-1;

		double T2 = T_ptr[index_2];
		double x1 = grid_obj->getXPosition(index_1);
		double x2 = grid_obj->getXPosition(index_2);


		x_fc_prev = x_fc;
		x_fc = (T_fc - T1)*(x2-x1)/(T2-T1) + x1;

		if(tstep>0.0){
			u_fc_prev = u_fc;
			u_fc = (x_fc - x_fc_prev)/tstep;
		}
		fc_ind = index_1;


}




Solver::~Solver() {

	delete [] T_tmp;

	delete cantera_obj;

	delete [] record_data;
	//for(int k=0;k<num_record;k++)
	//	delete [] record_data[k];
	//delete record_data;

	delete inlet;
	delete outlet;

	std::cout<<"Concrete solver's destructor called"<<std::endl;
}

const string& Solver::getCtmlFile() const {
	return ctml_file;
}

void Solver::setCtmlFile(const string& ctmlFile) {
	ctml_file = ctmlFile;
}

void Solver::setRefPressure(double P_inf) {
	this->P_inf = P_inf;
}


void Solver::estimateFlameThickness() {

	double *T_ptr = fluid->getT();

	double *x = grid_obj->getX();
	//double T_min = *std::min_element(T_ptr,T_ptr+num_pts);
	//double T_max = *std::max_element(T_ptr,T_ptr+num_pts);

	double T_min = T_u_init;





	double *dT_dx = fluid->getdT_Dx();
	grid_ops->estimateDAoverDx(T_ptr,dT_dx);

	double *d2T_dx2 = fluid->getD2T_Dx2();
	grid_ops->estimateD2AoverDx2(T_ptr,d2T_dx2);

	double xstart = (1-0.75)*grid_obj->getXPosition(fc_ind) + 0.75*grid_obj->getXPosition(num_pts-1);

	int x_start = grid_obj->getXIndex(xstart);
	//int x_start = fc_ind + int(double(num_pts -fc_ind)*0.75);

	//int x_start = num_pts -50;

	double m2 = (d2T_dx2[x_start+1]-d2T_dx2[x_start-1])/(x[x_start+1]-x[x_start-1]);
	double c2 = d2T_dx2[x_start];
	double c1 = dT_dx[x_start];
	double c  = T_ptr[x_start];

	double x_inf2 = -2.0/m2*(c1+c2);
	double x_inf = x_inf2;

	T_ad = m2*x_inf*x_inf*x_inf/6.0 + c2*x_inf*x_inf/2.0 + c1*x_inf + c;

	double T_max = T_ad;

	double dTdx_abs_max = absmax(dT_dx,num_pts);
	/*grid_ops->estimateDAoverDx(T_ptr,T_tmp);

     //for(int i=0;i<num_pts;i++)
	//	T_tmp[i]=std::abs(T_tmp[i]);

	//double dTdx_abs_max = *std::max_element(T_tmp,T_tmp+num_pts);
	double dTdx_abs_max = absmax(T_tmp,num_pts);*/

	fl_thickness = (T_max - T_min)/dTdx_abs_max;

}


void Solver::recordVars() {

	int tmp_ind = record_index*num_record;

	record_data[tmp_ind] = soln_time;
	record_data[tmp_ind+1] = x_fc;
	record_data[tmp_ind+2] = u_fc;
	record_data[tmp_ind+3] = inlet->u;//err;
	record_data[tmp_ind+4] = inlet->T;
	record_data[tmp_ind+5] = outlet->P;
	record_data[tmp_ind+6] = outlet->T;


	/*record_data[tmp_ind+1] = inviscid_sr_dx;
	record_data[tmp_ind+2] = viscous_sr_dx2[0];
	record_data[tmp_ind+3] = viscous_sr_dx2[1];//err;
	record_data[tmp_ind+4] = viscous_sr_dx2[2];
	record_data[tmp_ind+5] = viscous_sr;
	record_data[tmp_ind+6] = source_sr;*/


	record_index++;

}

void Solver::setRecordInterval(int recordInterval) {
	record_interval = recordInterval;
}

}
