/*
 * GridOperations.cpp
 *
 *  Created on: Jun 17, 2014
 *      Author: abishekg
 */

#include "GridOperations.h"
#include "PhysicalConstants.h"
#include <cmath>
#include <iostream>
namespace Flame {

GridOperations::GridOperations(Grid *grid) {

	grid_obj = grid;

}



GridOperations::GridOperations(Grid* grid, FaceData* inlet, FaceData* outlet) {

	grid_obj = grid;
	this->inlet = inlet;
	this->outlet = outlet;


	tmp_face = new double[grid_obj->getNumFaces()];
	tmp_node = new double[grid_obj->getNumNodes()];

}

GridOperations::~GridOperations() {

	delete []tmp_face;
	delete []tmp_node;

}

void GridOperations::estimateDelQoverQ(double** Q_ptr, double** DelQ_Q_ptr,int num_dim2) {

	int num_nodes = grid_obj->getNumNodes();
	//double dx = grid_obj->getDx();
	double *node_pos = grid_obj->getX();
	int num_faces = grid_obj->getNumFaces();
	double *face_pos = grid_obj->getXFaces();
	double *interp_f = grid_obj->getFacialInterpCoeffs();

	//for(int i=1;i<num_nodes-1;i++)

			for(int k=0;k<num_dim2;k++){

			for(int i=1;i<num_faces-1;i++)
				tmp_face[i] = ((1.0-interp_f[i])*Q_ptr[i-1][k] + interp_f[i]*Q_ptr[i][k]);

			for(int i=1;i<num_faces-1;i++)
				DelQ_Q_ptr[i][k]=(Q_ptr[i][k] - Q_ptr[i-1][k])/((node_pos[i]-node_pos[i-1])*tmp_face[i]);

			 //DelQ_Q_ptr[i][k]=(Q_ptr[i+1][k]-Q_ptr[i-1][k])/((node_pos[i+1]-node_pos[i])*Q_ptr[i][k]);

			}

	//TODO: move to boundary conditions??   use higher order extrap
	/*for(int k=0;k<num_dim2;k++){
		 DelQ_Q_ptr[0][k] = DelQ_Q_ptr[1][k];
		 DelQ_Q_ptr[num_nodes-1][k] =  DelQ_Q_ptr[num_nodes-2][k];
	}*/


/*	for(int i=0;i<num_nodes;i++)
			for(int k=0;k<num_dim2;k++)
				if(Q_ptr[i][k]<1.0e-8)
					 std::cout<<"Q_ptr :"<<Q_ptr[i][k]<<" DelQ_Q_Qptr:"<<DelQ_Q_ptr[i][k]<<std::endl;
*/
	for(int i=0;i<num_nodes;i++)
		for(int k=0;k<num_dim2;k++)
			if(Q_ptr[i][k]<safe_zero)
			//if(Q_ptr[i][k]<1.0e-8)
			//if(Q_ptr[i][k]<1.0e-20)
				 DelQ_Q_ptr[i][k]=0.0;
}

void GridOperations::estimateDelXkoverXk(double** Q_ptr, double** DelQ_Q_ptr,
		int num_dim2) {

	int num_nodes = grid_obj->getNumNodes();
	int num_faces = grid_obj->getNumFaces();
	double *node_pos = grid_obj->getX();
	double *face_pos = grid_obj->getXFaces();

	for(int k=0;k<num_dim2;k++){

		for(int i=0;i<num_nodes;i++)
			tmp_node[i] = std::log(Q_ptr[i][k]);

		//Note: on the faces!!!!
		for(int i=1;i<num_faces-1;i++)
			DelQ_Q_ptr[i][k] = (tmp_node[i]-tmp_node[i-1])/(node_pos[i]-node_pos[i-1]);

	}


}


void GridOperations::estimateDelXkoverXkBoundaries(double** Q_ptr, double** DelQ_Q_ptr,double *Q_inlet,double *Q_outlet,int num_dim2) {

	int num_nodes = grid_obj->getNumNodes();
	int num_faces = grid_obj->getNumFaces();
	double *node_pos = grid_obj->getX();
	double *face_pos = grid_obj->getXFaces();
	double *interp_f = grid_obj->getFacialInterpCoeffs();

	double one_dx1 = 1.0/(face_pos[1]-face_pos[0]);
	//double one_dx1 = 1.0/(node_pos[0]-face_pos[0]);
	double one_dx2 = 1.0/(face_pos[num_faces-1]-face_pos[num_faces-2]);
	//double one_dx2 = 1.0/(face_pos[num_faces-1]-node_pos[num_nodes-1]);

	for(int k=0;k<num_dim2;k++){

		//DelQ_Q_ptr[0][k] =  one_dx1*(std::log(Q_ptr[0][k]) - std::log(Q_inlet[k]) );
		//DelQ_Q_ptr[num_faces-1][k] = one_dx2*( std::log(Q_outlet[k]) - std::log(Q_ptr[num_nodes-1][k]) );
		//std::cout<<"At inlet Xk:"<<Q_inlet[k]<< " Next two av: "<<0.5*(Q_ptr[0][k] +Q_ptr[1][k])<<std::endl;
		//DelQ_Q_ptr[0][k] =  0.0;

		//DelQ_Q_ptr[0][k] =  one_dx1*(std::log( 0.5*(Q_ptr[0][k] +Q_ptr[1][k]) ) - std::log(Q_inlet[k]) );
		//DelQ_Q_ptr[num_faces-1][k] = one_dx2*( std::log(Q_outlet[k]) - std::log( 0.5*(Q_ptr[num_nodes-2][k] +Q_ptr[num_nodes-1][k]) ));

		DelQ_Q_ptr[0][k] =  one_dx1*(std::log( ((1.0-interp_f[1])*Q_ptr[0][k] +interp_f[1]*Q_ptr[1][k]) ) - std::log(Q_inlet[k]) );
		DelQ_Q_ptr[num_faces-1][k] = one_dx2*( std::log(Q_outlet[k]) - std::log( ((1.0-interp_f[num_faces-2])*Q_ptr[num_nodes-2][k] +interp_f[num_faces-2]*Q_ptr[num_nodes-1][k]) ));


		/*if(Q_ptr[i][k]<safe_zero)
					//if(Q_ptr[i][k]<1.0e-8)
					//if(Q_ptr[i][k]<1.0e-20)
						 DelQ_Q_ptr[i][k]=0.0;*/
	}


}


void GridOperations::estimateDAoverDx(double* A, double* DAoverDX) {

	int num_nodes = grid_obj->getNumNodes();
	double *node_pos = grid_obj->getX();
	//double dx = grid_obj->getDx();

	for(int i=1;i<num_nodes-1;i++)
		DAoverDX[i] = (A[i+1] - A[i-1])/(node_pos[i+1]-node_pos[i-1]);


	//TODO: come back!!
	DAoverDX[0] = DAoverDX[1];
	DAoverDX[num_nodes-1] = DAoverDX[num_nodes-2];
}


void GridOperations::estimateD2AoverDx2(const double* A, double* D2AoverDX2) {

	int num_nodes = grid_obj->getNumNodes();
	double *node_pos = grid_obj->getX();
	double h = 0.0;
	//double dx = grid_obj->getDx();

	for(int i=1;i<num_nodes-1;i++){
		h = (node_pos[i+1]-node_pos[i-1])/2.0;
		D2AoverDX2[i] = (A[i+1] -2.0*A[i] + A[i-1])/(h*h);
	}

	//TODO: come back!!
	D2AoverDX2[0] = D2AoverDX2[1];
	D2AoverDX2[num_nodes-1] = D2AoverDX2[num_nodes-2];

}



void GridOperations::estimateFaceVars(double* Q, double* Q_face) {

	int num_faces = grid_obj->getNumFaces();
	double *interp_f = grid_obj->getFacialInterpCoeffs();
	double r_coeff,l_coeff;
	for(int i=1;i<num_faces-1;i++){
			r_coeff = interp_f[i];
			l_coeff = (1.0 - r_coeff);
			Q_face[i] = (l_coeff*Q[i-1] + r_coeff*Q[i]);
	}
}



double* GridOperations::getInterpCoeffs() {


	return grid_obj->getFacialInterpCoeffs();
}


/*void GridOperations::estimateFaceVars(double** Q, double* tmp_face,int n,int ind) {


	double *interp_f = grid_obj->getFacialInterpCoeffs();
	double r_coeff,l_coeff;
	r_coeff = interp_f[ind];
	l_coeff = (1.0 - r_coeff);
	for(int i=0;i<n;i++){
		tmp_face[i] = (l_coeff*Q[ind-1][i] + r_coeff*Q[ind][i]);
	}
}*/


}
/* namespace Flame */

