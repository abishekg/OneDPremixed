/*
 * Kinetics.h
 *
 *  Created on: Jun 10, 2014
 *      Author: abishekg
 */

#ifndef KINETICS_H_
#define KINETICS_H_

namespace Flame {

class Kinetics {
public:
	Kinetics();
	virtual ~Kinetics();
	virtual void getProductSourceTerm_rhoYkT(double *rho,double **Yk,double *T,double *W_k);
	virtual void getProductSourceTerm_PXkT(double *P,double **Xk,double *T,double *W_k);
	double** getOmegaK() const;

protected:
	double **omega_k;

};

} /* namespace Flame */

#endif /* KINETICS_H_ */
