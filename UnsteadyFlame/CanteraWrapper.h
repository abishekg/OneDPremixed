/*
 * CanteraWrapper.h
 *
 *  Created on: Jul 7, 2014
 *      Author: abishekg
 */

#ifndef CANTERAWRAPPER_H_
#define CANTERAWRAPPER_H_

#include "cantera/kinetics.h"
#include "cantera/base/xml.h"
#include "cantera/transport/TransportParams.h"
#include "cantera/base/ct_defs.h"
#include "MMCollisionInt.h"



#include "cantera/base/ctml.h"
#include "cantera/transport/GasTransport.h"
#include "cantera/base/global.h"


//#include "cantera/base/XML_Writer.h"
//#include "cantera/base/stringUtils.h"
//#include "cantera/transport/TransportFactory.h"

#include <cstdio>

using namespace std;
using namespace Cantera;

namespace Flame {

class CanteraWrapper {
public:
	CanteraWrapper(string ctml_file,string mech_id);
	virtual ~CanteraWrapper();
	void initializeChemistry();
	void initializeStoichiometry(double phi,double o2_air_frac,double n_fo2_st,double *num_moles_1,double *num_moles_2,double *mol_wts,string fuel,string ox,string m_body);
	void initializeTransportFits(double T_low,double T_mid,double T_high);
	void copyMolecularWeights(double *W_k);
	void copyNasaCoefficients(double **a_low_k,double **a_high_k);
	void copyTransportFits(double **V_low_k,double **V_high_k,double **C_low_k,double **C_high_k,double **D_bin_low_k,double **D_bin_high_k);

	GasKinetics*& getGaskineticsObj();
	GasTransportParams& getGasTrParamsHigh();
	GasTransportParams& getGasTrParamsLow();
	int getNumSpecies() const;
	const vector<string>& getSpeciesNames() const;
	ThermoPhase*& getThermophaseObj();
	double getHighFit() const;
	double getLowFit() const;
	double getMedFit() const;

private:
	int num_species;
	vector<string> species_names;
	//vector_fp mol_weights;
	string ctml_prefix;
	string ctml_file;
	string mech_id;

	double T_low_fit,T_med_fit,T_high_fit;

	ThermoPhase* thermophase_obj;
	GasKinetics* gaskinetics_obj;
	GasTransportParams gasTrParams_low;
	GasTransportParams gasTrParams_high;


	void fitCollisionIntegrals(ostream& logfile,GasTransportParams& tr,MMCollisionInt& integrals);
	void getTransportData(const std::vector<const XML_Node*> &xspecies,XML_Node& log, const std::vector<std::string> &names, GasTransportParams& tr);
	void setupMM(std::ostream& flog, const std::vector<const XML_Node*> &transport_database,thermo_t* thermo, int mode, int log_level, GasTransportParams& tr);
	void fitProperties(GasTransportParams& tr,MMCollisionInt& integrals, std::ostream& logfile);
	void makePolarCorrections(size_t i, size_t j,const GasTransportParams& tr, doublereal& f_eps, doublereal& f_sigma);

	void getBinDiffCorrection(doublereal t,const GasTransportParams& tr, MMCollisionInt& integrals,
	        size_t k, size_t j, doublereal xk, doublereal xj,
	        doublereal& fkj, doublereal& fjk);



};

} /* namespace Flame */

#endif /* CANTERAWRAPPER_H_ */
