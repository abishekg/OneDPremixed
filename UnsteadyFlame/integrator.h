#ifndef INTEGRATOR_H
#define INTEGRATOR_H
//#include "genericsolver.h"


namespace Flame{

class Integrator
{
public:
    Integrator();
    virtual ~Integrator();

    double getDel_t() const;
    void setDel_t(double value);

    int getNum_steps() const;
    void setNum_steps(int value);

    //virtual void advance(GenericSolver* solv_obj,double **qn,double **rhs,double dt) = 0;
    virtual void advance(void* solv_obj,double **qn,double **rhs,double dt) = 0;

    int getNum_elements() const;
    void setNum_elements(int value);

protected:
    double del_t;
    int num_steps;
    int num_elements;

    //virtual void advanceInternal() = 0;

};

}
#endif // INTEGRATOR_H
