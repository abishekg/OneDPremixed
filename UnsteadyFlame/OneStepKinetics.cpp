/*
 * OneStepKinetics.cpp
 *
 *  Created on: Jun 14, 2014
 *      Author: abishekg
 */

#include "OneStepKinetics.h"
#include "PhysicalConstants.h"
#include <cmath>
#include <iostream>

namespace Flame {

OneStepKinetics::OneStepKinetics(int nx,int nk) {

	num_pts = nx;
	num_species = nk;

	omega_k  = new double*[num_pts];
	for(int i=0;i<num_pts;i++)
		omega_k[i] = new double[num_species];

}

OneStepKinetics::~OneStepKinetics() {

	for(int i=0;i<num_pts;i++)
		delete [] omega_k[i];
	delete omega_k;

	std::cout<<"Deallocating Kinetics objects.."<<std::endl;

	// TODO Auto-generated destructor stub
}

void OneStepKinetics::getProductSourceTerm_rhoYkT(double *rho,double **Yk,double *T,double *W_k) {

	//For H2-O2 one step mechanism

	double T_a = 73745/PhysicalConstants::R_cal;
	double kn = 1.8e13;
	double kg;
	//kn = kn/5.0;


	double rate_coeff[3] = {-1.0,-0.5,1.0};

	for(int i=0;i<num_pts;i++){

		kg = kn*std::exp(-T_a/T[i])*(rho[i]*1.0e-6*Yk[i][0]/W_k[0])*std::sqrt(rho[i]*1.0e-6*Yk[i][1]/W_k[1]);

    	for(int k=0;k<num_species;k++)
    		omega_k[i][k]=1.0e6*kg*rate_coeff[k]*W_k[k];

	}
}

} /* namespace Flame */
