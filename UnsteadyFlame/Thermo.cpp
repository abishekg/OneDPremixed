/*
 * Thermo.cpp
 *
 *  Created on: Jun 11, 2014
 *      Author: abishekg
 */

#include "Thermo.h"
#include "ArrayUtils.h"
#include "PhysicalConstants.h"
#include <iostream>
namespace Flame {

Thermo::Thermo(int nx,int nk) {

	num_pts = nx;
	num_species = nk;
	initializeArrays();

	initializeNasaPolynomials();
	processNasaPoly();

}


Thermo::Thermo(int nx,CanteraWrapper *cantera_obj){

	num_pts = nx;
	num_species = cantera_obj->getNumSpecies();

	initializeArrays();

	cantera_obj->copyNasaCoefficients(a_low_k,a_high_k);
	processNasaPoly();

}


void Thermo::initializeArrays() {

	T_prev =  new double[num_pts];
	for(int i=0;i<num_pts;i++)
		T_prev[i] = -1.0;

	Cp_k = new double*[num_pts];
	for(int i=0;i<num_pts;i++)
		Cp_k[i] = new double[num_species];

	h_k = new double*[num_pts];
	for(int i=0;i<num_pts;i++)
		h_k[i] = new double[num_species];


	a_low_k  = new double*[num_species];
	for(int i=0;i<num_species;i++)
		a_low_k[i] = new double[7];

	a_high_k  = new double*[num_species];
	for(int i=0;i<num_species;i++)
		a_high_k[i] = new double[7];

	a_low_k_tr  = new double*[7];
	for(int i=0;i<7;i++)
		a_low_k_tr[i] = new double[num_species];

	a_high_k_tr  = new double*[7];
	for(int i=0;i<7;i++)
		a_high_k_tr[i] = new double[num_species];
}

Thermo::~Thermo() {

	delete [] T_prev;

	for(int i=0;i<num_pts;i++)
		delete [] Cp_k[i];
	delete Cp_k;

	for(int i=0;i<num_pts;i++)
		delete [] h_k[i];
	delete h_k;

	for(int i=0;i<num_species;i++)
		delete [] a_low_k[i];
	delete a_low_k;

	for(int i=0;i<num_species;i++)
		delete [] a_high_k[i];
	delete a_high_k;

	for(int i=0;i<7;i++)
		delete [] a_low_k_tr[i];
	delete a_low_k_tr;

	for(int i=0;i<7;i++)
		delete [] a_high_k_tr[i];
	delete a_high_k_tr;

	std::cout<<"Deallocating thermo objects.."<<std::endl;
	// TODO Auto-generated destructor stub
}


void Thermo::initializeNasaPolynomials() {


    //double high_coeffs[3][6];
    //double low_coeffs[3][6];

    double high_coeffs[3][7] = {{3.33727920 ,-4.94024731E-5,   4.99456778E-07, -1.79566394E-10,  2.00255376E-14, -9.50158922E+02, -3.20502331E+00},
    	 {3.28253784,  1.48308754E-3,  -7.57966669E-07,  2.09470555E-10, -2.16717794E-14, -1.08845772E+03,  5.45323129E+00},
    	 {3.03399249 , 2.17691804E-3,  -1.64072518E-07, -9.70419870E-11,  1.68200992E-14, -3.00042971E+04 , 4.96677010E+00}};

	/*high_coeffs[0] = {3.33727920 ,-4.94024731E-5,   4.99456778E-07, -1.79566394E-10,  2.00255376E-14, -9.50158922E+02, -3.20502331E+00};
	high_coeffs[1] = {3.28253784,  1.48308754E-3,  -7.57966669E-07,  2.09470555E-10, -2.16717794E-14, -1.08845772E+03,  5.45323129E+00};
	high_coeffs[2] = {3.03399249 , 2.17691804E-3,  -1.64072518E-07, -9.70419870E-11,  1.68200992E-14, -3.00042971E+04 , 4.96677010E+00};*/

	for(int k=0;k<3;k++)
		for(int i=0;i<7;i++)
			a_high_k[k][i]=high_coeffs[k][i];

	double low_coeffs[3][7] = {{2.34433112,  7.98052075E-03, -1.94781510E-05,  2.01572094E-08, -7.37611761E-12, -9.17935173E+02,  6.83010238E-01},
						 {3.78245636,-2.99673416E-03,  9.84730201E-06, -9.68129509E-09 , 3.24372837E-12,-1.06394356E+03,  3.65767573E+00},
						{4.19864056, -2.03643410E-03,  6.52040211E-06, -5.48797062E-09,  1.77197817E-12, -3.02937267E+04, -8.49032208E-01}};

	/*low_coeffs[0]  = {2.34433112,  7.98052075E-03, -1.94781510E-05,  2.01572094E-08, -7.37611761E-12, -9.17935173E+02,  6.83010238E-01};
	low_coeffs[1]  = {3.78245636,-2.99673416E-03,  9.84730201E-06, -9.68129509E-09 , 3.24372837E-12,-1.06394356E+03,  3.65767573E+00};
	low_coeffs[2]  = {4.19864056, -2.03643410E-03,  6.52040211E-06, -5.48797062E-09,  1.77197817E-12, -3.02937267E+04, -8.49032208E-01};*/

	for(int k=0;k<3;k++)
		for(int i=0;i<7;i++)
			a_low_k[k][i]=low_coeffs[k][i];




}


void Thermo::processNasaPoly() {

	for(int k=0;k<num_species;k++){
		for(int i=0;i<5;i++)
			a_high_k_tr[i][k]=a_high_k[k][i]/double(i+1);
		a_high_k_tr[5][k]=a_high_k[k][5];
	}

	for(int k=0;k<num_species;k++){
		for(int i=0;i<5;i++)
			a_low_k_tr[i][k]=a_low_k[k][i]/double(i+1);
		a_low_k_tr[5][k]=a_low_k[k][5];
	}

}

/*void Thermo::processTemperature(double *T) {

	num_low_T = 0;
	num_high_T = 0;
	for(int i=0;i<num_pts;i++)
		if(T[i]<1000.0){
			low_T_indices[num_low_T++]=i;
		}else{
			high_T_indices[num_high_T++]=i;
		}

}*/

void Thermo::estimateSpeciesCpfromNasaPoly(int num_low_T,int num_high_T,int *low_T_indices,int *high_T_indices,double* T_ptr,double *R_sp) {


	for(int j=0;j<num_low_T;j++){
		int i = low_T_indices[j];
		double T = T_ptr[i];
		for(int k=0;k<num_species;k++){
			Cp_k[i][k]=R_sp[k]*(a_low_k[k][0] + T*(a_low_k[k][1] + T*(a_low_k[k][2] + T*(a_low_k[k][3] + T*(a_low_k[k][4]) ))));
		}
	}


	for(int j=0;j<num_high_T;j++){
		int i = high_T_indices[j];
		double T = T_ptr[i];
		for(int k=0;k<num_species;k++){
			Cp_k[i][k]=R_sp[k]*(a_high_k[k][0] + T*(a_high_k[k][1] + T*(a_high_k[k][2] + T*(a_high_k[k][3] + T*(a_high_k[k][4]) ))));
		}
	}


}

/*void Thermo::getThermoProperties(int num_low_T,int num_high_T,int *low_T_indices,int *high_T_indices,double *T_ptr,double *R_sp) {

	estimateSpeciesCpfromNasaPoly(num_low_T,num_high_T,low_T_indices,high_T_indices,T_ptr,R_sp);
	estimateSpeciesEnthalpyfromNasaPoly(num_low_T,num_high_T,low_T_indices,high_T_indices,T_ptr,R_sp);
}*/


void Thermo::getThermoProperties(int num_low_T,int num_high_T,int *low_T_indices,int *high_T_indices,double *T_ptr,double *R_sp) {

	double m_polytempvec[5];

	double **coeffs;
	int i ;
	double T ;
	coeffs = a_low_k;

	for(int j=0;j<num_low_T;j++){
		i = low_T_indices[j];
		T = T_ptr[i];

		//if(T!=T_prev[i]){  // Only update quantities when Temp changes
		if(abs(T-T_prev[i])>temp_tol){  // Only update quantities when Temp changes

		T_prev[i] = T;

		// compute powers of T
		m_polytempvec[0] = 1.0;
		m_polytempvec[1] = T;
		m_polytempvec[2] = T*T;
		m_polytempvec[3] = T*T*T;
		m_polytempvec[4] = T*T*T*T;

		for(int k=0;k<num_species;k++){
			Cp_k[i][k] = R_sp[k]*dot5(&m_polytempvec[0], coeffs[k]);
		}

		//for(size_t i=0;i<5;i++)
		//	m_polytempvec[i] = 1.0;
		m_polytempvec[0] = m_polytempvec[0]*T;
		m_polytempvec[1] = m_polytempvec[1]*T*0.50;
		m_polytempvec[2] = m_polytempvec[2]*T/3.0;
		m_polytempvec[3] = m_polytempvec[3]*T*0.25;
		m_polytempvec[4] = m_polytempvec[4]*T*0.20;

		for(int k=0;k<num_species;k++){
			h_k[i][k] = R_sp[k]*(dot5(&m_polytempvec[0], coeffs[k]) + coeffs[k][5]);
		}


		/*for(int k=0;k<num_species;k++){
			Cp_k[i][k]=R_sp[k]*(a_low_k[k][0] + T*(a_low_k[k][1] + T*(a_low_k[k][2] + T*(a_low_k[k][3] + T*(a_low_k[k][4]) ))));
			h_k[i][k]=R_sp[k]*(T*(a_low_k[k][0] + T*(a_low_k[k][1]/2.0 + T*(a_low_k[k][2]/3.0 + T*(a_low_k[k][3]/4.0 + T*(a_low_k[k][4]/5.0) )))) + a_low_k[k][5]);
		}*/
		}
	}

	coeffs = a_high_k;

	for(int j=0;j<num_high_T;j++){
		i = high_T_indices[j];
		T = T_ptr[i];

		//if(T!=T_prev[i]){  // Only update quantities when Temp changes
		if(abs(T-T_prev[i])>temp_tol){  // Only update quantities when Temp changes

		T_prev[i] = T;

		// compute powers of T
		m_polytempvec[0] = 1.0;
		m_polytempvec[1] = T;
		m_polytempvec[2] = T*T;
		m_polytempvec[3] = T*T*T;
		m_polytempvec[4] = T*T*T*T;

		for(int k=0;k<num_species;k++){
			Cp_k[i][k] = R_sp[k]*dot5(&m_polytempvec[0], coeffs[k]);
		}

		//for(size_t i=0;i<5;i++)
		//	m_polytempvec[i] = 1.0;
		m_polytempvec[0] = m_polytempvec[0]*T;
		m_polytempvec[1] = m_polytempvec[1]*T*0.50;
		m_polytempvec[2] = m_polytempvec[2]*T/3.0;
		m_polytempvec[3] = m_polytempvec[3]*T*0.25;
		m_polytempvec[4] = m_polytempvec[4]*T*0.20;

		for(int k=0;k<num_species;k++){
			h_k[i][k] = R_sp[k]*(dot5(&m_polytempvec[0], coeffs[k]) + coeffs[k][5]);
		}



		/*for(int k=0;k<num_species;k++){
			Cp_k[i][k]=R_sp[k]*(a_high_k[k][0] + T*(a_high_k[k][1] + T*(a_high_k[k][2] + T*(a_high_k[k][3] + T*(a_high_k[k][4]) ))));
			h_k[i][k]=R_sp[k]*(T*(a_high_k[k][0] + T*(a_high_k[k][1]/2.0 + T*(a_high_k[k][2]/3.0 + T*(a_high_k[k][3]/4.0 + T*(a_high_k[k][4]/5.0) )))) + a_high_k[k][5]);
		}*/

		}
	}


}



/*!
      \todo species enthalpy
      check the formula for correctness
*/
// TODO: check the formula for correctness
void Thermo::estimateSpeciesEnthalpyfromNasaPoly(int num_low_T,int num_high_T,int *low_T_indices,int *high_T_indices,double* T_ptr,double *R_sp) {

	for(int j=0;j<num_low_T;j++){
		int i = low_T_indices[j];
		double T = T_ptr[i];
		for(int k=0;k<num_species;k++){
			h_k[i][k]=R_sp[k]*(T*(a_low_k[k][0] + T*(a_low_k[k][1]/2.0 + T*(a_low_k[k][2]/3.0 + T*(a_low_k[k][3]/4.0 + T*(a_low_k[k][4]/5.0) )))) + a_low_k[k][5]);
		}
	}


	for(int j=0;j<num_high_T;j++){
		int i = high_T_indices[j];
		double T = T_ptr[i];
		for(int k=0;k<num_species;k++){
			h_k[i][k]=R_sp[k]*(T*(a_high_k[k][0] + T*(a_high_k[k][1]/2.0 + T*(a_high_k[k][2]/3.0 + T*(a_high_k[k][3]/4.0 + T*(a_high_k[k][4]/5.0) )))) + a_high_k[k][5]);
		}
	}

}

double** Thermo::getNasaPolyCoeffHighT() const {
return a_high_k;
}

double** Thermo::getNasaPolyCoeffLowT() const {
return a_low_k;
}

double** Thermo::getNasaPolyCoeffHighT_Tr() const {
return a_high_k_tr;
}

double** Thermo::getNasaPolyCoeffLowT_Tr() const {
return a_low_k_tr;
}

double** Thermo::geth_k() const {
	return h_k;
}

void Thermo::seth_k(double** k) {
	h_k = k;
}

double** Thermo::getCp_k() const {
	return Cp_k;
}


} /* namespace Flame */

