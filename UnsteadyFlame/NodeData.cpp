/*
 * NodeData.cpp
 *
 *  Created on: Jul 7, 2014
 *      Author: abishekg
 */

#include "NodeData.h"

namespace Flame {

NodeData::NodeData(int nk,int xi) {

	Xk = new double[nk];
	Yk = new double[nk];
	x_index = xi;

}

NodeData::~NodeData() {

	delete []Xk;
	delete []Yk;
}

} /* namespace Flame */
