/*
 * Transport.h
 *
 *  Created on: Jun 11, 2014
 *      Author: abishekg
 */

#ifndef TRANSPORT_H_
#define TRANSPORT_H_
#include "FaceData.h"
namespace Flame {

class Transport {
public:
	Transport();
	Transport(int nx,int nk);
	virtual ~Transport();

	virtual void getTransportProperties(int num_low_T,int num_high_T,int *low_T_indices,int *high_T_indices,double* T_ptr);
	virtual void getDiffusionProperties(double *lambda,double *rho,double *Cp,double *Le_k,double *Wk,double *W,double **del_Xk_Xk,double **Xk);
	virtual void estimateBoundaryProperties(double *Wk,double **del_Xk_Xk,FaceData *inlet,FaceData *outlet,double *Le_k);

	//virtual void processTemperature();
	virtual void initializePolynomials();
	virtual void estimateSpeciesViscosityPoly(int num_low_T,int num_high_T,int *low_T_indices,int *high_T_indices,double* T_ptr);
	virtual void estimateSpeciesConductivityPoly(int num_low_T,int num_high_T,int *low_T_indices,int *high_T_indices,double* T_ptr);
	virtual void estimateSpeciesMolecularDiffusivity(double *lambda,double *rho,double *Cp,double *Le_k);
	virtual void estimateSpeciesDiffusivityVelocity(double *Wk,double *W,double **del_Xk_Xk,double **Xk);
	double** getDk() const;
	double** getLambdak() const;
	double** getMuK() const;
	double** getMuSqrootK() const;
	double* getVc() const;
	double** getVk() const;

protected:
	int num_pts;
	int num_species;

	double **lambda_k;
	double **mu_k;
	double **mu_sqroot_k;
	double **D_k;
	double **D_bin_k;
	double **V_k;
	double *V_c;

	double **V_low_k;
	double **V_high_k;

	double **C_low_k;
	double **C_high_k;

	double **D_bin_low_k;
	double **D_bin_high_k;

};

} /* namespace Flame */

#endif /* TRANSPORT_H_ */
