/*
 * FaceData.cpp
 *
 *  Created on: Jul 17, 2014
 *      Author: abishekg
 */

#include "FaceData.h"

namespace Flame {

FaceData::FaceData(int nk,int xi) {

	Xk = new double[nk];
	Yk = new double[nk];
	hk = new double[nk];
	x_index = xi;
}

FaceData::~FaceData() {
	delete []Xk;
	delete []Yk;
	delete []hk;
}

} /* namespace Flame */
