#include<iostream>
#include "solver.h"
#include <cstdlib>
#include <cstdio>
#include "Config.h"
#undef DEBUG_MODE

using namespace std;
using namespace Flame;


int main(int argc,char **argv){


	cout<<"1D Flame:: Main program initialized"<<endl;

	Solver *solver;

	if(argc == 1)
		solver = new Solver();
	else if(argc == 2){
		string op1(argv[1]);
		if(op1.compare("-g")==0){
			Config cfg("Flame.ini");
			cfg.writeSettingsFile("Flame.ini");
			return 0;
		}else
			solver = new Solver(string(argv[1]),false);
	}
	if(argc == 3){
		string op1(argv[1]);
		if(op1.compare("-g")==0){
			string op2(argv[2]);
			Config cfg(op2);
			cfg.writeSettingsFile(op2);
			return 0;
		}
	}


	//double L = 0.01;
	//double L = 0.025;
    //int nx = 2048;
	//int nx = 4096;
    //int nx = 1024;
	//int nx = 512;

	//solver->setRecordInterval(500);
	solver->setRecordInterval(4000);

	solver->Initialize();
	cout<<"1D Flame: Solver initialized"<<endl;

	solver->Run();

	//solver->Finalize();

	delete solver;

	return 0;

}
