/*
 * DataIO.cpp
 *
 *  Created on: Jun 18, 2014
 *      Author: abishekg
 */

#include "DataIO.h"
#include "ClassDefs.h"
//#include"IdealGas.h"

namespace Flame {

DataIO::DataIO() {
	// TODO Auto-generated constructor stub

}

void DataIO::openFileWrite(string fileName) {

	//ofile_ptr.open(fileName.c_str());
	ofile_ptr.open(fileName.c_str(),ios::out | ios::binary);
}

//void DataIO::writeVarsX(FluidsClass* fluid_obj, Grid* grid_obj) {
void DataIO::writeVarsX(IdealGas* gas, Grid* grid_obj) {

	//IdealGas* gas = (IdealGas *)fluid_obj;

	double *P		= gas->getP();
	double *u  		= gas->getU();
	double *rho 	= gas->getRho();
	double *T 		= gas->getT();
	double *e 		= gas->getE();

	double *W      = gas->getMixtureObj()->getW();

	double **Xk      = gas->getMixtureObj()->getXk();
	//double **Vk      = gas->getMixtureObj()->getTransportObj()->getVk();
	//double **Dk      = gas->getMixtureObj()->getTransportObj()->getDk();
	double **del_Xk_Xk = gas->getMixtureObj()->getDelXk_Xk();
	double **omega_k = gas->getMixtureObj()->getKineticsObj()->getOmegaK();

	int num_pts = grid_obj->getNumNodes();
	int num_species = gas->getNumSpecies();

	double *x = grid_obj->getX();

	double *tmp_arr = new double[num_pts];
	int num_recs = num_pts*sizeof(double);
	ofile_ptr.write((char *)x,num_recs);
	ofile_ptr.write((char *)rho,num_recs);
	ofile_ptr.write((char *)u,num_recs);
	ofile_ptr.write((char *)T,num_recs);
	ofile_ptr.write((char *)e,num_recs);
	ofile_ptr.write((char *)P,num_recs);
	for(int k=0;k<num_species;k++){
		for(int i=0;i<num_pts;i++)
			tmp_arr[i]=Xk[i][k];
		ofile_ptr.write((char *)tmp_arr,num_recs);
	}
	for(int k=0;k<num_species;k++){
		for(int i=0;i<num_pts;i++)
			tmp_arr[i]=omega_k[i][k];
		ofile_ptr.write((char *)tmp_arr,num_recs);
	}
	for(int k=0;k<num_species;k++){
		for(int i=0;i<num_pts;i++)
			tmp_arr[i]=del_Xk_Xk[i][k];
			//tmp_arr[i]=Dk[i][k];
			//tmp_arr[i]=Vk[i][k];
		ofile_ptr.write((char *)tmp_arr,num_recs);
	}

	delete []tmp_arr;



	//ofile_ptr<<"X,rho,u,T,e,P,Yk_H2,Yk_O2,Yk_H2O,omega_H2O"<<std::endl;
	//for(int i=0;i<num_pts;i++)
	//	ofile_ptr<<x[i]<<","<<rho[i]<<","<<u[i]<<","<<T[i]<<","<<e[i]<<","<<P[i]<<","<<Xk[i][0]<<","<<Xk[i][1]<<","<<Xk[i][2]<<","<<omega_k[i][2]<<std::endl;



}

/*
void DataIO::writeVarsX(Fluid* fluid_obj, Grid* grid_obj) {

	IdealGas* gas = (IdealGas *)fluid_obj;

	double *P		= gas->getP();
	double *u  		= gas->getU();
	double *rho 	= gas->getRho();
	double *T 		= gas->getT();
	double *e 		= gas->getE();

	double **Xk      = gas->getMixtureObj()->getXk();
	double **omega_k = gas->getMixtureObj()->getKineticsObj()->getOmegaK();

	int num_pts = grid_obj->getNumNodes();
	double *x = grid_obj->getX();

	ofile_ptr<<"X,rho,u,T,e,P,Yk_H2,Yk_O2,Yk_H2O,omega_H2O"<<std::endl;
	for(int i=0;i<num_pts;i++)
		ofile_ptr<<x[i]<<","<<rho[i]<<","<<u[i]<<","<<T[i]<<","<<e[i]<<","<<P[i]<<","<<Xk[i][0]<<","<<Xk[i][1]<<","<<Xk[i][2]<<","<<omega_k[i][2]<<std::endl;



}*/

void DataIO::writeVarsXDebug(IdealGas* gas, Grid* grid_obj) {

	//IdealGas* gas = (IdealGas *)fluid_obj;

	double *P		= gas->getP();
	double *u  		= gas->getU();
	double *rho 	= gas->getRho();
	double *T 		= gas->getT();
	double *e 		= gas->getE();

	double **Yk      = gas->getMixtureObj()->getYk();
	double **omega_k = gas->getMixtureObj()->getKineticsObj()->getOmegaK();

	double *lambda   = gas->getMixtureObj()->getLambda();
	double *mu	     = gas->getMixtureObj()->getMu();
	double *Cp		 = gas->getMixtureObj()->getCp();
	double **Dk      = gas->getMixtureObj()->getTransportObj()->getDk();
	double *V_c      = gas->getMixtureObj()->getTransportObj()->getVc();
	double **Vk      = gas->getMixtureObj()->getTransportObj()->getVk();

	int num_pts = grid_obj->getNumNodes();
	double *x = grid_obj->getX();


	ofile_ptr<<"i,X,rho,u,T,e,P,Yk_H2,Yk_O2,Yk_H2O,omega_H2O,lambda,mu,Cp,Dk_H2,Dk_O2,Dk_H2O,V_c,Vk_H2,Vk_O2,Vk_H2O"<<std::endl;
	for(int i=0;i<num_pts;i++){
		ofile_ptr<<i<<","<<x[i]<<","<<rho[i]<<","<<u[i]<<","<<T[i]<<","<<e[i]<<","<<P[i]<<","<<Yk[i][0]<<","<<Yk[i][1]<<","<<Yk[i][2]<<","<<omega_k[i][2];
		ofile_ptr<<","<<lambda[i]<<","<<mu[i]<<","<<Cp[i]<<","<<Dk[i][0]<<","<<Dk[i][1]<<","<<Dk[i][2]<<","<<V_c[i]<<","<<Vk[i][0]<<","<<Vk[i][1]<<","<<Vk[i][2]<<std::endl;
	}


}



void DataIO::writeFlameVarsX(string fileName,IdealGas *gas,Grid *grid_obj){

	ofile_ptr.open(fileName.c_str(),ios::out | ios::binary);

	//IdealGas* gas = (IdealGas *)fluid_obj;

		double *P		= gas->getP();
		double *u  		= gas->getU();
		double *rho 	= gas->getRho();
		double *T 		= gas->getT();
		double *e 		= gas->getE();

		double *W      = gas->getMixtureObj()->getW();
		double *Wk     = gas->getMixtureObj()->getW_k();

		double *Cp      = gas->getMixtureObj()->getCp();
		double *lambda      = gas->getMixtureObj()->getLambda();
		double *Mu      = gas->getMixtureObj()->getMu();


		double **Xk      = gas->getMixtureObj()->getXk();
		double **Vk      = gas->getMixtureObj()->getTransportObj()->getVk();
		double **Dk      = gas->getMixtureObj()->getTransportObj()->getDk();
		double **del_Xk_Xk = gas->getMixtureObj()->getDelXk_Xk();
		double **omega_k = gas->getMixtureObj()->getKineticsObj()->getOmegaK();

		int num_pts = grid_obj->getNumNodes();
		int num_species = gas->getNumSpecies();

		double *x = grid_obj->getX();

		double *tmp_arr = new double[num_pts];
		int num_recs = num_pts*sizeof(double);
		ofile_ptr.write((char *)x,num_recs);
		ofile_ptr.write((char *)rho,num_recs);
		ofile_ptr.write((char *)u,num_recs);
		ofile_ptr.write((char *)T,num_recs);
		ofile_ptr.write((char *)e,num_recs);
		ofile_ptr.write((char *)P,num_recs);

		ofile_ptr.write((char *)Cp,num_recs);
		ofile_ptr.write((char *)lambda,num_recs);
		ofile_ptr.write((char *)Mu,num_recs);
		ofile_ptr.write((char *)W,num_recs);

		for(int k=0;k<num_species;k++){
			for(int i=0;i<num_pts;i++)
				tmp_arr[i]=Xk[i][k];
			ofile_ptr.write((char *)tmp_arr,num_recs);
		}
		for(int k=0;k<num_species;k++){
			for(int i=0;i<num_pts;i++)
				//tmp_arr[i]=Vk[i][k];
				tmp_arr[i]=0.001*(omega_k[i][k]/Wk[k]);
			ofile_ptr.write((char *)tmp_arr,num_recs);
		}
		for(int k=0;k<num_species;k++){
			for(int i=0;i<num_pts;i++)
				//tmp_arr[i]=del_Xk_Xk[i][k];
				tmp_arr[i]=Dk[i][k];
				//tmp_arr[i]=Vk[i][k];
			ofile_ptr.write((char *)tmp_arr,num_recs);
		}

		delete []tmp_arr;


	ofile_ptr.close();
}


/*

void DataIO::writeRestartFile(string fileName,IdealGas *gas,Grid *grid_obj){

	ofile_ptr.open(fileName.c_str(),ios::out | ios::binary);

	//IdealGas* gas = (IdealGas *)fluid_obj;

		double *P		= gas->getP();
		double *u  		= gas->getU();
		double *rho 	= gas->getRho();
		double *T 		= gas->getT();
		double *e 		= gas->getE();

		double *W      = gas->getMixtureObj()->getW();

		double **Xk      = gas->getMixtureObj()->getXk();
		double **Vk      = gas->getMixtureObj()->getTransportObj()->getVk();
		//double **Dk      = gas->getMixtureObj()->getTransportObj()->getDk();
		double **del_Xk_Xk = gas->getMixtureObj()->getDelXk_Xk();
		double **omega_k = gas->getMixtureObj()->getKineticsObj()->getOmegaK();

		int num_pts = grid_obj->getNumNodes();
		int num_species = gas->getNumSpecies();

		double *x = grid_obj->getX();

		double *tmp_arr = new double[num_pts];
		int num_recs = num_pts*sizeof(double);
		ofile_ptr.write((char *)x,num_recs);
		ofile_ptr.write((char *)rho,num_recs);
		ofile_ptr.write((char *)u,num_recs);
		ofile_ptr.write((char *)T,num_recs);
		ofile_ptr.write((char *)e,num_recs);
		ofile_ptr.write((char *)P,num_recs);
		for(int k=0;k<num_species;k++){
			for(int i=0;i<num_pts;i++)
				tmp_arr[i]=Xk[i][k];
			ofile_ptr.write((char *)tmp_arr,num_recs);
		}
		for(int k=0;k<num_species;k++){
			for(int i=0;i<num_pts;i++)
				//tmp_arr[i]=Vk[i][k];
				tmp_arr[i]=omega_k[i][k];
			ofile_ptr.write((char *)tmp_arr,num_recs);
		}
		for(int k=0;k<num_species;k++){
			for(int i=0;i<num_pts;i++)
				tmp_arr[i]=del_Xk_Xk[i][k];
				//tmp_arr[i]=Dk[i][k];
				//tmp_arr[i]=Vk[i][k];
			ofile_ptr.write((char *)tmp_arr,num_recs);
		}

		delete []tmp_arr;


	ofile_ptr.close();
}

*/

void DataIO::closeFileWrite() {

	ofile_ptr.close();

}

void DataIO::openRecordFileWrite(string fileName) {

	//recfile_ptr.open(fileName.c_str(), ios::out | ios::binary|std::ios_base::app);
	recfile_ptr.open(fileName.c_str(), ios::out | ios::binary);
}

void DataIO::RecordVarsTime(int num_record,int record_int, double* data) {

	//recfile_ptr<<"t,rho,u,T,e,P,Yk_H2,Yk_O2,Yk_H2O,omega_H2O"<<std::endl;

	recfile_ptr.write((char *) data,num_record*record_int*sizeof(double));
	//for(int k=0;k<num_record;k++)
		//recfile_ptr.write((char *) data[k],record_int);

}

void DataIO::closeRecordFileWrite() {

	recfile_ptr.close();
}

DataIO::~DataIO() {
	// TODO Auto-generated destructor stub
}

} /* namespace Flame */

void Flame::DataIO::openAnimFileWrite(string fileName) {
	animfile_ptr.open(fileName.c_str(),ios::out | ios::binary);
}

void Flame::DataIO::writeAnimVarsX(IdealGas* gas, Grid* grid_obj) {

	//IdealGas* gas = (IdealGas *)fluid_obj;

	double *P		= gas->getP();
	double *u  		= gas->getU();
	double *rho 	= gas->getRho();
	double *T 		= gas->getT();
	double *e 		= gas->getE();

	double *dTdx     = gas->getdT_Dx();
	double *d2Tdx2     = gas->getD2T_Dx2();

	double *W      = gas->getMixtureObj()->getW();

	double **Xk      = gas->getMixtureObj()->getXk();
	//double **Vk      = gas->getMixtureObj()->getTransportObj()->getVk();
	//double **Dk      = gas->getMixtureObj()->getTransportObj()->getDk();
	double **del_Xk_Xk = gas->getMixtureObj()->getDelXk_Xk();
	double **omega_k = gas->getMixtureObj()->getKineticsObj()->getOmegaK();

	int num_pts = grid_obj->getNumNodes();
	int num_species = gas->getNumSpecies();

	double *x = grid_obj->getX();

	double *tmp_arr = new double[num_pts];
	int num_recs = num_pts*sizeof(double);
	animfile_ptr.write((char *)x,num_recs);
	animfile_ptr.write((char *)rho,num_recs);
	animfile_ptr.write((char *)u,num_recs);
	//animfile_ptr.write((char *)dTdx,num_recs);
	//animfile_ptr.write((char *)d2Tdx2,num_recs);
	animfile_ptr.write((char *)T,num_recs);
	//animfile_ptr.write((char *)e,num_recs);
	for(int i=0;i<num_pts;i++)
		tmp_arr[i]=omega_k[i][5];
	animfile_ptr.write((char *)tmp_arr,num_recs);
	animfile_ptr.write((char *)P,num_recs);

	for(int k=0;k<num_species;k++){
		for(int i=0;i<num_pts;i++)
			tmp_arr[i]=Xk[i][k];
		animfile_ptr.write((char *)tmp_arr,num_recs);
	}/*
	for(size_t k=0;k<num_species;k++){
		for(size_t i=0;i<num_pts;i++)
			tmp_arr[i]=omega_k[i][k];
		animfile_ptr.write((char *)tmp_arr,num_recs);
	}
	for(size_t k=0;k<num_species;k++){
		for(size_t i=0;i<num_pts;i++)
			tmp_arr[i]=del_Xk_Xk[i][k];
			//tmp_arr[i]=Dk[i][k];
			//tmp_arr[i]=Vk[i][k];
		animfile_ptr.write((char *)tmp_arr,num_recs);
	}*/

	delete []tmp_arr;





}

void Flame::DataIO::closeAnimFileWrite() {

	animfile_ptr.close();
}

//TODO: could use map later
void Flame::DataIO::writeResultFile(double sl, double fl_thick,double runtime,double soln_time, string str3) {

	ofstream resfile_ptr;
	resfile_ptr.open("run_output.txt",ios::out);

	resfile_ptr<<" Program Runtime :"<<runtime<< " minutes "<<endl;
	resfile_ptr<<" Solution Time :"<<soln_time<<" seconds "<<endl;
	resfile_ptr<<" Flame velocity :"<<sl*100.0<<" cm/s"<<endl;
	resfile_ptr<<" Flame thickness :"<<fl_thick/1000.0<<" mm"<<endl;
	resfile_ptr<<" ############## Input Parameters ############# :"<<endl<<endl<<str3;

	resfile_ptr.close();

}
