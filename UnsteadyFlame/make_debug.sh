cmake -D CMAKE_BUILD_TYPE:STRING=Debug .
make
if [ "$?" = "0" ]; then                                                                                                                                                     
   gdb ./UnsteadyFlame
else
      echo "Errors during build. Halting script." 1>&2
      exit 1
fi