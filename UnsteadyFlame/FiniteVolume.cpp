/*
 * FiniteVolume.cpp
 *
 *  Created on: Jul 17, 2014
 *      Author: abishekg
 */

#include "FiniteVolume.h"
#include "IdealGas.h"
#include "CanteraKinetics.h"
#include "CanteraTransport.h"
#include "ArrayUtils.h"
#include <iostream>

namespace Flame {


FiniteVolume::FiniteVolume(IdealGas* fluid,Grid* grid, GridOperations* gridops,FaceData *inlet,FaceData *outlet) {

	grid_obj = grid;
	grid_ops = gridops;
	//fluid_obj = (IdealGas*)fluid;
	fluid_obj = fluid;
	inletBC = inlet;
	outletBC = outlet;

	//node_pos = grid_obj->getX();
	//face_pos = grid_obj->getXFaces();

	num_nodes = grid_obj->getNumNodes();
		//num_faces = num_nodes - 1;
	num_faces = num_nodes + 1;
	num_species = fluid_obj->getMixtureObj()->getNumSpecies();
	num_srcs = 1;
	//this->dx  = dx;
	//this->dx_PE = dx;

	u_face = new double[num_faces];
	P_face = new double[num_faces];
	rho_face = new double[num_faces];
	et_face = new double[num_faces];
	mu_face = new double[num_faces];
	lambda_face = new double[num_faces];
	stress_face = new double[num_faces];

	tmp_face = new double*[2+num_species];
	for(int k=0;k<2+num_species;k++)
		tmp_face[k] = new double[num_faces];

	diff_flux = new double*[3+num_species];
	for(int k=0;k<3+num_species;k++)
		diff_flux[k] = new double[num_faces];

	conv_flux = new double*[3+num_species];
	for(int k=0;k<3+num_species;k++)
		conv_flux[k] = new double[num_faces];


	src_term = new double*[num_srcs];
	for(int k=0;k<num_srcs;k++)
		src_term[k] = new double[num_nodes];

	//dx_face = new double[num_faces];

	//one_over_dx = new double[num_nodes];

	one_over_node_dx = grid->getNodalDxPtr();
	one_over_face_dx = grid->getFacialDxPtr();

	interp_f = grid->getFacialInterpCoeffs();


}

void FiniteVolume::estimateDiffusiveFluxes(double* rho, double* u,
		double* T, double* W_face, double* W_k, double** Xk, double** del_Xk_Xk_face,
		double* mu, double* lambda, double** D_k,double** V_k, double** h_k, double** Yk) {

	double tmp_dudx_node=0.0;
	double const1 = 4.0/3.0;
	// Calculate du/dx, dXk/dx and dT/dx at each face

	for(int i=1;i<num_faces-1;i++)
		tmp_face[0][i] = (u[i]-u[i-1])*one_over_node_dx[i];

    tmp_face[0][0] = (u_face[1] - inletBC->u)*one_over_face_dx[0];
    tmp_face[0][num_faces-1] = (outletBC->u - u_face[num_faces-2] )*one_over_face_dx[num_nodes-1];

	for(int i=1;i<num_faces-1;i++)
		for(int k=0;k<num_species;k++)
			tmp_face[1+k][i] = (Xk[i][k] - Xk[i-1][k])*one_over_node_dx[i];
			//tmp_face[1+k][i] = (Xk[i][k] - Xk[i-1][k])/dx_face[i];

	for(int k=0;k<num_species;k++)
        tmp_face[1+k][0] = ( ((1.0-interp_f[1])*Xk[0][k]+interp_f[1]*Xk[1][k])  -inletBC->Xk[k])*one_over_face_dx[0];
	for(int k=0;k<num_species;k++)
        tmp_face[1+k][num_faces-1] = (outletBC->Xk[k] - ((1.0-interp_f[num_faces-2])*Xk[num_nodes-2][k]+interp_f[num_faces-2]*Xk[num_nodes-1][k]) )*one_over_face_dx[num_nodes-1];


	for(int i=1;i<num_faces-1;i++)
		tmp_face[1+num_species][i] = (T[i]-T[i-1])*one_over_node_dx[i];

    tmp_face[1+num_species][0] =  (((1.0-interp_f[1])*T[0]+interp_f[1]*T[1])-inletBC->T)*one_over_face_dx[0];
    tmp_face[1+num_species][num_faces-1] = (outletBC->T - ((1.0-interp_f[num_faces-2])*T[num_nodes-2]+interp_f[num_faces-2]*T[num_nodes-1]) )*one_over_face_dx[num_nodes-1];


	//Estimate the shear stress tensor (xx) at faces . 4/3*mu*du/dx
	for(int i=1;i<num_faces-1;i++)
        stress_face[i] = const1*mu_face[i]*tmp_face[0][i];
        //stress_face[i] = 0.5*( const1*(mu[i] +mu[i-1])*tmp_face[0][i]);
		//stress_face[i] = 0.5*( const1*(mu[i] +mu[i-1])*tmp_face[0][i] - (P[i] +P[i-1]) );

	stress_face[0] = 0.0;
	stress_face[num_faces-1] = 0.0;
	//stress_face[0] = -inletBC->P;
	//stress_face[num_faces-1] = -outletBC->P;

	// Now estimate the diffusive fluxes

	for(int i=0;i<num_faces;i++)
		diff_flux[0][i] = stress_face[i];
		//diff_flux[0][i] = 0.5*( const1*(mu[i] +mu[i-1])*tmp_face[0][i] - (P[i] +P[i-1]) );

	//diff_flux[0][0] = 4.0/3.0*mu[0]*tmp_face[0][0]-inletBC->P;
	//diff_flux[0][num_faces-1] = 4.0/3.0*mu[num_nodes-1]*tmp_face[0][num_faces-1]-outletBC->P;
	// Set viscous fluxes to zero at boundaries
	//diff_flux[0][0] = -inletBC->P;
	//diff_flux[0][num_faces-1] = -outletBC->P;

	for(int k=0;k<num_species;k++)
		for(int i=1;i<num_faces-1;i++)
            diff_flux[1+k][i] = W_k[k]*(rho_face[i]*((1.0-interp_f[i])*D_k[i-1][k]+interp_f[i]*D_k[i][k])*tmp_face[1+k][i])/(W_face[i]);
			//diff_flux[1+k][i] = 0.5*W_k[k]*( (rho[i]+rho[i-1])*(D_k[i][k]+D_k[i-1][k])*tmp_face[1+k][i])/(W[i]+W[i-1]);

	// Note : extrapolating Dk from interior points
	for(int k=0;k<num_species;k++)
		diff_flux[1+k][0] = 0.0;
		//diff_flux[1+k][0] = W_k[k]*( inletBC->rho*D_k[0][k]*tmp_face[1+k][0])/inletBC->W;
	for(int k=0;k<num_species;k++)
		diff_flux[1+k][num_faces-1] = 0.0;
		//diff_flux[1+k][num_faces-1] = W_k[k]*( outletBC->rho*D_k[num_nodes-1][k]*tmp_face[1+k][num_faces-1])/outletBC->W;


	for(int i=1;i<num_faces-1;i++)
        diff_flux[1+num_species][i] = lambda_face[i]*tmp_face[1+num_species][i];

	diff_flux[1+num_species][0] = 0.0;
	diff_flux[1+num_species][num_faces-1] = 0.0;
	//diff_flux[1+num_species][0] = lambda[0]*tmp_face[1+num_species][0];
	//diff_flux[1+num_species][num_faces-1] = lambda[num_nodes-1]*tmp_face[1+num_species][num_faces-1];

	//+ 4/3*mu*du/dx*u

	for(int i=1;i<num_faces-1;i++)
		diff_flux[1+num_species][i] += stress_face[i]*u_face[i];
		//diff_flux[1+num_species][i] += 0.25*(const1*(mu[i]+mu[i-1])*tmp_face[0][i] - (P[i]+P[i-1]))*(u[i]+u[i-1]);

	diff_flux[1+num_species][0] += stress_face[0]*inletBC->u;
	diff_flux[1+num_species][num_faces-1] += stress_face[num_faces-1]*outletBC->u;
	//diff_flux[1+num_species][0] += (const1*mu[0]*tmp_face[0][0] - inletBC->P)*inletBC->u;
	//diff_flux[1+num_species][num_faces-1] += (const1*mu[num_nodes-1]*tmp_face[0][num_faces-1] - outletBC->P)*outletBC->u;

    double r_coeff,l_coeff;

	double local_sum = 0.0;
	for(int i=1;i<num_faces-1;i++){
		local_sum = 0.0;
        r_coeff = interp_f[i];
        l_coeff = (1.0 - r_coeff);
		for(int k=0;k<num_species;k++){
            local_sum += V_k[i][k]*(l_coeff*h_k[i-1][k]+r_coeff*h_k[i][k])*(l_coeff*Yk[i-1][k]+r_coeff*Yk[i][k]);
			//local_sum += 0.125*(V_k[i][k]+V_k[i-1][k])*(h_k[i][k]+h_k[i-1][k])*(Yk[i][k]+Yk[i-1][k]);
		}
		//conv_flux[2+num_species][i] +=  0.5*(rho[i]+rho[i-1])*local_sum;
		diff_flux[1+num_species][i] -=  rho_face[i]*local_sum;
	}

	//Note: V_k now evaluated at faces
	local_sum = 0.0;
	for(int k=0;k<num_species;k++){
		local_sum += V_k[0][k]*inletBC->hk[k]*inletBC->Yk[k];
		//local_sum += V_k[0][k]*h_k[0][k]*inletBC->Yk[k];
	}
	//conv_flux[2+num_species][0] += inletBC->rho*local_sum;
	diff_flux[1+num_species][0]  -= inletBC->rho*local_sum;


	local_sum = 0.0;
	for(int k=0;k<num_species;k++){
		local_sum += V_k[num_faces-1][k]*outletBC->hk[k]*outletBC->Yk[k];
		//local_sum += V_k[num_nodes-1][k]*outletBC->hk[k]*outletBC->Yk[k];
	}
	//conv_flux[2+num_species][num_faces-1] += outletBC->rho*local_sum;
	diff_flux[1+num_species][num_faces-1] -= outletBC->rho*local_sum;

}


void FiniteVolume::estimateConvectiveFluxes(double** qn, double* e_t, double* u,
		double* rho,double *P, double* V_c, double** Yk) {

	//Note : I used to use qn directly, but ran into all sorts of issues.

	// 1. Estimate rho*u

	for(int i=1;i<num_faces-1;i++)
		conv_flux[0][i] = rho_face[i]*u_face[i];

	conv_flux[0][0] = inletBC->rho*inletBC->u;
	conv_flux[0][num_faces-1] = outletBC->rho*outletBC->u;

	// 2. Estimate rho*u*u + P

	for(int i=1;i<num_faces-1;i++)
		conv_flux[1][i] = conv_flux[0][i]*u_face[i] + P_face[i];

	conv_flux[1][0] = conv_flux[0][0]*inletBC->u + inletBC->P;
	conv_flux[1][num_faces-1] = conv_flux[0][num_faces-1]*outletBC->u + outletBC->P;

	// 3. Estimate rho*Yk*(u+Vc)

    double r_coeff,l_coeff;

	for(int k=0;k<num_species;k++)
        for(int i=1;i<num_faces-1;i++){
            r_coeff = interp_f[i];
            l_coeff = (1.0 - r_coeff);
            conv_flux[2+k][i] = rho_face[i]*(l_coeff*Yk[i-1][k]+r_coeff*Yk[i][k])*(u_face[i]+V_c[i]);
			//conv_flux[2+k][i] = 0.125*(rho[i]+rho[i-1])*(Yk[i][k]+Yk[i-1][k])*(u[i]+u[i-1]+V_c[i]+V_c[i-1]);
    }
	// Note: V_c now evaluated at faces
	for(int k=0;k<num_species;k++){
		conv_flux[2+k][0] = inletBC->rho*inletBC->Yk[k]*(inletBC->u+V_c[0]);
		conv_flux[2+k][num_faces-1] = outletBC->rho*outletBC->Yk[k]*(outletBC->u+V_c[num_faces-1]);
	}


	// 4. Estimate rho*u*et  + rho*Sigma(hk*Yk*Vk)
	// 4. Estimate rho*u*et  + Pu

	for(int i=1;i<num_faces-1;i++)
		conv_flux[2+num_species][i] =  conv_flux[0][i]*(et_face[i]) + P_face[i]*u_face[i];
		//conv_flux[2+num_species][i] =  0.5*conv_flux[0][i]*(e_t[i]+e_t[i-1]) + 0.25*(P[i]+P[i-1])*(u[i]+u[i-1]);
		//conv_flux[2+num_species][i] = 0.5*(conv_flux[0][i]*(e[i]+e[i-1]) + 0.5*conv_flux[0][i]*(u[i]+u[i-1]));

	conv_flux[2+num_species][0] =  conv_flux[0][0]*(inletBC->e + 0.5*inletBC->u*inletBC->u) + inletBC->P*inletBC->u;
	conv_flux[2+num_species][num_faces-1] = conv_flux[0][num_faces-1]*(outletBC->e + 0.5*outletBC->u*outletBC->u) + outletBC->P*outletBC->u;


/*	double local_sum = 0.0;
	for(int i=1;i<num_faces-1;i++){
		local_sum = 0.0;
		for(int k=0;k<num_species;k++){
			local_sum += 0.25*(V_k[i][k])*(h_k[i][k]+h_k[i-1][k])*(Yk[i][k]+Yk[i-1][k]);
			//local_sum += 0.125*(V_k[i][k]+V_k[i-1][k])*(h_k[i][k]+h_k[i-1][k])*(Yk[i][k]+Yk[i-1][k]);
		}
		//conv_flux[2+num_species][i] +=  0.5*(rho[i]+rho[i-1])*local_sum;
		conv_flux[2+num_species][i] =  0.5*(rho[i]+rho[i-1])*local_sum;
	}

	//Note: V_k now evaluated at faces
	local_sum = 0.0;
	for(int k=0;k<num_species;k++){
		local_sum += V_k[0][k]*inletBC->hk[k]*inletBC->Yk[k];
		//local_sum += V_k[0][k]*h_k[0][k]*inletBC->Yk[k];
	}
	//conv_flux[2+num_species][0] += inletBC->rho*local_sum;
	conv_flux[2+num_species][0] = inletBC->rho*local_sum;


	local_sum = 0.0;
	for(int k=0;k<num_species;k++){
		local_sum += V_k[num_faces-1][k]*outletBC->hk[k]*outletBC->Yk[k];
		//local_sum += V_k[num_nodes-1][k]*outletBC->hk[k]*outletBC->Yk[k];
	}
	//conv_flux[2+num_species][num_faces-1] += outletBC->rho*local_sum;
	conv_flux[2+num_species][num_faces-1] = outletBC->rho*local_sum;
*/

}


void FiniteVolume::formRHS(double** omega_k, double** qn, double** rhs) {

	//double one_over_dx = 1.0/dx;
	/*for(int i=0;i<num_nodes;i++)
		one_over_dx[i] = 1.0/(face_pos[i+1]-face_pos[i]);*/

	//Add convective flux contributions
	for(int k=0;k<num_species+3;k++)
		for(int i=0;i<num_nodes;i++)
			rhs[k][i]  = -one_over_face_dx[i]*(conv_flux[k][i+1] - conv_flux[k][i]);
			//rhs[k][i]  = -one_over_dx*(tmp_upwind[k][i] - tmp_upwind[k][i-1]) - one_over_dx*(tmp_downwind[k][i+1] - tmp_downwind[k][i]);

	/*for(int k=0;k<num_species+3;k++){
		double local_max = absmax(rhs[k],num_nodes);
		std::cout<<" conv max "<<local_max<<std::endl;
	}*/

	//Add diffusive flux contributions
	for(int k=1;k<num_species+3;k++)
		for(int i=0;i<num_nodes;i++)
			rhs[k][i]  += one_over_face_dx[i]*(diff_flux[k-1][i+1]-diff_flux[k-1][i]);
			//rhs[k][i]  += one_over_dx*(flux[k-1][i]-flux[k-1][i-1]);

	/*for(int k=1;k<num_species+3;k++){
			double local_max = absmax(rhs[k],num_nodes);
			std::cout<<" diff max "<<local_max<<std::endl;
		}*/

	// Add chemical source terms
	for(int k=0;k<num_species;k++)
		for(int i=0;i<num_nodes;i++)
			rhs[2+k][i]  += omega_k[i][k];//*(face_pos[i+1]-face_pos[i]);

	/*for(int k=1;k<num_species+3;k++)
				double local_max = absmax(omega_k[i][k],num_nodes);
				cout<<" diff max "<<local_max<<std::endl;
			}*/

	// If not using total energy equation, add viscous heating and pressure work source terms
	//for(int i=0;i<num_nodes;i++)
	//	rhs[2+num_species][i]  += src_term[0][i];//*(face_pos[i+1]-face_pos[i]);


}


void FiniteVolume::Discretize(double** qn, double** rhs) {

	double *rho = fluid_obj->getRho();
	double *u   = fluid_obj->getU();
	double *P   = fluid_obj->getP();
	double *T   = fluid_obj->getT();
	double *e_t   = fluid_obj->getE_t();
	double *c   = fluid_obj->getC();

	//IdealGas *gas = (IdealGas*)fluid_obj;
	Mixture *mixobj = fluid_obj->getMixtureObj();
	double *W  = mixobj->getW();
	double *W_k  = mixobj->getW_k();
	double **X_k  = mixobj->getXk();
	double **Y_k  = mixobj->getYk();
	//double **Yk_face  = mixobj->getYkFace();
	double **del_Xk_Xk_face  = mixobj->getDelXk_Xk();

	double *mu = mixobj->getMu();
	double *lambda = mixobj->getLambda();

	Thermo *thermobj = mixobj->getThermoObj();

	double **h_k = thermobj->geth_k();

	CanteraTransport *transpobj = mixobj->getTransportObj();

	double **D_k = transpobj->getDk();
	double *V_c = transpobj->getVc();
	double **V_k = transpobj->getVk();

	CanteraKinetics *kinetics = mixobj->getKineticsObj();
	double **omega_k = kinetics->getOmegaK();

	double l_coeff = 0.0;
	double r_coeff = 0.0;

	for(int i=1;i<num_faces-1;i++){
		r_coeff = interp_f[i];
		l_coeff = (1.0 - r_coeff);
		u_face[i] = (l_coeff*u[i-1] + r_coeff*u[i]);
		rho_face[i] = (l_coeff*rho[i-1] + r_coeff*rho[i]);
		P_face[i] = (l_coeff*P[i-1] + r_coeff*P[i]);
		et_face[i] = (l_coeff*e_t[i-1] + r_coeff*e_t[i]);
		mu_face[i] = (l_coeff*mu[i-1] + r_coeff*mu[i]);
        lambda_face[i] = (l_coeff*lambda[i-1] + r_coeff*lambda[i]);
	}



	//,double *u,double *P,double *T,double *W,double *W_k,double **Xk,double **del_Xk_Xk,doublmu,double *lambda,double **D_k

	estimateDiffusiveFluxes(rho,u,T,W,W_k,X_k,del_Xk_Xk_face,mu,lambda,D_k,V_k,h_k,Y_k);
	estimateConvectiveFluxes(qn,e_t,u,rho,P,V_c,Y_k);
	formRHS(omega_k,qn,rhs);

}


FiniteVolume::~FiniteVolume() {

	for(int k=0;k<2+num_species;k++)
		delete [] tmp_face[k];
	delete tmp_face;

	for(int k=0;k<3+num_species;k++)
		delete [] diff_flux[k];
	delete diff_flux;

	for(int k=0;k<3+num_species;k++)
		delete [] conv_flux[k];
	delete conv_flux;

	for(int k=0;k<num_srcs;k++)
		delete [] src_term[k];
	delete src_term;

	//delete [] dx_face;

	delete [] u_face;
	delete [] P_face;
	delete [] rho_face;
	delete [] et_face;
	delete [] mu_face;
    delete [] lambda_face;
	delete [] stress_face;

	//delete [] one_over_dx;

	std::cout<<"Deallocating Numerics objects.."<<std::endl;

}

double** FiniteVolume::getConvFlux() const {
	return conv_flux;
}

double** FiniteVolume::getDiffFlux() const {
	return diff_flux;
}

double** FiniteVolume::getSrcTerm() const {
	return src_term;
}


double** FiniteVolume::getTmpFace() const {
	return tmp_face;
}

} /* namespace Flame */
