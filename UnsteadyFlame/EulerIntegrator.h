/*
 * EulerIntegrator.h
 *
 *  Created on: Jun 23, 2014
 *      Author: abishekg
 */

#ifndef EULERINTEGRATOR_H_
#define EULERINTEGRATOR_H_

#include "integrator.h"

namespace Flame {

class EulerIntegrator: public Integrator {
public:
	EulerIntegrator(int size_dim1,int size_dim2);
	virtual ~EulerIntegrator();
	void advance(void* solv_obj,double **qn,double **rhs,double dt);


private:

    int num_dim1;
    int num_dim2;

};

} /* namespace Flame */

#endif /* EULERINTEGRATOR_H_ */
