/*
 * Kinetics.cpp
 *
 *  Created on: Jun 10, 2014
 *      Author: abishekg
 */

#include "Kinetics.h"

namespace Flame {

Kinetics::Kinetics() {
	// TODO Auto-generated constructor stub

}

Kinetics::~Kinetics() {
	// TODO Auto-generated destructor stub
}

} /* namespace Flame */

double** Flame::Kinetics::getOmegaK() const {
	return omega_k;
}

void Flame::Kinetics::getProductSourceTerm_rhoYkT(double* rho, double** Yk,
		double* T, double* W_k) {
}

void Flame::Kinetics::getProductSourceTerm_PXkT(double* P, double** Xk,
		double* T, double* W_k) {
}
