/*
 * Mixture.cpp
 *
 *  Created on: Jun 11, 2014
 *      Author: abishekg
 */

#include "Mixture.h"

#include "PhysicalConstants.h"
#include "CanteraWrapper.h"
#include "ArrayUtils.h"
//#include "CanteraTransport.h"
#include<cmath>
#include<numeric>
#include<iostream>
namespace Flame {

Mixture::Mixture() {
	// TODO Auto-generated constructor stub

}

Mixture::Mixture(int npts, CanteraWrapper* cantera) {

	num_pts     = npts;
	num_species = cantera->getNumSpecies();

	initializeArrays();

	thermo_obj = new Thermo(num_pts,cantera);
    //transport_obj = new CanteraTransport(num_pts,cantera);
	transport_obj = new CanteraTransport(num_pts,cantera);
    //transport_obj = new Transport(num_pts,num_species);
	kinetics_obj = new CanteraKinetics(num_pts,cantera);

// TODO: move these initalizations

	for(int k=0;k<num_species;k++)
		Le_k[k] = 1.0;

	// H2,O2,H2O
	//Le_k[0] = 0.28;
	//Le_k[1] = 1.1;
	//Le_k[2] = 0.9;


	cantera->copyMolecularWeights(W_k);

	for(int k=0;k<num_species;k++)
		One_over_W_k[k]=1.0/W_k[k];

	for(int k=0;k<num_species;k++)
		R_sp[k] = PhysicalConstants::R_si*One_over_W_k[k];

}


Mixture::Mixture(int npts, int nk) {

	num_pts     = npts;
	num_species = nk;

	/*initializeArrays();

	thermo_obj = new Thermo(num_pts,num_species);
	transport_obj = new Transport(num_pts,num_species);
	kinetics_obj = new Kinetics(num_pts,num_species);

// TODO: move these initalizations

	for(int k=0;k<num_species;k++)
		Le_k[k] = 1.0;

	W_k[0]=2.01588*0.001;
	W_k[1]=31.99880*0.001;
	W_k[2]=18.01528*0.001;

	for(int k=0;k<num_species;k++)
		One_over_W_k[k]=1.0/W_k[k];

	for(int k=0;k<num_species;k++)
		R_sp[k] = PhysicalConstants::R_si*One_over_W_k[k];*/


}


void Mixture::initializeArrays() {

	Yk = new double*[num_pts];
	for(int i=0;i<num_pts;i++)
		Yk[i] = new double[num_species];

	/*Yk_face = new double*[num_pts+1];
	for(int i=0;i<num_pts+1;i++)
		Yk_face[i] = new double[num_species];*/

	Xk = new double*[num_pts];
	for(int i=0;i<num_pts;i++)
		Xk[i] = new double[num_species];

	/*Xk_face = new double*[num_pts+1];
	for(int i=0;i<num_pts+1;i++)
		Xk_face[i] = new double[num_species];*/


	W_k = new double[num_species];
	R_sp = new double[num_species];
	Le_k = new double[num_species];
	One_over_W_k = new double[num_species];

	W = new double[num_pts];
	W_face = new double[num_pts+1];

	//del_Xk_Xk = new double*[num_pts];
	del_Xk_Xk = new double*[num_pts+1];
	for(int i=0;i<num_pts+1;i++)
		del_Xk_Xk[i] = new double[num_species];

	Cp =  new double[num_pts];
	Cp_molar =  new double[num_pts];
	mu =  new double[num_pts];
	lambda =  new double[num_pts];


	/*Yk = new double*[num_species];
		for(int k=0;k<num_species;k++)
			Yk[k] = new double[num_pts];*/

}


Mixture::~Mixture() {

	for(int i=0;i<num_pts;i++)
		delete [] Yk[i];
	delete Yk;

	/*for(int i=0;i<num_pts+1;i++)
		delete [] Yk_face[i];
	delete Yk_face;*/


	for(int i=0;i<num_pts;i++)
		delete [] Xk[i];
	delete Xk;


	/*for(int i=0;i<num_pts+1;i++)
		delete [] Xk_face[i];
	delete Xk_face;*/

	delete [] W_k;
	delete [] R_sp;
	delete [] Le_k;
	delete [] One_over_W_k;
	delete [] W;
	delete [] W_face;

	for(int i=0;i<num_pts;i++)
		delete [] del_Xk_Xk[i];
	delete del_Xk_Xk;

	delete [] Cp;
	delete [] Cp_molar;
	delete [] mu;
	delete [] lambda;

	for(int k=0;k<num_species;k++)
		delete [] phi[k];
	delete phi;

	for(int k=0;k<num_species;k++)
		delete [] phi_tmp1[k];
	delete phi_tmp1;

	for(int k=0;k<num_species;k++)
		delete [] phi_tmp2[k];
	delete phi_tmp2;

	for(int k=0;k<num_species;k++)
		delete [] Wk_k_over_j[k];
	delete Wk_k_over_j;

	delete [] phi_tmp3;

	delete thermo_obj;
	delete transport_obj;
	delete kinetics_obj;




	std::cout<<"Deallocating Mixture objects.."<<std::endl;
	// TODO Auto-generated destructor stub
}

Thermo*& Mixture::getThermoObj() {
	return thermo_obj;
}

CanteraTransport*& Mixture::getTransportObj() {
	return transport_obj;
}

double** Mixture::getDelXk_Xk() const {
	return del_Xk_Xk;
}

double* Mixture::getW() const {
	return W;
}

double* Mixture::getWFace() const {
	return W_face;
}

double* Mixture::getW_k() const {
	return W_k;
}

double** Mixture::getXk() const {
	return Xk;
}


double** Mixture::getXkFace() const {
	return Xk_face;
}

double** Mixture::getYk() const {
	return Yk;
}


void Mixture::estimateMixtureMolecularWeight(GridOperations *grid_ops) {


	for(int i=0;i<num_pts;i++)
		//W[i]=1.0/inner_prod(num_species,One_over_W_k, Yk[i]);
		W[i]=1.0/std::inner_product(One_over_W_k,One_over_W_k+num_species,Yk[i],0.0);

	//for(int i=1;i<num_pts;i++)
	//	W_face[i] = 0.5*(W[i]+W[i-1]);

	grid_ops->estimateFaceVars(W,W_face);

	// Transport uses inlet->W, outlet->W at boundaries
	//W_face[0] = W[0];
	//W_face[num_pts] = W[num_pts-1];

}


void Flame::Mixture::estimateYkFace() {

	/*double **Yk_ptr = getYk();
	double **Yk_face_ptr = getYkFace();

	for(int i=1;i<num_pts;i++)
		for(int k=0;k<num_species;k++){
			Yk_face_ptr[i][k]=0.5*(Yk_ptr[i][k]+Yk_ptr[i-1][k]);
	}*/


}



void Mixture::estimateXkfromYk(GridOperations *grid_ops) {

	double **Xk_ptr = getXk();
	//double **Xk_face_ptr = getXkFace();
	double **Yk_ptr = getYk();
	double *W_ptr = getW();
	double *One_over_Wk_ptr = getOneOverWK();

	for(int i=0;i<num_pts;i++)
		for(int k=0;k<num_species;k++){
			Xk_ptr[i][k]=Yk_ptr[i][k]*W_ptr[i]*One_over_Wk_ptr[k];
	}

	//TODO: dirty impl. Mixture shouldn't know abt num faces or pts or boundary conditions
	// equivalent to i< num_faces-1

	/*for(int i=1;i<num_pts;i++)
		for(int k=0;k<num_species;k++)
			Xk_face_ptr[i][k]= 0.5*(Xk_ptr[i-1][k]+Xk_ptr[i][k]);*/



	double **DelXk_Xk_ptr = getDelXk_Xk();

	grid_ops->estimateDelQoverQ(Xk_ptr,DelXk_Xk_ptr,num_species);

	//grid_ops->estimateDelXkoverXk(Xk_ptr,DelXk_Xk_ptr,num_species);

	/*for(int i=1;i<num_pts-1;i++)
			for(int k=0;k<num_species;k++){
				DelXk_Xk_ptr[i][k]=(Xk_ptr[i+1][k]-Xk_ptr[i-1][k])/(2.0*dx*Xk_ptr[i][k]);

		}

	for(int k=0;k<num_species;k++){
		DelXk_Xk_ptr[0][k] = DelXk_Xk_ptr[1][k];
		DelXk_Xk_ptr[num_pts-1][k] = DelXk_Xk_ptr[num_pts-2][k];
	}

	for(int i=0;i<num_pts;i++)
		for(int k=0;k<num_species;k++)
			if(Xk_ptr[i][k]<1.0e-8)
				DelXk_Xk_ptr[i][k]=0.0;
*/

}

void Mixture::estimateMixtureSpecificHeats() {

	double **Cpk = thermo_obj->getCp_k();
	double **Yk  = getYk();
	double *W_ptr  = getW();

	for(int i=0;i<num_pts;i++)
		//Cp[i] = inner_prod(num_species,Cpk[i], Yk[i]);
		Cp[i] = std::inner_product(Cpk[i],Cpk[i]+num_species,Yk[i],0.0);

	for(int i=0;i<num_pts;i++)
		Cp_molar[i]=Cp[i]*W_ptr[i];

}

void Mixture::estimateMixtureViscosities() {

	//double phi[num_species][num_species];  //Note: pre-initialized

	double **Xk  = getXk();
	double *Wk   = getW_k();
	double **mu_k = getTransportObj()->getMuK();
	double **mu_sqroot_k = getTransportObj()->getMuSqrootK();
	double **lambda_k = getTransportObj()->getLambdak();
	double m_tmp;

	double visc_j_over_k = 0.0;

	for(int i=0;i<num_pts;i++){

	for(int k=0;k<num_species;++k){
		phi[k][k] = 1.0;
		for(int j=k+1;j<num_species;++j){
		//for(int j=0;j<num_species;j++){
			visc_j_over_k = mu_k[i][j]/mu_k[i][k];

			m_tmp =1.0 + mu_sqroot_k[i][k]/mu_sqroot_k[i][j]*phi_tmp2[k][j];

			phi[k][j] = phi_tmp1[k][j]*m_tmp*m_tmp;

			phi[j][k] = visc_j_over_k*Wk_k_over_j[k][j]*phi[k][j];
		}
	}
			//phi[k][j] = phi_tmp1[k][j]*std::pow((1.0+std::sqrt(mu_k[i][k]/mu_k[i][j])*phi_tmp2[k][j]),2.0);

	for(int k=0;k<num_species;k++)
		phi_tmp3[k] = mu_k[i][k]/inner_prod(num_species,Xk[i],phi[k]);
		//phi_tmp3[k] = 1.0/inner_prod(num_species,Xk[i],phi[k]);
		//phi_tmp3[k] = 1.0/std::inner_product(Xk[i],Xk[i]+num_species,phi[k],0.0);

	mu[i]   = inner_prod(num_species,Xk[i],phi_tmp3);
	//mu[i] = inner_3prod(num_species,Xk[i],mu_k[i],phi_tmp3);




	/*mu[i]=0.0;
	//double *Xk_i = Xk[i];
	for(int k=0;k<num_species;k++)
		//mu[i] += Xk[i][k]*mu_k[i][k]/inner_prod(num_species,Xk[i],phi[k]);
		mu[i] += Xk[i][k]*mu_k[i][k]/(std::inner_product(Xk[i],Xk[i]+num_species,phi[k],0.0));
*/

	/*lambda[i]=0.0;
		//double *Xk_i = Xk[i];
	for(int k=0;k<num_species;k++)
		//lambda[i] += Xk[i][k]*lambda_k[i][k]/inner_prod(num_species,Xk[i],phi[k]);
		lambda[i] += Xk[i][k]*lambda_k[i][k]/(std::inner_product(Xk[i],Xk[i]+num_species,phi[k],0.0));*/

	//lambda[i] = 0.5*(std::inner_product(Xk[i],Xk[i]+num_species,lambda_k[i],0.0)+1.0/divide_each(num_species,Xk[i],lambda_k[i]));
	lambda[i] = 0.5*(inner_prod(num_species,Xk[i],lambda_k[i])+1.0/divide_each(num_species,Xk[i],lambda_k[i]));

	//lambda[i] = inner_3prod(num_species,Xk[i],lambda_k[i],phi_tmp3);





	}



}

void Mixture::preWilkesRuleCompute() {

	phi = new double*[num_species];
	phi_tmp1 = new double*[num_species];
	phi_tmp2 = new double*[num_species];

	Wk_k_over_j = new double*[num_species];

	phi_tmp3 = new double[num_species];

	for(int k=0;k<num_species;k++){
		phi[k] = new double[num_species];
		phi_tmp1[k] = new double[num_species];
		phi_tmp2[k] = new double[num_species];
		Wk_k_over_j[k] = new double[num_species];
	}

	for(int k=0;k<num_species;k++)
		for(int j=0;j<num_species;j++){
			Wk_k_over_j[k][j] =  W_k[k]/W_k[j];
	        phi_tmp1[k][j] = sqrt(1.0/(8.0*(1.0+Wk_k_over_j[k][j])));
	        ///phi_tmp2[k][j] = pow((Wk_k_over_j[k][j]),0.25);  Wrong!!
	        phi_tmp2[k][j] = pow((W_k[j]/W_k[k]),0.25);
		}

}

void Mixture::updateMixtureProperties(GridOperations *grid_ops,double num_low_T,double num_high_T,int *low_T_indices,int *high_T_indices,double *T_ptr,double *rho_ptr,double *P_ptr) {

	//estimateYkFace();

	estimateXkfromYk(grid_ops);

	getThermoObj()->getThermoProperties(num_low_T,num_high_T,low_T_indices,high_T_indices,T_ptr,R_sp);

	estimateMixtureSpecificHeats();

	getTransportObj()->getTransportProperties(num_low_T,num_high_T,low_T_indices,high_T_indices,T_ptr,P_ptr,Xk,Yk);
	//getTransportObj()->getTransportProperties(num_low_T,num_high_T,low_T_indices,high_T_indices,T_ptr);

	estimateMixtureViscosities();

	estimateMixtureConductivities();

	//getTransportObj()->getDiffusionProperties(lambda,rho_ptr,Cp,Le_k,W_k,W,del_Xk_Xk,Xk);
	getTransportObj()->getDiffusionProperties(grid_ops,lambda,rho_ptr,Cp,Le_k,W_k,W_face,del_Xk_Xk,Xk);


}

void Mixture::updateBoundaryProperties(GridOperations *grid_ops,FaceData *inlet,FaceData *outlet) {


	grid_ops->estimateDelXkoverXkBoundaries(Xk,del_Xk_Xk,inlet->Xk,outlet->Xk,num_species);

	inlet->lambda = lambda[0];
	outlet->lambda = lambda[num_pts-1];

	getTransportObj()->estimateBoundaryProperties(W_k,del_Xk_Xk,inlet,outlet,Le_k);




}

void Mixture::updateMixtureChemistry(double* rho, double* T) {

	double **Yk  = getYk();
	double *W_k  = getW_k();

	//OneStepKinetics* kinetics=(OneStepKinetics* )getKineticsObj();
	CanteraKinetics* kinetics=getKineticsObj();
	kinetics->getProductSourceTerm_rhoYkT(rho,Yk,T,W_k);
}



void Mixture::updateMixtureChemistry(double* rho, double* T, double *P) {

	double **Yk  = getYk();
	double *W_k  = getW_k();
	double *W_ptr  = getW();

	//OneStepKinetics* kinetics=(OneStepKinetics* )getKineticsObj();
	CanteraKinetics* kinetics=getKineticsObj();
	kinetics->getProductSourceTerm_rhoYkTWP(rho,Yk,T,W_k,W_ptr,P);
	//kinetics->getProductSourceTerm_rhoYkT(rho,Yk,T,W_k);
}


void Mixture::updateMixtureChemistry_PXk(double* P, double* T) {

	double **Xk  = getXk();
	double *W_k  = getW_k();

	//OneStepKinetics* kinetics=(OneStepKinetics* )getKineticsObj();
	CanteraKinetics* kinetics=getKineticsObj();
	kinetics->getProductSourceTerm_PXkT(P,Xk,T,W_k);
}

void Mixture::estimateMixtureConductivities() {

	/*double phi[num_species][num_species];  //TODO: pre-initialize?

	double **Xk  = getXk();
	double **lambda_k = getTransportObj()->getLambdak();

	for(int i=0;i<num_pts;i++){

	for(int k=0;k<num_species;k++)
		for(int j=0;j<num_species;j++)
			phi[k][j] = phi_tmp1[k][j]*pow((1.0+std::sqrt(lambda_k[i][k]/lambda_k[i][j])*phi_tmp2[k][j]),2.0);

	lambda[i]=0.0;
	double *Xk_i = Xk[i];
	for(int k=0;k<num_species;k++)
		lambda[i] += Xk_i[k]*lambda_k[i][k]/(std::inner_product(Xk_i,Xk_i+num_species,phi[k],0.0));

	}*/
}

double* Mixture::getOneOverWK() const {
return One_over_W_k;
}


CanteraKinetics *& Mixture::getKineticsObj(){
	return kinetics_obj;
}

double* Mixture::getLambda() const {
	return lambda;
}

double* Mixture::getMu() const {
	return mu;
}

double* Mixture::getCp() const {
	return Cp;
}

} /* namespace Flame */

void Flame::Mixture::setCp(double* cp) {
	Cp = cp;
}

double* Flame::Mixture::getR_sp() const {
	return R_sp;
}

double* Flame::Mixture::getCpMolar() const {
	return Cp_molar;
}

int Flame::Mixture::getNumSpecies() const {
	return num_species;
}

