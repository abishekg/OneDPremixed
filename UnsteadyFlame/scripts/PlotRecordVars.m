function [ output_args ] = PlotRecordVars( filename )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    if nargin < 1
      %filename = 'records.bin'
      filename = '/home/abishekg/simulations/OneDPremixed/tests_controller/test3/records.bin';
      filename2 = '/home/abishekg/simulations/OneDPremixed/tests_controller/test2/records.bin';
    end

    u_target = 2.00;
    P_target = 1.0e5;
    T_target = 300.0;
    
    num_record = 7;
    %record_int = 200000;
    %record_int = 340000;
    record_int = 800000;
    %record_int = 250000;
    
    headerlinesIn = 1;
    
    fileID = fopen(filename);
    A = fread(fileID,[num_record record_int],'double')
    
     fileID = fopen(filename2);
    B = fread(fileID,[num_record record_int],'double')
    
    %A = importdata(filename,delimiterIn,headerlinesIn);


    figure(1);
    plot(A(1,:),A(3,:),'-b');
    hold on;
    plot(A(1,:),A(4,:),'-r');
    title('Ku = 8.0e-4; Pu = 2.0e-4s; K_p = 0.45*Ku; K_i = 1.2*K_p/Pu;');
    ylabel(' velocity (m/s)');
    xlabel('time(seconds)');
    
    plot(B(1,:),B(3,:),'.b');
    %hold on;
    plot(B(1,:),B(4,:),'.r');
    
    legend('Flame speed','Inlet velocity');
    ylim([-100 100]);
    
    hold off;
    
        %SP=x(i); %your point goes here 
%line([SP SP],get(hax,'YLim'),'Color',[0 0 0])
    
    figure(2);
    
    plot(A(2,:),A(1,:));
    hold on;
    plot(B(2,:),B(1,:),'k');
    xlim([0.0 0.01]);
    ylabel(' time(seconds)');
    xlabel('position(m)');
    
    figure(3);
    subplot(4,1,1);
    plot(A(1,:),A(4,:));
    hold on;
    plot(B(1,:),B(4,:),'k');
    hline = refline([0 u_target]);
    set(hline,'Color','r');
    hold off;
    xlabel(' time(seconds)');
    ylabel('inlet velocity');
    
    subplot(4,1,2);
    plot(A(1,:),A(5,:));
    hold on;
    plot(B(1,:),B(5,:),'k');
    hline = refline([0 T_target]);
    set(hline,'Color','r');
    hold off;
    xlabel(' time(seconds)');
    ylabel('inlet temperature');
    
    subplot(4,1,3);
    plot(A(1,:),A(6,:));
    hold on;
    plot(B(1,:),B(6,:),'k');
    hline = refline([0 P_target]);
    set(hline,'Color','r');
    hold off;
    xlabel(' time(seconds)');
    ylabel('outlet pressure');
    
    subplot(4,1,4);
    plot(A(1,:),A(7,:));
    hold on;
    plot(B(1,:),B(7,:),'k');
    xlabel(' time(seconds)');
    ylabel('outlet temperature');
    
    
        %A.colheaders{1, k}
    %subplot(2,2,1);
    %plot_syms={':','--','-'};
    
    %
    %for ki=7:9, %TODO: change later
    %    plot(A.data(:, 1),A.data(:, ki),plot_syms{ki-6});
    %    hold on;
    %end
    %legend(species_sym);
    %hold off;

    %subplot(2,2,2); %T
    %plot(A.data(:, 1),A.data(:, 4));

    %subplot(2,2,3); %u
    %plot(A.data(:, 1),A.data(:, 3));

    %subplot(2,2,4);
    %plot(A.data(:, 1),A.data(:, 10));
    %plot(x,rho);




end
