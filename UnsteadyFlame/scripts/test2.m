[X,Y] = meshgrid(-4.2:0.01:4.2,-4.2:0.01:4.2);


mu = X + 1i*Y;

%G = zeros(nx,nx);

G = abs(1.0 + mu + mu.*mu/2.0);

%G = (1.0 + mu + mu.*mu/2.0+ mu.*mu.*mu/6.0 +mu.*mu.*mu.*mu/24.0 );

%contour(X,Y,G,[-1,1]);
%contour(G,[-1 1]);

hold on;

%G = abs(1.0 + mu + mu.*mu/2.0+ mu.*mu.*mu/6.0 +mu.*mu.*mu.*mu/24.0 );

%contour(X,Y,G,[-1,1],'b');
%contour(G,[-1 1]);


G = abs(1.0 + mu + mu.*mu*5.0/12.0+ mu.*mu.*mu/8.0 +mu.*mu.*mu.*mu/48.0 );


contour(X,Y,G,[-1,1],'r');

xlim([-4.2 1]);
xlim([-4.2 4.2]);

hold off;