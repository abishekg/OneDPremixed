[X,Y] = meshgrid(-4:0.01:4,-4:0.01:4);

s = size(X);
nx = s(1);

G = zeros(nx,nx);

for k=1:nx
    for j=1:nx
        mu = X(k,j) + 1i*Y(k,j);
            G(k,j) = (1.0 + mu + mu*mu/2.0); 
    end
end

contour(X,Y,G);