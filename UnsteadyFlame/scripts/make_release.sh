cmake -D CMAKE_BUILD_TYPE:STRING=Release .
make
if [ "$?" = "0" ]; then 
   ./UnsteadyFlame
else
   echo "Errors during build. Halting script." 1>&2
   exit 1
fi