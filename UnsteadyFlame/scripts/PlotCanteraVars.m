function [ output_args ] = PlotCanteraVars( filename )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
 


    clc;
    clear all;
    close all;
    
    
linestyles = cellstr(char('-',':','-.','--','-',':','-.','--','-',':','-',':',...
'-.','--','-',':','-.','--','-',':','-.'));
 

Markers=['o','x','+','*','s','d','v','^','<','>','p','h','.',...
'+','*','o','x','^','<','h','.','>','p','s','d','v',...
'o','x','+','*','s','d','v','^','<','>','p','h','.'];
    
    
     if nargin < 1
      filename = 'flamespeed.csv'
    end

    delimiterIn = ',';
    headerlinesIn = 1;
    A = importdata(filename,delimiterIn,headerlinesIn);
   
    %smooth = 'off';
    smooth = 'on';
    
    %num_pts = 4096;
    %num_pts = 512;
    %num_pts = 2048;
    %num_pts = 1024;
    %num_species = 3;
    num_species = 9;
    %num_species = 18;
    
    dat = A.data
    num_pts = size(dat,1);
    
    
    num_base = 6;
    
    %num_rows = num_base+ num_species+ num_species + num_species;
    num_rows = 12;
    
    MarkerEdgeColors=jet(num_species);  % n is the number of different items you have
    
    
  
    figure('name','Flame Variables','units','normalized','outerposition',[0 0 1 1]);
    grid on;
        %A.colheaders{1, k}
    subplot(2,2,1);
    %plot_syms={':','--','-'};
    
    cc = hsv(num_species);
    %cc = distinguishable_colors(num_species);
    
    %plot_syms={'r','b','k'};
    %species_sym = {'H2','O2','H2O'};
    plot_syms={'-b','-g','-r','-y','-d','--','-',':','o','x'};
  species_sym = {'H2','H','O','O2','OH','H2O','HO2','H2O2','AR'};
    
    %species_sym = {'N2','H','O2','OH','O','H2','H2O','HO2','H2O2'};
 

    
    %species_sym = {'H2','H','O','O2','OH','H2O','HO2','H2O2','N','NH','NH2','NH3','NNH','NO','NO2','N2O','HNO','N2'};

    
    %
%     for ki=num_base+1:num_base+num_species, %TODO: change later
%         %plot(A(:,1),A(:,ki));
%         %plot(A(:,1),A(:,ki),plot_syms{ki-num_base},'LineWidth',1,'LineSmoothing',smooth,'Color',cc(ki,:));
%         plot(A(:,1),A(:,ki),'LineWidth',1.0,'LineSmoothing',smooth,'Color',cc(ki-num_base,:));
%         hold on;
%     end
%     legend(species_sym);
%     grid on;
%     hold off;

    max_x = max(dat(:,1));
    
    plot(dat(:,1),dat(:,7),'LineWidth',1,'LineSmoothing',smooth);
    xlabel('Position(m)');
    ylabel('Density (kg/m^3)');
    xlim([0.0 max_x]);
     grid on;
    hold off;

    subplot(2,2,2); %T
    plot(dat(:,1),dat(:,2),'LineWidth',1.2,'LineSmoothing',smooth,'Color',cc(1,:));
    xlabel('Position(m)');
    ylabel('Temperature (Kelvin)');
     xlim([0.0 max_x]);
    grid on;

    subplot(2,2,3); %u
    plot(dat(:,1),dat(:,3),'LineWidth',1,'LineSmoothing',smooth);
    xlabel('Position(m)');
    ylabel('Velocity (m/s)');
     xlim([0.0 max_x]);
     grid on;

    subplot(2,2,4); %P
    plot(dat(:,1),dat(:,6),'LineWidth',1,'LineSmoothing',smooth);
    xlabel('Position(m)');
    ylabel('Pressure (Pa)');
     xlim([0.0 max_x]);
     grid on;
    %plot(x,rho);
    
    %figure(2);
    %plot(A(:,1),A(:,2),'LineWidth',1,'LineSmoothing',smooth);
    %grid on;

    %figure(3);
    %plot(A(:,1),A(:,5),'LineWidth',1,'LineSmoothing',smooth);
    %grid on;
    
    %figure(4);
    %for ki=num_base+num_species+1:num_base+num_species+num_species, %TODO: change later
        %plot(A(:,1),A(:,ki));
        %plot(A(:,1),A(:,ki),plot_syms{ki-num_base-num_species},'LineWidth',1,'LineSmoothing',smooth,'Color',cc(ki,:));
    %    plot(A(:,1),A(:,ki),'LineWidth',1,'LineSmoothing',smooth,'Color',cc(ki-num_base-num_species,:));
    %    hold on;
    %end
    %legend(species_sym);
    %grid on;
    %hold off;
    
    figure(2);
    subplot(2,2,1); %T
    plot(dat(:,1),dat(:,9),'LineWidth',1,'LineSmoothing',smooth);
    xlabel('Position(m)');
    ylabel('Cp');
     xlim([0.0 max_x]);
     grid on;
    hold off;

    subplot(2,2,2); %T
    plot(dat(:,1),dat(:,10),'LineWidth',1.2,'LineSmoothing',smooth,'Color',cc(1,:));
    xlabel('Position(m)');
    ylabel('Viscosity');
     xlim([0.0 max_x]);
    grid on;

    subplot(2,2,3); %u
    plot(dat(:,1),dat(:,11),'LineWidth',1,'LineSmoothing',smooth);
    xlabel('Position(m)');
    ylabel(' lambda');
     xlim([0.0 max_x]);
     grid on;

    subplot(2,2,4); %P
    plot(dat(:,1),dat(:,12),'LineWidth',1,'LineSmoothing',smooth);
    xlabel('Position(m)');
    ylabel('Mol Diff Dk_1');
     xlim([0.0 max_x]);
     grid on;
    
    figure(3);
    plot(dat(:,1),dat(:,8),'LineWidth',1,'LineSmoothing',smooth);
     xlim([0.0 max_x]);
    grid on;
    
  

end

