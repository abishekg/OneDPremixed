function [ output_args ] = PlotFlameVars( filename )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    if nargin < 1
      filename = 'op.csv'
    end

    delimiterIn = ',';
    headerlinesIn = 1;
    A = importdata(filename,delimiterIn,headerlinesIn);


    figure(1);
        %A.colheaders{1, k}
    subplot(2,2,1);
    plot_syms={':','--','-'};
    species_sym = {'H2','O2','H2O'};
    %
    for ki=7:9, %TODO: change later
        plot(A.data(:, 1),A.data(:, ki),plot_syms{ki-6});
        hold on;
    end
    legend(species_sym);
    hold off;

    subplot(2,2,2); %T
    plot(A.data(:, 1),A.data(:, 4));

    subplot(2,2,3); %u
    plot(A.data(:, 1),A.data(:, 3));

    subplot(2,2,4);
    plot(A.data(:, 1),A.data(:, 10));
    %plot(x,rho);




end

