function [ output_args ] = PlotRecordVars( filename )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    if nargin < 1
      filename = 'records.bin'
      %filename = '/Users/abishekg/etna_remote/simulations/OneDPremixed/tests_controller/test2/records.bin';
      %filename = '/Users/abishekg/etna_remote/simulations/OneDPremixed/test_AR_phi0.6/records.bin';
      %filename = '/Users/abishekg/simulations/test_fc_param/caseC/records.bin';
    end

    u_target = 2.00;
    P_target = 1.0e5;
    T_target = 300.0;
    
    num_record = 7;
    %record_int = 200000;
    %record_int = 340000;
    record_int = 800000;
    %record_int = 250000;
    
    headerlinesIn = 1;
    
    fileID = fopen(filename);
    A = fread(fileID,[num_record record_int],'double')
    
    %A = importdata(filename,delimiterIn,headerlinesIn);

    figure(1);
    subplot(2,1,1);
    semilogy(A(1,:),A(2,:));
    hold on;
    semilogy(A(1,:),A(6,:),'-k');
    semilogy(A(1,:),A(7,:),'-r');
    hold off;
    xlabel(' time(seconds)');
    ylabel('spectral radius');
    legend('Inviscid','Diffusive','Source');
    
    subplot(2,1,2);
    plot(A(1,:),A(3,:));
    hold on;
    plot(A(1,:),A(4,:),'-k');
    plot(A(1,:),A(5,:),'-r');
    hold off;
    xlabel(' time(seconds)');
    ylabel('Diffusive terms');
    legend('Viscous','Molecular','Thermal');
    
    
        %A.colheaders{1, k}
    %subplot(2,2,1);
    %plot_syms={':','--','-'};
    
    %
    %for ki=7:9, %TODO: change later
    %    plot(A.data(:, 1),A.data(:, ki),plot_syms{ki-6});
    %    hold on;
    %end
    %legend(species_sym);
    %hold off;

    %subplot(2,2,2); %T
    %plot(A.data(:, 1),A.data(:, 4));

    %subplot(2,2,3); %u
    %plot(A.data(:, 1),A.data(:, 3));

    %subplot(2,2,4);
    %plot(A.data(:, 1),A.data(:, 10));
    %plot(x,rho);




end
