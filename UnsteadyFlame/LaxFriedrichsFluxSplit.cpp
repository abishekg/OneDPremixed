/*
 * LaxFriedrichsFluxSplit.cpp
 *
 *  Created on: Jun 14, 2014
 *      Author: abishekg
 */
/*
#include "LaxFriedrichsFluxSplit.h"
#include "Mixture.h"
#include "Transport.h"
#include "Thermo.h"
#include "IdealGas.h"
#include "OneStepKinetics.h"
#include <cmath>
#include <iostream>
namespace Flame {

LaxFriedrichsFluxSplit::LaxFriedrichsFluxSplit(int nx,int nk,double dx) {


	num_nodes = nx;
	//num_faces = num_nodes - 1;
	num_faces = num_nodes + 1;
	num_species = nk;
	this->dx  = dx;
	this->dx_PE = dx;

	tmp_fa = new double*[2+num_species];
	for(int k=0;k<2+num_species;k++)
		tmp_fa[k] = new double[num_faces];

	tmp_node = new double*[3+num_species];
	for(int k=0;k<3+num_species;k++)
		tmp_node[k] = new double[num_nodes];

	tmp_src = new double[num_nodes];

	tmp_upwind = new double*[3+num_species];
	for(int k=0;k<3+num_species;k++)
		tmp_upwind[k] = new double[num_nodes];

	tmp_downwind = new double*[3+num_species];
	for(int k=0;k<3+num_species;k++)
		tmp_downwind[k] = new double[num_nodes];

	flux = new double*[3+num_species];
	for(int k=0;k<3+num_species;k++)
		flux[k] = new double[num_faces];



}

LaxFriedrichsFluxSplit::~LaxFriedrichsFluxSplit() {

	for(int k=0;k<2+num_species;k++)
		delete [] tmp_fa[k];
	delete tmp_fa;

	for(int k=0;k<3+num_species;k++)
		delete [] tmp_node[k];
	delete tmp_node;

	delete [] tmp_src;

	for(int k=0;k<3+num_species;k++)
		delete [] tmp_upwind[k];
	delete tmp_upwind;

	for(int k=0;k<3+num_species;k++)
		delete [] tmp_downwind[k];
	delete tmp_downwind;

	for(int k=0;k<3+num_species;k++)
		delete [] flux[k];
	delete flux;

	std::cout<<"Deallocating Numerics objects.."<<std::endl;
	// TODO Auto-generated destructor stub
}

void LaxFriedrichsFluxSplit::estimateFaceFluxes(double *rho,double *u,double *P,double *T,double *W,double *W_k,double **Xk,double **del_Xk_Xk,double *mu,double *lambda,double **D_k) {

	double **tmp_fa = this->getTmpFa();
	double **flux   = this->getFlux();
	double *tmp_src   = this->getTmpSrc();
	double tmp_dudx_central=0.0;

	for(int i=1;i<num_faces-1;i++)
		tmp_fa[0][i] = (u[i]-u[i-1])/dx_PE;

	tmp_fa[0][0] = (u[0]-u[x])/dx_PE;
	tmp_fa[0][num_faces-1] = (u[x]-u[num_nodes-1])/dx_PE;


	//for(int k=0;k<num_species;k++)
	for(int i=1;i<num_faces-1;i++)
		for(int k=0;k<num_species;k++)
			tmp_fa[1+k][i] = (Xk[i][k] - Xk[i-1][k])/dx_PE;
			//tmp_fa[1+k][i] = 0.5*( del_Xk_Xk[i+1][k]*Xk[i+1][k] + del_Xk_Xk[i][k]*Xk[i][k] );
	//TODO: boundary conditions
	for(int k=0;k<num_species;k++)
		tmp_fa[1+k][i] = (Xk[i][k] - Xk[i-1][k])/dx_PE;
	for(int k=0;k<num_species;k++)
		tmp_fa[1+k][i] = (Xk[i][k] - Xk[i-1][k])/dx_PE;


	for(int i=1;i<num_faces-1;i++)
		tmp_fa[1+num_species][i] = (T[i]-T[i-1])/dx_PE;
	//TODO: BCs
	tmp_fa[1+num_species][0] =    (T[i]-T[i-1])/dx_PE;
	tmp_fa[1+num_species][num_faces-1] = (T[i]-T[i-1])/dx_PE;

	//for(int i=0;i<num_nodes-1;i++)
	//	if(std::isnan(P[i]))std::cout<<"EXCEPTION!!!! : Pressure is Nan at.."<<i<<std::endl;

	for(int i=1;i<num_faces-1;i++)
		//flux[0][i] = 2.0/3.0*(mu[i+1] +mu[i])*tmp_fa[0][i]  ;
		flux[0][i] = 0.5*( 4.0/3.0*(mu[i] +mu[i-1])*tmp_fa[0][i] - (P[i] +P[i-1]) );

	flux[0][i] = 0.5*( 4.0/3.0*(mu[i-1] +mu[i])*tmp_fa[0][i] - (P[i-1] +P[i]) );
	flux[0][i] = 0.5*( 4.0/3.0*(mu[i-1] +mu[i])*tmp_fa[0][i] - (P[i-1] +P[i]) );

	for(int k=0;k<num_species;k++)
		for(int i=1;i<num_faces-1;i++)
			flux[1+k][i] = 1.0/2.0*W_k[k]*( (rho[i]+rho[i-1])*(D_k[i][k]+D_k[i-1][k])*tmp_fa[1+k][i])/(W[i]+W[i-1]);

	for(int i=1;i<num_faces-1;i++)
		flux[1+num_species][i] = 1.0/2.0*(lambda[i] +lambda[i-1])*tmp_fa[1+num_species][i];


	//for(int i=0;i<num_nodes-1;i++)
	//	tmp_src[i+1] = 4.0/3.0*std::pow((mu[i+1]*(tmp_fa[0][i] + tmp_fa[0][i+1])/2.0),2.0) - P[i+1]*(tmp_fa[0][i] + tmp_fa[0][i+1])/2.0;
	for(int i=0;i<num_nodes;i++){
		tmp_dudx_central = 0.5*(tmp_fa[0][i] + tmp_fa[0][i+1]);
		tmp_src[i] = 4.0/3.0*mu[i]*tmp_dudx_central*tmp_dudx_central - P[i]*tmp_dudx_central;
	}
		//tmp_src[i] = 4.0/3.0*std::pow((mu[i]*(tmp_fa[0][i-1] + tmp_fa[0][i])/2.0),2.0) - P[i]*(tmp_fa[0][i-1] + tmp_fa[0][i])/2.0;


}

//TODO: change from qn to something else later
void LaxFriedrichsFluxSplit::estimateNodeData(double **qn,double *e,double *u,double *rho,double *V_c,double **V_k,double **h_k,double **Yk) {


	double **tmp_node = this->getTmpNode();

	for(int i=0;i<num_nodes;i++)
		//tmp_node[0][i] = rho[i]*u[i];
		tmp_node[0][i] = qn[1][i];

	for(int i=0;i<num_nodes;i++)
		tmp_node[1][i] = qn[1][i]*u[i];
		//tmp_node[1][i] = tmp_node[0][i]*u[i];

	for(int k=0;k<num_species;k++)
		for(int i=0;i<num_nodes;i++)
			//tmp_node[2+k][i] = rho[i]*Yk[i][k]*(u[i]+V_c[i]);
			tmp_node[2+k][i] = qn[2+k][i]*(u[i]+V_c[i]);

	for(int i=0;i<num_nodes;i++)
		//tmp_node[2+num_species][i] = tmp_node[0][i]*e[i];
		tmp_node[2+num_species][i] = qn[2+num_species][i]*u[i];

	double local_sum = 0.0;
	for(int i=0;i<num_nodes;i++){
		local_sum = 0.0;
		for(int k=0;k<num_species;k++){
			local_sum += V_k[i][k]*h_k[i][k]*Yk[i][k];
		}
		tmp_node[2+num_species][i] += rho[i]*local_sum;
	}

}

void LaxFriedrichsFluxSplit::Discretize(Fluid *fluid,double **qn,double **rhs) {

	double *rho = fluid->getRho();
	double *u   = fluid->getU();
	double *P   = fluid->getP();
	double *T   = fluid->getT();
	double *e   = fluid->getE();
	double *c   = fluid->getC();

	IdealGas *gas = (IdealGas*)fluid;
	Mixture *mixobj = gas->getMixtureObj();
	double *W  = mixobj->getW();
	double *W_k  = mixobj->getW_k();
	double **X_k  = mixobj->getXk();
	double **Y_k  = mixobj->getYk();
	double **del_Xk_Xk  = mixobj->getDelXk_Xk();

	double *mu = mixobj->getMu();
	double *lambda = mixobj->getLambda();

	Thermo *thermobj = mixobj->getThermoObj();

	double **h_k = thermobj->geth_k();

	Transport *transpobj = mixobj->getTransportObj();

	double **D_k = transpobj->getDk();
	double *V_c = transpobj->getVc();
	double **V_k = transpobj->getVk();

	OneStepKinetics *kinetics = (OneStepKinetics *)mixobj->getKineticsObj();
	double **omega_k = kinetics->getOmegaK();

	//,double *u,double *P,double *T,double *W,double *W_k,double **Xk,double **del_Xk_Xk,doublmu,double *lambda,double **D_k
	double V_k_sum[num_nodes];

	for(int i=0;i<num_nodes;i++){
	    V_k_sum[i]=0.0;
		for(int k=0;k<num_species;k++)
			V_k_sum[i]+=V_k[i][k];

	}


	//TODO:fix this later
	double local_max = 0.0;
	double local_sum = 0.0;
	for(int i=0;i<num_nodes;i++){
		local_sum = abs(u[i]) + c[i] + abs(V_k_sum[i]);
			//local_sum = abs(u[i]) +abs(c[i]);
			//for(int k=0;k<num_species;k++)
			//	local_sum += abs(V_k[i][k]);
			if(local_sum>local_max) local_max = local_sum;
	}
	double alpha = local_max;

	estimateFaceFluxes(rho,u,P,T,W,W_k,X_k,del_Xk_Xk,mu,lambda,D_k);
	estimateNodeData(qn,e,u,rho,V_c,V_k,h_k,Y_k);
	formRHS(omega_k,qn,rhs,alpha);

}

void LaxFriedrichsFluxSplit::formRHS(double **omega_k,double **qn,double **rhs,double alpha){


	double **tmp_upwind = this->getTmpUpwind();
	double **tmp_downwind = this->getTmpDownwind();
	double one_over_dx = 1.0/dx;

	for(int k=0;k<num_species+3;k++)
		for(int i=0;i<num_nodes;i++)
			tmp_upwind[k][i] = 1.0/2.0*(tmp_node[k][i] + alpha*qn[k][i]);
	for(int k=0;k<num_species+3;k++)
		for(int i=0;i<num_nodes;i++)
			tmp_downwind[k][i] = 1.0/2.0*(tmp_node[k][i] - alpha*qn[k][i]);

	for(int k=0;k<num_species+3;k++)
		for(int i=1;i<num_nodes-1;i++)
			rhs[k][i]  = -one_over_dx*(tmp_upwind[k][i] - tmp_upwind[k][i-1]) - one_over_dx*(tmp_downwind[k][i+1] - tmp_downwind[k][i]);

	for(int k=1;k<num_species+3;k++)
		for(int i=1;i<num_nodes-1;i++)
			rhs[k][i]  += one_over_dx*(flux[k-1][i]-flux[k-1][i-1]);

	for(int k=0;k<num_species;k++)
		for(int i=1;i<num_nodes-1;i++)
			rhs[2+k][i]  += omega_k[i][k]*dx;

	for(int i=1;i<num_nodes-1;i++)
			rhs[2+num_species][i]  += tmp_src[i]*dx;


	for(int k=0;k<num_species+3;k++){
		rhs[k][0]  = rhs[k][1];
		rhs[k][num_nodes-1] = rhs[k][num_nodes-2];
	}



}

double** LaxFriedrichsFluxSplit::getFlux() const {
	return flux;
}

double** LaxFriedrichsFluxSplit::getTmpDownwind() const {
	return tmp_downwind;
}

double** LaxFriedrichsFluxSplit::getTmpFa() const {
	return tmp_fa;
}

double** LaxFriedrichsFluxSplit::getTmpNode() const {
	return tmp_node;
}

double* LaxFriedrichsFluxSplit::getTmpSrc() const {
	return tmp_src;
}

void LaxFriedrichsFluxSplit::estimateDelXkXk(double **Xk_ptr,double **DelXk_Xk_ptr) {




	for(int i=1;i<num_nodes-1;i++)
			for(int k=0;k<num_species;k++){
				DelXk_Xk_ptr[i][k]=(Xk_ptr[i+1][k]-Xk_ptr[i-1][k])/(2.0*dx*Xk_ptr[i][k]);

		}

	for(int k=0;k<num_species;k++){
		DelXk_Xk_ptr[0][k] = DelXk_Xk_ptr[1][k];
		DelXk_Xk_ptr[num_nodes-1][k] = DelXk_Xk_ptr[num_nodes-2][k];
	}

	for(int i=0;i<num_nodes;i++)
		for(int k=0;k<num_species;k++)
			if(Xk_ptr[i][k]<1.0e-8)
				DelXk_Xk_ptr[i][k]=0.0;

}

double** LaxFriedrichsFluxSplit::getTmpUpwind() const {
	return tmp_upwind;
}

}*/ /* namespace Flame */
