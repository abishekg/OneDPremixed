/*
 * NodeData.h
 *
 *  Created on: Jul 7, 2014
 *      Author: abishekg
 */

#ifndef NODEDATA_H_
#define NODEDATA_H_

namespace Flame {

class NodeData {
public:
	NodeData(int nk,int xi);
	virtual ~NodeData();
	int num_species;
	int x_index;
	double *Xk;
	double *Yk;
	double W;
	double T;
	double P;
	double u;
	double rho;
	double e;
	double Cp;

};

} /* namespace Flame */

#endif /* NODEDATA_H_ */
