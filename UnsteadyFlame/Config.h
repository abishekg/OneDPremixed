/*
 * Config.h
 *
 *  Created on: Aug 16, 2014
 *      Author: abishekg
 */

#ifndef CONFIG_H_
#define CONFIG_H_

#include <string>
#include <sstream>
#include <map>
#include <boost/any.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/filesystem/fstream.hpp>

using boost::any_cast;
typedef std::map<std::string, boost::any> custom_map;

using namespace std;
using boost::property_tree::ptree;
namespace Flame {

class Config {
public:
	Config();
	Config(string fileName);
	void writeSettingsFile(string fileName);
	//custom_map readSettingsFile();
	ptree readSettingsFile(string fileName);
	string getSettingsStr();
	virtual ~Config();

private:
	string configFile;
	custom_map map;
	ptree pt;

};

} /* namespace Flame */

#endif /* CONFIG_H_ */
