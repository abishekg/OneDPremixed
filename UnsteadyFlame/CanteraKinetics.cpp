/*
 *  CanteraKinetics.cpp
 *
 *  Created on: Jul 6, 2014
 *      Author: abishekg
 */

#include "CanteraKinetics.h"
#include "PhysicalConstants.h"
#include "cantera/kinetics/GasKinetics.h"
#include "cantera/transport/TransportParams.h"
#include <cmath>
#include <iostream>

using namespace Cantera;

namespace Flame {

CanteraKinetics::CanteraKinetics(int nx,CanteraWrapper *cantera_obj) {

	num_pts = nx;
	num_species = cantera_obj->getNumSpecies();

	cantera = cantera_obj;

	Xk = new double[num_species];
	Yk = new double[num_species];
	src = new double[num_species];

	omega_k  = new double*[num_pts];
	for(int i=0;i<num_pts;i++)
		omega_k[i] = new double[num_species];

}

CanteraKinetics::~CanteraKinetics() {

	for(int i=0;i<num_pts;i++)
		delete [] omega_k[i];
	delete omega_k;

	delete []Xk;

	delete []Yk;

	delete []src;

	std::cout<<"Deallocating Kinetics objects.."<<std::endl;

	// TODO Auto-generated destructor stub
}

void CanteraKinetics::getProductSourceTerm_PXkT(double *P_ptr,double **Xk_ptr,double *T_ptr,double *W_k){

	double T;
	double P;
	//double Xk[num_species];

	GasKinetics* gaskinetics = cantera->getGaskineticsObj();
	ThermoPhase* thermophase = cantera->getThermophaseObj();

	//double src[num_species]; //Not sure if correct

	for(int i=0;i<num_pts;i++){

		T = T_ptr[i];
		P = P_ptr[i];
		for(int k=0;k<num_species;k++)
			Xk[k] = Xk_ptr[i][k];


		thermophase->setState_TPX(T, P, Xk);
		//Species net production rates [kmol/m^3/s or kmol/m^2/s].
		gaskinetics->getNetProductionRates(src);
	    // convert kmol/m3-s to kg/m3-s
    	for(int k=0;k<num_species;k++)
    		omega_k[i][k]=1000.0*src[k]*W_k[k];
    		//omega_k[i][k]=1.0e6*kg*rate_coeff[k]*W_k[k];

	}
}

void CanteraKinetics::getProductSourceTerm_rhoYkT(double* rho_ptr, double** Yk_ptr,double* T_ptr, double* W_k) {

	double T;
	double rho;
	//double Yk[num_species];

	GasKinetics* gaskinetics = cantera->getGaskineticsObj();
	ThermoPhase* thermophase = cantera->getThermophaseObj();

	//double src[num_species];

	for(int i=0;i<num_pts;i++){

		/*T = T_ptr[i];
		rho = rho_ptr[i];
		for(int k=0;k<num_species;k++)
			Yk[k] = Yk_ptr[i][k];

		thermophase->setState_TRY(T, rho, Yk);*/

		thermophase->setState_TRY(T_ptr[i], rho_ptr[i], Yk_ptr[i]);

		//checks
		/*T = thermophase->temperature();
		rho = thermophase->density();
		thermophase->getMassFractions(&Yk[0]);*/

		//
		//setConcentrations(const doublereal* const conc)

		//Species net production rates [kmol/m^3/s or kmol/m^2/s].
		gaskinetics->getNetProductionRates(src);
		// convert kmol/m3-s to kg/m3-s
		for(int k=0;k<num_species;k++)
			omega_k[i][k]=1000.0*src[k]*W_k[k];
			//omega_k[i][k]=400.0*1000.0*src[k]*W_k[k];


	}


}

void CanteraKinetics::getProductSourceTerm_rhoYkTWP(double* rho, double** Yk,
		double* T, double* W_k, double* W, double* P) {


		//double Yk[num_species];

		GasKinetics* gaskinetics = cantera->getGaskineticsObj();
		ThermoPhase* thermophase = cantera->getThermophaseObj();

		//double src[num_species];

		for(int i=0;i<num_pts;i++){

			/*T = T_ptr[i];
			rho = rho_ptr[i];
			for(int k=0;k<num_species;k++)
				Yk[k] = Yk_ptr[i][k];

			thermophase->setState_TRY(T, rho, Yk);*/

			thermophase->setState_TRYWP(T[i], rho[i], Yk[i],W[i]*1000.0,P[i]);

			//checks
			/*T = thermophase->temperature();
			rho = thermophase->density();
			thermophase->getMassFractions(&Yk[0]);*/

			//
			//setConcentrations(const doublereal* const conc)

			//Species net production rates [kmol/m^3/s or kmol/m^2/s].
			gaskinetics->getNetProductionRates(src);
			// convert kmol/m3-s to kg/m3-s
			for(int k=0;k<num_species;k++)
				omega_k[i][k]=1000.0*src[k]*W_k[k];
				//omega_k[i][k]=400.0*1000.0*src[k]*W_k[k];


		}


}

} /* namespace Flame */
