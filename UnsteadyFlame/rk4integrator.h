#ifndef RK4INTEGRATOR_H
#define RK4INTEGRATOR_H
#include "integrator.h"

namespace Flame{

class RK4Integrator : public Integrator
{
public:
    RK4Integrator(int size_dim1,int size_dim2);
    ~RK4Integrator();
    //void RK4Integrator::advance(GenericSolver* solv_obj,double **qn,double **rhs,double dt);
    void advance(void* solv_obj,double **qn,double **rhs,double dt);

private:

    int num_dim1;
    int num_dim2;
    double tstep_coeffs[4];
    double **temp_qn0;
    double **temp;
    double  xdt1;
    double  xdt2;

    void advanceInternal(int rk_substep,double **qn,double **rhs);
};

}
#endif // RK4INTEGRATOR_H
