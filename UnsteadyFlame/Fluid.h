/*
 * Fluid.h
 *
 *  Created on: Jun 11, 2014
 *      Author: abishekg
 */

#ifndef FLUID_H_
#define FLUID_H_
#include "GridOperations.h"
namespace Flame {

class Fluid {
public:
	Fluid();
	virtual ~Fluid();
	double* getC() const;
	double* getE() const;
	double* getE_t() const;
	double* getP() const;
	double* getRho() const;
	double* getT() const;
	double* getU() const;

	virtual void Initialize() = 0;
	virtual void updateFluidProperties(GridOperations *grid_ops) = 0;

protected:

	double *rho;
	double *u;
	double *c;
	double *P;
	double *e;
	double *e_t;
	double *T;

};

} /* namespace Flame */

#endif /* FLUID_H_ */
