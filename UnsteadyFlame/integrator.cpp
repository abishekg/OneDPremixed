#include "integrator.h"

namespace Flame{



Integrator::Integrator()
{
}
Integrator::~Integrator()
{

}
double Integrator::getDel_t() const
{
    return del_t;
}

void Integrator::setDel_t(double value)
{
    del_t = value;
}
int Integrator::getNum_steps() const
{
    return num_steps;
}

void Integrator::setNum_steps(int value)
{
    num_steps = value;
}
int Integrator::getNum_elements() const
{
    return num_elements;
}

void Integrator::setNum_elements(int value)
{
    num_elements = value;
}




}
