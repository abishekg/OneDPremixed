/*
 * DataIO.h
 *
 *  Created on: Jun 18, 2014
 *      Author: abishekg
 */

#ifndef DATAIO_H_
#define DATAIO_H_
#include<vector>
#include<fstream>
#include<memory>
#include "ClassDefs.h"
//#include "Fluid.h"
#include "IdealGas.h"
#include "Grid.h"
using namespace std;

namespace Flame {

class DataIO {
public:
	DataIO();
	void openFileWrite(string fileName);
	void openAnimFileWrite(string fileName);
	void openRecordFileWrite(string fileName);
	//void writeVars(double t,double rho,);
	void RecordVarsTime(int num_record,int record_int, double* data);
	void writeFlameVarsX(string fileName,IdealGas *fluid_obj,Grid *grid_obj);
	void writeAnimVarsX(IdealGas *fluid_obj,Grid *grid_obj);
	void writeVarsX(IdealGas *fluid_obj,Grid *grid_obj);
	void writeVarsXDebug(IdealGas* fluid_obj, Grid* grid_obj);
	void writeResultFile(double sl, double fl_thick,double runtime,double soln_time, string str3);
	void closeFileWrite();
	void closeAnimFileWrite();
	void closeRecordFileWrite();

	virtual ~DataIO();

private:
	ifstream infile_ptr;
	ofstream ofile_ptr;
	ofstream animfile_ptr;
	ofstream recfile_ptr;
};

} /* namespace Flame */

#endif /* DATAIO_H_ */
