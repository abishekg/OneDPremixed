#ifndef SOLVER_H
#define SOLVER_H
#include "genericsolver.h"
#include "CanteraWrapper.h"
#include "FaceData.h"
#include <string>

#define ANIMATE_FLAME_VARS 1
#define RECORD_TIME_VARS 1
#define DYNAMIC_TIME_STEP 0

namespace Flame {

class Solver : public GenericSolver
{
public:
    Solver();
    Solver(string fileName,bool genSettings);
    virtual ~Solver();
    void Run();
	void Initialize();
	void updateVariables();
	void evalPreTimeAdvance();
	void evalPreSubstep();
	void evalPostSubstep();
	void Finalize();
	void evalRHS(int substep,double tstep_frac);
	int getNumSpecies() const;
	void setNumSpecies(int numSpecies);
	const string& getCtmlFile() const;
	void setCtmlFile(const string& ctmlFile);

	void setRecordInterval(int recordInterval);
	void setRefPressure(double P_inf);

private:

	double u_inf_in;
	double rho_inf_in;
	double T_inf_in;

	double P_imp_in;
	double rho_imp_in;
	double c_imp_in;
	double u_imp_in;
	double w1_imp_in;
	double w2_imp_in;
	double w3_imp_in;

	double P_in;
	double rho_in;
	double c_in;
	double u_in;


	double P_imp;
	double rho_imp;
	double c_imp;
	double u_imp;
	double w1_imp;
	double w2_imp;
	double w3_imp;

	double w3_out_int;

	double P_inf;
	double rho_inf;
	double c_inf;
	double u_inf;

	double rho_min;
	double rho_max;


	int record_interval;
	int num_record;
	int record_index;
    int num_species;
    int fc_ind;
    double fl_thickness;
    double x_fc, x_fc_prev;
    double u_fc, u_fc_prev;
    double u_err,u_err_prev;

    double u_add, u_add_prev;
    double *T_tmp;
    double *Yk_tmp;
    //double u_err;
    double u_err_prod;
    double w1_err_prod;
    double w2_err_prod;
    double w3_err_prod;
    double w3_init;
    double du_err_dt;

    double trans_time;
    double k_T;
    double k_P;
    double k_u;

    double T_fit_low;
    double T_fit_mid;
    double T_fit_high;

    double T_u_init;
    double T_b_init;

    double T_ad;

    double flame_init_pos;

    double phi;
    double ox_air_frac;
    double n_fuel_ox_st;
	string fuel_species;
	string ox_species;
	string mbody_species;

	string configFile;
    string ctml_file;
    string canteramechid;
    string FlameVarsFile;
	string RecordFile;
	string AnimFile;
    CanteraWrapper *cantera_obj;

    string settings_str;

    double *record_data;

    //NodeData *inlet,*outlet;
    FaceData *inlet,*outlet;


    double source_sr;
    double viscous_sr;
    double inviscid_sr;
    double viscous_sr_dx2[3];
    double inviscid_sr_dx;

    void recordVars();
    void getPrimitiveVars();
    void constructConservedVars();
    void InitializeArrays();
    void setInitialConditions();
    void setBoundaryConditions(double local_tstep);
    void estimateTimeStepSize();
    void estimateFlameParams(double tstep);
    void estimateFlameParams2(double tstep);
    void estimateFlameParams3(double tstep);
    void estimateFlameThickness();


};

}
#endif // SOLVER_H
