/*
 * EulerIntegrator.cpp
 *
 *  Created on: Jun 23, 2014
 *      Author: abishekg
 */

#include "EulerIntegrator.h"

#include "genericsolver.h"
#include "solver.h"

namespace Flame {


EulerIntegrator::EulerIntegrator(int size_dim1, int size_dim2) {
	num_dim1 = size_dim1;
	num_dim2 = size_dim2;
}

EulerIntegrator::~EulerIntegrator() {
	// TODO Auto-generated destructor stub
}

void EulerIntegrator::advance(void* solv_obj1, double** qn, double** rhs,
		double dt) {

	Solver *solv_obj;
	//TODO: generic solver or solver???
	solv_obj = reinterpret_cast<Solver*>(solv_obj1);

	setDel_t(dt);
	solv_obj->evalPreTimeAdvance();
	solv_obj->evalRHS(1,1.0);
	solv_obj->evalPostSubstep();

	for(int k=0;k<num_dim1;k++)
		for(int i=0;i<num_dim2;i++)
			qn[k][i]   = qn[k][i] + dt*rhs[k][i];

}

} /* namespace Flame */
