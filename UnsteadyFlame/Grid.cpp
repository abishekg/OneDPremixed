/*
 * Grid.cpp
 *
 *  Created on: Jun 17, 2014
 *      Author: abishekg
 */

#include "Grid.h"
#include<iostream>

namespace Flame {

Grid::Grid(int nx,double L,bool stretch,double flame_pos_frac,double refine_len_frac,double refine_pts_frac,double rl,double rr) {

	//num_faces = nx;
	//num_nodes = num_faces - 1;

	num_nodes = nx;
	num_faces = num_nodes + 1;
	this->L = L;

	x_face = new double[num_faces];
	x_node = new double[num_nodes];
	dx_ptr = new double[num_nodes];
	nodal_dx_ptr = new double[num_faces];
	facial_dx_ptr = new double[num_nodes];

	facial_interp_coeffs = new double[num_faces];

	if(stretch)
		generateStretchedGrid(flame_pos_frac,refine_len_frac,refine_pts_frac,rl,rr);
	else
		generateUniformGrid();


	//for(int i=0;i<num_faces;i++)
		//std::cout<<"i:"<<i<<" face_x:"<<std::scientific<<x_face[i]<<std::endl;


	for(int i=0;i<num_nodes;i++)
		x_node[i]=0.5*(x_face[i]+x_face[i+1]);

	for(int i=0;i<num_nodes;i++)
		dx_ptr[i]=x_face[i+1] - x_face[i];


	for(int i=0;i<num_nodes;i++)
		facial_dx_ptr[i] = 1.0/dx_ptr[i];


	for(int i=1;i<num_faces-1;i++)
		nodal_dx_ptr[i] = 1.0/(x_node[i]-x_node[i-1]);
	nodal_dx_ptr[0] = 0.5/x_node[0];
	nodal_dx_ptr[num_faces-1] = 0.5/(L-x_node[num_nodes-1]);


	for(int i=1;i<num_faces-1;i++){
		facial_interp_coeffs[i] = (x_face[i] - x_node[i-1])*nodal_dx_ptr[i];
		//std::cout<<"i:"<<i<<" face_coeff:"<<facial_interp_coeffs[i]<<std::endl;
	}


}

Grid::~Grid() {

	delete [] x_node;
	delete [] x_face;
	delete [] dx_ptr;
	delete [] nodal_dx_ptr;
	delete [] facial_dx_ptr;

}

double Grid::getDx() const {
	return dx;
}

void Grid::setDx(double dx) {
	this->dx = dx;
}

double Grid::getDxPe() const {
	return dx_PE;
}

void Grid::setDxPe(double dxPe) {
	dx_PE = dxPe;
}

int Grid::getNumFaces() const {
	return num_faces;
}

void Grid::setNumFaces(int numFaces) {
	num_faces = numFaces;
}

int Grid::getNumNodes() const {
	return num_nodes;
}

void Grid::setNumNodes(int numNodes) {
	num_nodes = numNodes;
}

double* Grid::getX() const {
	return x_node;
}

double* Grid::getXFaces() const {
	return x_face;
}

double Grid::getXPosition(int x_index) {
	return x_node[x_index];
}


int Grid::getXIndex(double x_position) {

	for(int i=0;i<num_nodes;i++)
		if( x_node[i]>x_position)
			return i-1;

	return -1;
}



double Grid::getL() const {
	return L;
}

void Grid::generateUniformGrid() {


	dx= L/(num_faces - 1);

	for(int i=0;i<num_faces;i++)
		x_face[i] = double(i)*dx;



}



void Grid::generateStretchedGrid(double flame_pos_frac,double refine_len_frac,double refine_pts_frac,double rl,double rr) {

	/*double refine_len_frac = 0.5;
	int    refine_pts_frac  = 0.85;
	double rl = 1.01;
	double rr = 1.02;*/


	//flame_pos_frac = 0.35;
	//refine_len_frac = 0.28;
	//refine_pts_frac  = 0.50;

	//for 1024 nodes
	//rl = 1.011190858794028;
	//rr = 1.004578965801975;

	/*//for 2048 nodes
	double rl = 1.005558164120065;
	double rr = 1.002295525069384;*/



	int N = num_faces;

	double Lx = refine_len_frac*L;

	double Lfp = flame_pos_frac*L;
	double Ll = (Lfp-Lx/2.0);
	//double Ll = (L-Lx)/2.0;
	double Lr = L - Lx - Ll;

	double length_ratio = Ll/(Ll+Lr);

	//int Nx = int(refine_pts_frac*double(N));
	//int Nl = int(length_ratio*double(N-Nx));
	//because matlab int32() rounds up while c++ int() doesn't
	int Nx = int(refine_pts_frac*double(N)+0.5);
	int Nl = int(length_ratio*double(N-Nx)+0.5);
	//int Nl = int((N-Nx)/2);
	int Nr = N - Nx - Nl;

	std::cout<<" Nx :"<<Nx<<" Nl :"<<Nl<<" Nr :"<<Nr<<std::endl;

	double dx_high = Lx/double(Nx-1);

	for(int i=Nl;i<Nl+Nx;i++)
		x_face[i] = Ll + double(i-Nl)*dx_high;

	double dxl = dx_high;

	//for(int i=0;i<Nl;i++)
	for(int i=Nl-1;i>0;i--){
		dxl = dxl*rl;
		x_face[i] = x_face[i+1] - dxl;
	}

	double dxr = dx_high;

	for(int i=Nl+Nx;i<N-1;i++){
		dxr = dxr*rr;
		x_face[i] = x_face[i-1] + dxr;
	}

	x_face[0] = 0.0;
	x_face[N-1] = L;



}


void Grid::generateMultiBlockGrid() {


	double Lx = 0.5*L;

	int N = num_faces;
	int Nx = int(0.85*N);

	double dx_high = Lx/double(Nx-1);

	int Nl = int((N-Nx)/2);
	int Nr = N - Nx - Nl;

	double Ll = (L-Lx)/2.0;
	double Lr = L - Lx - Ll;

	for(int i=Nl;i<Nl+Nx;i++)
		x_face[i] = Ll + double(i-Nl)*dx_high;

	double dxl = Ll/double(Nl);

	for(int i=0;i<Nl;i++)
		x_face[i] = 0.0 + double(i)*dxl;

	double dxr = Lr/double(Nr);

	for(int i=Nl+Nx;i<N;i++)
		x_face[i] = Ll+Lx + double(i-(Nl+Nx-1))*dxr;


}

double* Grid::getDxPtr() const {
	return dx_ptr;
}


double* Grid::getNodalDxPtr() const {
	return nodal_dx_ptr;
}


double* Grid::getFacialDxPtr() const {
	return facial_dx_ptr;
}

double* Grid::getFacialInterpCoeffs() const {
	return facial_interp_coeffs;
}

} /* namespace Flame */
