/*
 * Fluid.cpp
 *
 *  Created on: Jun 11, 2014
 *      Author: abishekg
 */

#include "Fluid.h"

namespace Flame {

Fluid::Fluid() {
	// TODO Auto-generated constructor stub

}

Fluid::~Fluid() {
	// TODO Auto-generated destructor stub
}

} /* namespace Flame */

double* Flame::Fluid::getC() const {
	return c;
}

double* Flame::Fluid::getE() const {
	return e;
}

double* Flame::Fluid::getE_t() const {
	return e_t;
}

double* Flame::Fluid::getP() const {
	return P;
}

double* Flame::Fluid::getRho() const {
	return rho;
}

double* Flame::Fluid::getT() const {
	return T;
}

double* Flame::Fluid::getU() const {
	return u;
}
