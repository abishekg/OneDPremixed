#include "rk4integrator.h"
#include <cstring>
#include "string.h"
#include "genericsolver.h"
#include "solver.h"
#include <iostream>
using namespace std;

namespace Flame{

RK4Integrator::RK4Integrator(int size_dim1,int size_dim2)
{

	num_dim1 = size_dim1;
	num_dim2 = size_dim2;

	xdt1 = 0.0;
	xdt2 = 0.0;

	/*tstep_coeffs[0] = 0.0;
	tstep_coeffs[1] = 0.5;
	tstep_coeffs[2] = 0.5;
	tstep_coeffs[3] = 1.0;*/

	tstep_coeffs[0] = 0.0;
	tstep_coeffs[1] = 0.5;
	tstep_coeffs[2] = 0.0;
	tstep_coeffs[3] = 0.5;



	temp_qn0 = new double*[size_dim1];
	for(int k=0;k<size_dim1;k++)
		  temp_qn0[k] = new double[size_dim2];

	temp = new double*[size_dim1];
	for(int k=0;k<size_dim1;k++)
		  temp[k] = new double[size_dim2];

	//temp_qn0 = new double[num_elements];
    //temp     = new double[num_elements];
}

RK4Integrator::~RK4Integrator()
{
	for(int k=0;k<num_dim1;k++)
		delete [] temp_qn0[k];
	delete temp_qn0;

	for(int k=0;k<num_dim1;k++)
		delete [] temp[k];
	delete temp;

	std::cout<<"Deallocating RK4 objects.."<<std::endl;

}

//void RK4Integrator::advance(GenericSolver* solv_obj,double **qn,double **rhs,double dt){
void RK4Integrator::advance(void* solv_obj1,double **qn,double **rhs,double dt){
//void RK4Integrator::advance(void* solv_obj1,double **qn,double **rhs,double dt){

	Solver *solv_obj;
    //TODO: generic solver or solver???
	//solv_obj = reinterpret_cast<Solver*>(solv_obj1);
	solv_obj = static_cast<Solver*>(solv_obj1);

    setDel_t(dt);

    advanceInternal(0,qn,rhs);
    solv_obj->evalPreTimeAdvance();
    for(int rk_step=1;rk_step<=4;rk_step++){
    	//solv_obj->evalRHS(rk_step,dt*tstep_coeffs[rk_step-1]);
    	solv_obj->evalPreSubstep();
    	solv_obj->evalRHS(rk_step,tstep_coeffs[rk_step-1]);
    	advanceInternal(rk_step,qn,rhs);
    	solv_obj->evalPostSubstep();
    }

}


void RK4Integrator::advanceInternal(int rk_substep,double **qn,double **rhs){

    switch(rk_substep){

    	case 0:

    		for(int k=0;k<num_dim1;k++)
    			for(int i=0;i<num_dim2;i++)
    				temp_qn0[k][i] = qn[k][i];

    		break;

         case 1:
            xdt1 = 1.0/2.0*del_t;
            xdt2 = 1.0/6.0*del_t;

    		for(int k=0;k<num_dim1;k++)
    			for(int i=0;i<num_dim2;i++)
    				qn[k][i]   = temp_qn0[k][i] + xdt1*rhs[k][i];

    		for(int k=0;k<num_dim1;k++)
				for(int i=0;i<num_dim2;i++)
					temp[k][i] = temp_qn0[k][i] + xdt2*rhs[k][i];

            break;

         case 2:
             xdt1 = 1.0/2.0*del_t;
             xdt2 = 1.0/3.0*del_t;

             for(int k=0;k<num_dim1;k++)
				for(int i=0;i<num_dim2;i++)
					qn[k][i]   = temp_qn0[k][i] + xdt1*rhs[k][i];

			 for(int k=0;k<num_dim1;k++)
				for(int i=0;i<num_dim2;i++)
					temp[k][i] = temp[k][i] + xdt2*rhs[k][i];

             break;

         case 3:
             xdt1 = 1.0*del_t;
             xdt2 = 1.0/3.0*del_t;

             for(int k=0;k<num_dim1;k++)
				for(int i=0;i<num_dim2;i++)
					qn[k][i]   = temp_qn0[k][i] + xdt1*rhs[k][i];

			 for(int k=0;k<num_dim1;k++)
				for(int i=0;i<num_dim2;i++)
					temp[k][i] = temp[k][i] + xdt2*rhs[k][i];
             break;

         case 4:
              xdt2 = 1.0/6.0*del_t;

              for(int k=0;k<num_dim1;k++)
 				for(int i=0;i<num_dim2;i++)
 					qn[k][i]   = temp[k][i] + xdt2*rhs[k][i];


    }

}

}
