/*
 * ClassDefs.h
 *
 *  Created on: Aug 13, 2014
 *      Author: abishekg
 */


#ifndef CLASSDEFS_H_
#define CLASSDEFS_H_

//#include "ClassDefsHead.h"

#include "IdealGas.h"
//#include "FiniteVolume.h"
#include "rk4integrator.h"
#include "CanteraKinetics.h"
#include "CanteraTransport.h"
//#include "EulerIntegrator.h"
//#include "LaxFriedrichsFluxSplit.h"
//#include "OneStepKinetics.h"

namespace Flame {

//typedef FiniteVolume NumericsClass;
typedef CanteraKinetics KineticsClass;
typedef CanteraTransport TransportClass;
typedef IdealGas FluidsClass;
typedef	RK4Integrator IntegratorClass;

}



#endif /* CLASSDEFS_H_ */
