#ifndef GENERICSOLVER_H
#define GENERICSOLVER_H
//#include "ClassDefs.h"
//#include "ClassDefs2.h"
#include "IdealGas.h"
#include "rk4integrator.h"
#include "FiniteVolume.h"
#include <ctime>

/*#include "EulerIntegrator.h"
#include "LaxFriedrichsFluxSplit.h"
#include "OneStepKinetics.h"*/

namespace Flame
{

class GenericSolver
{
public:
    GenericSolver();
    ~GenericSolver();
    
    virtual void Initialize() =0;
    virtual void Finalize() =0;
    virtual void InitializeArrays() =0;
    virtual void setInitialConditions() =0;
    virtual void setBoundaryConditions(double) =0;
    virtual void estimateTimeStepSize() = 0;
    virtual void Run() = 0;
    virtual void updateVariables() = 0;
    virtual void evalPreTimeAdvance() = 0;
    virtual void evalPreSubstep() = 0;
    virtual void evalPostSubstep() = 0;
    virtual void evalRHS(int substep,double tstep_frac) =0;

	int getNumPts() const;
	void setNumPts(int numPts);
	int getNumIter() const;
	void setNumIter(int numIter);
	double getSolnTime() const;
	void setSolnTime(double solnTime);
	double getTimeStep() const;
	void setTimeStep(double timeStep);
	double getDomainLength() const;
	void setDomainLength(double domainLength);
	double getGridSpacing() const;
	void setGridSpacing(double gridSpacing);
	int getElapsedIter() const;
	void setElapsedIter(int elapsedIter);

protected:
	std::clock_t start_time;
	double solver_runtime;
    double **qn;
    //double *qn_contiguous;
    double **rhs;

    int num_pts;
    int num_dim1;
    int num_dim2;
    int num_iter;
    int elapsed_iter;
    double soln_time;
    double max_sim_time;
    double time_step;
    double domain_length;
    double grid_spacing;
    // TODO: Only for IDE context - change back to Fluid.
    //FluidsClass *fluid;
    IdealGas *fluid; /* \todo: change back */
    // TODO: Only for IDE context - change back to Integrator.
    //IntegratorClass *integrator;
    RK4Integrator *integrator;
    //EulerIntegrator *integrator;
    // TODO: Only for IDE context - change back to Integrator.
    //NumericsClass *numerics;
    FiniteVolume *numerics;
    //LaxFriedrichsFluxSplit *numerics;

    Grid *grid_obj;
    GridOperations *grid_ops;

    // TODO: Only for IDE context - change back to Integrator.
   //Kinetics *kinetics;
   //OneStepKinetics *kinetics;

};


}
#endif // GENERICSOLVER_H
