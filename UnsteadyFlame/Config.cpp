/*
 * Config.cpp
 *
 *  Created on: Aug 16, 2014
 *      Author: abishekg
 */

#include "Config.h"
#include <iostream>

namespace fs = boost::filesystem;

using namespace std;


namespace Flame {


Config::Config() {

	configFile = "Flame.ini";

}


Config::Config(string fileName) {

	configFile = fileName;

}

void Config::writeSettingsFile(string fileName) {

		if(!fileName.empty()) configFile = fileName;


	   map["Solver.runName"] = string("new");
	   //map["Solver.nTimesteps"] = int(800000);
	   map["Solver.maxSimTime"] = double(0.002);
	   map["Solver.ctmlFile"] = string("h2o2.xml");
	   //map["Solver.canteraMechanismId"] = string("gas");
	   map["Solver.canteraMechanismId"] = string("ohmech");
	   map["Solver.FlameVarsFile"] = string("results/flamevars.dat");
	   map["Solver.RecordFile"] = string("results/records.bin");;
	   map["Solver.AnimFile"] = string("results/timehistory.dat");

	   map["Grid.domainLength"] = double(0.01);
	   map["Grid.numNodes"] = int(1024);
	   map["Grid.flameInitPos"] = double(0.25);
	   map["Grid.stretch"] = int(1);
	   map["Grid.flamePosLFrac"] = double(0.35);
	   map["Grid.refineLenFrac"] = double(0.28);
	   map["Grid.refinePtsFrac"] = double(0.50);
	   map["Grid.leftRefineRatio"] = double(1.011190858794028);  //1.005558164120065 for 2048
	   map["Grid.rightRefineRatio"] = double(1.004578965801975); //1.002295525069384 for 2048

	   //map["Grid."] = 1024;

	   //map["Chemistry.ctmlFile"] = string("h2o2.xml");
	   map["Chemistry.phi"] = double(1.0);
	   map["Chemistry.fuelSpecies"] = string("H2");
	   map["Chemistry.oxidizerSpecies"] = string("O2");
	   map["Chemistry.airSpecies"] = string("AR");
	   map["Chemistry.oxidizerAirFraction"] = double(0.21);
	   map["Chemistry.fuelOxFractionStoich"] = double(2.0);


	   map["Inlet.u"] = double(0.0);
	   map["Inlet.T"] = double(300.0);
	   map["Inlet.P"] = double(1.0e5);

	   map["Outlet.u"] = double(0.0);
	   map["Outlet.T"] = double(2000.0);
	   map["Outlet.P"] = double(1.0e5);

	   map["Transport.T_fit_low"] = double(200.0);
	   map["Transport.T_fit_mid"] = double(1000.0);
	   map["Transport.T_fit_high"] = double(3500.0);
	   //map["Transport.T_fit_high"] = double(5000.0);

	   /*map["BoundaryConditions.k_T"] = double(10.0);
	   map["BoundaryConditions.k_u"] = double(300.0);
	   map["BoundaryConditions.k_P"] = double(950.0);
	   map["BoundaryConditions.transTime"] = double(10.0);*/

	   map["BoundaryConditions.k_T"] = double(200.0);
	   map["BoundaryConditions.k_u"] = double(700.0);
	   map["BoundaryConditions.k_P"] = double(1550.0);



	 // int num_settings = map.size();


	  //map["a"] = 2.1;
	 // map["b"] = pc;
	 // map["c"] = value;


	  //cout << any_cast<double>(map["a"]) << endl;
	  //cout << any_cast<char*>(map["b"]) << endl;
	  //std::cout.precision(13);

	  //cout <<std::fixed<< any_cast<double>(map["c"]) << endl;


	//po::variables_map vm;
	//vm.insert()
	//vm.insert(std::make_pair("MyNewEmptyOption",);




	//   pt.put("a.path.to.float.value", 3.14f);
	//   pt.put("a.path.to.float.value", 2.72f);
	//   pt.add("a.path.to.float.value", 3.14f);

	  ptree pt;

	   //std::map<std::string, std::string>::iterator iter;
	   custom_map::iterator iter;
	   string key;
	   boost::any val;

	   //cout << "map contents" << endl;

	   for (iter = map.begin(); iter != map.end(); ++iter) {

		   key = iter->first;
		   val  = map[key];

		   if(val.type() == typeid(char *)){
			   char * strval = any_cast<char *>(val);
			   string st(strval);
			   pt.put(key,strval);
		   	   //cout<<"Type of "<<key<<" is: char* "<<st<<std::endl;
		   }
		   else if(val.type() == typeid(string)){
			   string strval = any_cast<string>(val);
			   pt.put(key,strval);
			  // cout<<"Type of "<<key<<" is: string "<<strval<<std::endl;
		   }
		   else if(val.type() == typeid(double)){
			   double dblval = any_cast<double>(val);
			   pt.put(key,dblval);
			  // cout<<"Type of "<<key<<" is: double "<<dblval<<std::endl;
		   }
		   else if(val.type() == typeid(int)){
			   int intval = any_cast<int>(val);
			   pt.put(key,intval);
			  // cout<<"Type of "<<key<<" is: int "<<intval<<std::endl;
		   }




		  // cout<<"Type of :"<<key<<" is: "<<val.type()<<std::endl;

		   //pt.put(key,any_cast<val.type()>(map[key]));

	   }






	 /*  pt.put("Solver.runName","new");
	   pt.put("Solver.nTimesteps",800000);
	   pt.put("Solver.ctmlFile","h2o2.xml");
	   pt.put("Solver.canteraMechanism","gas");
	   pt.put("Solver.FlameVarsFile","results/flamevars.dat");
	   pt.put("Solver.RecordFile","results/records.bin");
	   pt.put("Solver.AnimFile","results/timehistory.dat");

	   pt.put("Grid.domainLength",0.01f);
	   pt.put("Grid.numNodes",1024);
	   //pt.put("Grid.",1024);

	   pt.put("Chemistry.ctmlFile","h2o2.xml");
	   pt.put("Chemistry.phi",1.0f);
	   pt.put("Chemistry.fuelSpecies","H2");
	   pt.put("Chemistry.oxidizerSpecies","O2");
	   pt.put("Chemistry.airSpecies","AR");
	   pt.put("Chemistry.oxidizerAirFraction",0.21);
	   pt.put("Chemistry.fuelOxFractionStoich",2.0);


	   pt.put("Inlet.u",0.0f);
	   pt.put("Inlet.T",300.0f);
	   pt.put("Inlet.P",1.0e5);

	   pt.put("Outlet.u",0.0f);
	   pt.put("Outlet.T",2000.0f);
	   pt.put("Outlet.P",1.0e5);

	   pt.put("Transport.T_fit_low",200.0f);
	   pt.put("Transport.T_fit_mid",1000.0f);
	   pt.put("Transport.T_fit_high",5000.0f);

	   pt.put("BoundaryConditions.k_T",10.0);
	   pt.put("BoundaryConditions.k_u",10.0);
	   pt.put("BoundaryConditions.k_P",10.0);
	   pt.put("BoundaryConditions.transTime",10.0);*/

	   cout <<" Writing settings file: "<<configFile<< endl;

	   write_ini(configFile, pt );

}

ptree Config::readSettingsFile(string fileName) {


	if(!fileName.empty()) configFile = fileName;

	 cout <<" Reading settings file: "<<configFile<< endl;

	  //ptree pt;
	 pt.clear();
	  boost::property_tree::ini_parser::read_ini(configFile, pt);

	  return pt;


}

string Config::getSettingsStr() {

    stringstream ss;
	write_ini(ss, pt);

	return ss.str();

}

Config::~Config() {

}

} /* namespace Flame */
