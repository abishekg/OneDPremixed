/*
 * IdealGas.h
 *
 *  Created on: Jun 11, 2014
 *      Author: abishekg
 */

#ifndef IDEALGAS_H_
#define IDEALGAS_H_

#include "Fluid.h"
#include "Mixture.h"
#include "CanteraWrapper.h"
#include "FaceData.h"

namespace Flame {

class IdealGas: public Fluid {
public:
	IdealGas(int nx,int nk);
	IdealGas(int nx,CanteraWrapper *cantera);
	virtual ~IdealGas();


	void getTemperaturefromInternalEnergy();
	Mixture*& getMixtureObj();
	double* getT_Prev() const;
	void setT_Prev(double* prev);
	void getPresurefromDensityTemp();
	void getSpeedofSound();
	double getSpeedofSound(double T,double W,double Cp);
	void updateFluidProperties(GridOperations *grid_ops);
	void updateBoundaryProperties(GridOperations *grid_ops,FaceData *inlet,FaceData *outlet);
	void Initialize();
	void processTemperature(GridOperations *grid_ops);
	double EstimateInternalEnergyFromTYk(double T0,double *Yk0,double W0,double P0,double rho0);
	double EstimateEnthalpyInternalEnergyFromTYk(double T0,double *Yk0,double W0,double P0,double rho0,double *hk_out);
	double EstimateSpecificHeatFromTYk(double T0,double *Yk0);
	double EstimateTfromPRhoW(double P,double rho,double W);
	double EstimatePfromTRhoW(double T,double rho,double W);
	double EstimateRhofromPTW(double P,double T,double W);
	int getNumSpecies() const;
	double* getD2T_Dx2() const;
	double* getdT_Dx() const;

private:
	int num_species;
	int num_pts;
	Mixture *mixture_obj;
	double *R_sp;
	double *T_prev;

	double *dT_dx;
	double *d2T_dx2;

	double *revpoly;
	//double revpoly_coeffs[6];
	double *tmp1;

    int num_low_T;
	int num_high_T;
	int *low_T_indices;
	int *high_T_indices;

	void initializeArrays();

	static int loc_k;
	static double loc_sum;

	inline double e_T(double *arr,double T);
	inline double Cv_T(double **arr,double *y_k,double T);

};

} /* namespace Flame */

#endif /* IDEALGAS_H_ */
