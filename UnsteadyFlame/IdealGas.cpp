/*
 * IdealGas.cpp
 *
 *  Created on: Jun 11, 2014
 *      Author: abishekg
 */

#include "IdealGas.h"
#include "PhysicalConstants.h"
#include "ArrayUtils.h"
#include <numeric>
#include <cmath>
#include <iostream>
#include "macros.h"

namespace Flame {

IdealGas::IdealGas(int nx,int nk) {

	num_pts = nx;
	num_species = nk;

	initializeArrays();

	mixture_obj = new Mixture(nx,nk);
	mixture_obj->preWilkesRuleCompute();

	num_low_T = 0;
	num_high_T = 0;

    double *R_sp_ptr       =  this->getMixtureObj()->getR_sp();
    for(int k=0;k<num_species;k++)
    	R_sp[k] = R_sp_ptr[k];

}


IdealGas::IdealGas(int nx, CanteraWrapper* cantera) {
	num_pts = nx;
	num_species = cantera->getNumSpecies();

	initializeArrays();

	mixture_obj = new Mixture(nx,cantera);
	mixture_obj->preWilkesRuleCompute();

	num_low_T = 0;
	num_high_T = 0;

	double *R_sp_ptr   =  this->getMixtureObj()->getR_sp();
	for(int k=0;k<num_species;k++)
		R_sp[k] = R_sp_ptr[k];
}

void IdealGas::initializeArrays() {

	rho = new double[num_pts];
	u = new double[num_pts];
	c = new double[num_pts];
	P = new double[num_pts];
	e = new double[num_pts];
	e_t = new double[num_pts];
	T = new double[num_pts];
	T_prev = new double[num_pts];

	dT_dx = new double[num_pts];
	d2T_dx2 = new double[num_pts];

	low_T_indices = new int[num_pts];
	high_T_indices = new int[num_pts];

	R_sp = new double[num_species];

	revpoly = new double[6];
	//revpoly = &revpoly_coeffs[0];
	tmp1    = new double[num_species];
}


IdealGas::~IdealGas() {

	delete [] rho;
	delete [] u;
	delete [] c;
	delete [] P;
	delete [] e;
	delete [] e_t;
	delete [] T;
	delete [] T_prev;

	delete [] d2T_dx2;
	delete [] dT_dx;

	delete mixture_obj;

	delete [] low_T_indices;
	delete [] high_T_indices;
	delete [] R_sp;
	delete [] revpoly;
	delete [] tmp1;


	std::cout<<"Deallocating Ideal gas objects.."<<std::endl;
	// TODO Auto-generated destructor stub
}



Mixture*& IdealGas::getMixtureObj() {
	return mixture_obj;
}

void IdealGas::getTemperaturefromInternalEnergy() {

	double *P_ptr 		= getP();
	double *rho_ptr 	= getRho();
	double *T_ptr 		= getT();
	double *e_ptr 		= getE();
	double *Tprev_ptr   = getT_Prev();
	double **Yk_ptr      = this->getMixtureObj()->getYk();
	double **a_low_k_tr   = this->getMixtureObj()->getThermoObj()->getNasaPolyCoeffLowT_Tr();
	double **a_high_k_tr   = this->getMixtureObj()->getThermoObj()->getNasaPolyCoeffHighT_Tr();
	double **a_low_k   = this->getMixtureObj()->getThermoObj()->getNasaPolyCoeffLowT();
	double **a_high_k   = this->getMixtureObj()->getThermoObj()->getNasaPolyCoeffHighT();
	double *R_sp       =  this->getMixtureObj()->getR_sp();

	double et_val = 0.0;
	double cv_val = 0.0;

	for(int i=0;i<num_pts;i++)Tprev_ptr[i]=T_ptr[i];

	//double *revpoly = new double[6];
	//double *tmp1    = new double[num_species];
	//double err = 1,tol = 0.5;
	double err = 1,tol = 0.05;
	int rep = 0;

	double P0,rho0,e0;

	for(int i=0;i<num_pts;i++){

		err = 1;
		//tol = 0.1;
		//tol = 0.01;
		tol = temp_tol;
		rep = 0;

		//double T_safe = Tprev_ptr[i];
		double T_new  = Tprev_ptr[i];
		double T_prev = Tprev_ptr[i];

		P0 = P_ptr[i];
		rho0 = rho_ptr[i];
		e0   = e_ptr[i];

		double *yk0   = Yk_ptr[i];

		for(int k=0;k<num_species;k++)
			tmp1[k]=R_sp[k]*yk0[k];
		//double tmp1 = std::inner_product(R_sp,R_sp+num_species,yk0,0.0);

		double **polyarr ;// nullptr;
		double **coeff_arr ;// nullptr;
		if(T_prev <=1000.0){ // polyarr must be divided by coeffs
			polyarr = a_low_k_tr;
			coeff_arr = a_low_k;
		}else{
			polyarr = a_high_k_tr;
			coeff_arr = a_high_k;
		}

		for(int j=0;j<5;j++){
			revpoly[j] = inner_prod(num_species,tmp1,polyarr[4-j]);
			//revpoly[j] = std::inner_product(tmp1,tmp1+num_species,polyarr[4-j],0.0);
		}

		revpoly[5] = inner_prod(num_species,tmp1,polyarr[5]) - e0  - P0/rho0;
		//revpoly[5] = std::inner_product(tmp1,tmp1+num_species,polyarr[5],0.0) - e0  - P0/rho0;


		//revpoly[5] = tmp1[0]*polyarr[5][0] + tmp1[1]*polyarr[5][1] + tmp1[2]*polyarr[5][2] - e0  - P0/rho0;
		//revpoly[5] = tmp1[0]*a_low_k_tr[5][0] + tmp1[1]*a_low_k_tr[5][1] + tmp1[2]*a_low_k_tr[5][2] - e0  - P0/rho0;
		while(err > tol){
			//T_new = T_prev - e_T(revpoly,T_prev)/Cv_T(polyarr,yk0,T_prev);
			T_new = T_prev - e_T(revpoly,T_prev)/Cv_T(coeff_arr,yk0,T_prev);

			//err = abs(T_new-T_prev)/T_prev;
			err = std::abs(T_new-T_prev);

			T_prev = T_new;
			rep = rep + 1;
			//assert(rep<30, 'Temp lookup , Inf loop!! For e=%d , Yk=%d %d %d, T_prev= %d ,W0 = %d \n',e0,yk0(1),yk0(2),yk0(3),T_safe,W0);
		}

		T_ptr[i] = T_new;

	}

}

int IdealGas::getNumSpecies() const {
	return num_species;
}

double IdealGas::EstimateTfromPRhoW(double P, double rho, double W) {

	double R = PhysicalConstants::R_si;
	return P*W/(R*rho);
}

double IdealGas::EstimatePfromTRhoW(double T, double rho, double W) {

	double R = PhysicalConstants::R_si;
	return rho*R/W*T;

}

double IdealGas::EstimateRhofromPTW(double P, double T, double W) {
	double R = PhysicalConstants::R_si;
	return P*W/(R*T);
}

double* IdealGas::getD2T_Dx2() const {
	return d2T_dx2;
}

double* IdealGas::getdT_Dx() const {
	return dT_dx;
}

/*
inline double IdealGas::Cv_T(double** arr,double *y_k, double T) {

	double local_sum =0.0;
	for(int k=0;k<num_species;k++)
		local_sum += R_sp[k]*( arr[k][0] - 1.0 + T*(arr[k][1] + T*(arr[k][2] +
         T*(arr[k][3] +  T*arr[k][4]  ) ) ) )*y_k[k];

    return local_sum;

}

inline double IdealGas::e_T(double* arr, double T) {
	return arr[5] + T*(arr[4] +T*(arr[3] +T*(arr[2] + T*(arr[1] +T*arr[0]))));
	//arr[1]*(T)^5 + arr[2]*(T)^4  + arr[3]*(T)^3  + arr[4]*(T)^2  + arr[5]*(T) + arr[6];
}*/



inline double IdealGas::Cv_T(double** arr,double *y_k, double T) {

	double local_sum =0.0;
	for(int k=0;k<num_species;k++)
		local_sum += R_sp[k]*( arr[k][0] - 1.0 + T*(arr[k][1] + T*(arr[k][2] +
         T*(arr[k][3] +  T*arr[k][4]  ) ) ) )*y_k[k];

    return local_sum;

}

inline double IdealGas::e_T(double* arr, double T) {
	return arr[5] + T*(arr[4] +T*(arr[3] +T*(arr[2] + T*(arr[1] +T*arr[0]))));
	//arr[1]*(T)^5 + arr[2]*(T)^4  + arr[3]*(T)^3  + arr[4]*(T)^2  + arr[5]*(T) + arr[6];
}




double* IdealGas::getT_Prev() const {
	return T_prev;
}

void IdealGas::setT_Prev(double* prev) {
	T_prev = prev;
}

void IdealGas::getPresurefromDensityTemp() {

	double *P_ptr = getP();
	double *rho_ptr = getRho();
	double *T_ptr = getT();
	double *W_ptr = getMixtureObj()->getW();

	double R = PhysicalConstants::R_si;

	for(int i=0;i<num_pts;i++){

		//if(W_ptr[i]<1.e-4)std::cout<<"EXCEPTION!!!! : W_ptr is zero at.."<<i<<std::endl;

		P_ptr[i] = R*rho_ptr[i]*T_ptr[i]/W_ptr[i];
		ASSERT(std::isnan(P_ptr[i]));
		//if(std::isnan(P_ptr[i]))std::cout<<"EXCEPTION!!!! : Pressure is Nan at.."<<i<<std::endl;
	}

}

void IdealGas::getSpeedofSound() {

	double *c_ptr = getC();
	//double *rho_ptr = getRho();
	double *T_ptr = getT();
	double *W_ptr = getMixtureObj()->getW();
	double *Cp_m_ptr = getMixtureObj()->getCpMolar();

	double R = PhysicalConstants::R_si;
	double gamma;// = PhysicalConstants::gamma_air;

	for(int i=0;i<num_pts;i++){
		gamma = Cp_m_ptr[i]/(Cp_m_ptr[i] -R);
		c_ptr[i] = sqrt(gamma*R*T_ptr[i]/W_ptr[i]);
	}

	//std::cout<<"c1 :"<<c_ptr[0]<<" c2:"<<c_ptr[num_pts-1];
}

double IdealGas::getSpeedofSound(double T,double W,double Cp) {

	double R_sp = PhysicalConstants::R_si/W;
	double gamma_over_W = Cp/(Cp - R_sp);
	return sqrt(gamma_over_W*R_sp*T);

}


void IdealGas::updateFluidProperties(GridOperations *grid_ops) {

	processTemperature(grid_ops);
	//getMixtureObj()->updateMixtureProperties(grid_ops,num_low_T,num_high_T,low_T_indices,high_T_indices,T,rho);
	getMixtureObj()->updateMixtureProperties(grid_ops,num_low_T,num_high_T,low_T_indices,high_T_indices,T,rho,P);
	getSpeedofSound();
	//getMixtureObj()->updateMixtureChemistry(rho,T);
	getMixtureObj()->updateMixtureChemistry(rho,T,P);
	//getMixtureObj()->updateMixtureChemistry_PXk(P,T);

}

void IdealGas::updateBoundaryProperties(GridOperations *grid_ops,FaceData *inlet,FaceData *outlet) {

	getMixtureObj()->updateBoundaryProperties(grid_ops,inlet,outlet);
}

void IdealGas::Initialize() {

	mixture_obj->preWilkesRuleCompute();
}


void IdealGas::processTemperature(GridOperations *grid_ops) {


	num_low_T = 0;
	num_high_T = 0;
	for(int i=0;i<num_pts;i++)
		if(T[i]<1000.0){
			low_T_indices[num_low_T++]=i;
		}else{
			high_T_indices[num_high_T++]=i;
		}

	double *T_ptr = getT();
	//cout<<"T[0] isss "<<T_ptr[0]<<endl;
	//grid_ops->estimateDAoverDx(T_ptr,dT_dx);
	//grid_ops->estimateD2AoverDx2(T_ptr,d2T_dx2);

}



double IdealGas::EstimateInternalEnergyFromTYk(double T0,double *Yk0,double W0,double P0,double rho0){

    //double *h_k0 = new double[num_species];
	double h_k0[num_species];
    double **arr_k ;

	if(T0<1000.0)
		arr_k   = this->getMixtureObj()->getThermoObj()->getNasaPolyCoeffLowT();
	else
		arr_k   = this->getMixtureObj()->getThermoObj()->getNasaPolyCoeffHighT();

	for(int k=0;k<num_species;k++)
		h_k0[k] = R_sp[k]*T0*(arr_k[k][0] + T0*(arr_k[k][1]/2.0 + T0*(arr_k[k][2]/3.0 +
				   T0*(arr_k[k][3]/4.0 +  T0*arr_k[k][4]/5.0 ))) + arr_k[k][5]/T0 );

    //%e0 = h_k0'*Yk0 - R*T0/W0;
	//return ( inner_prod(num_species,&h_k0[0],Yk0) - P0/rho0);
    return ( std::inner_product(&h_k0[0],&h_k0[0]+num_species,Yk0,0.0) - P0/rho0);

}

double IdealGas::EstimateEnthalpyInternalEnergyFromTYk(double T0,double *Yk0,double W0,double P0,double rho0,double *hk_out){


    double **arr_k ;

	if(T0<1000.0)
		arr_k   = this->getMixtureObj()->getThermoObj()->getNasaPolyCoeffLowT();
	else
		arr_k   = this->getMixtureObj()->getThermoObj()->getNasaPolyCoeffHighT();

	for(int k=0;k<num_species;k++)
		hk_out[k] = R_sp[k]*T0*(arr_k[k][0] + T0*(arr_k[k][1]/2.0 + T0*(arr_k[k][2]/3.0 +
				   T0*(arr_k[k][3]/4.0 +  T0*arr_k[k][4]/5.0 ))) + arr_k[k][5]/T0 );

    //%e0 = h_k0'*Yk0 - R*T0/W0;
	//return ( inner_prod(num_species,hk_out,Yk0) - P0/rho0);
    return ( std::inner_product(hk_out,hk_out+num_species,Yk0,0.0) - P0/rho0);

}

double IdealGas::EstimateSpecificHeatFromTYk(double T0,double *Yk0){

	//double *Cp_k0 = new double[num_species];  //TODO: fix memory leak here
	double Cp_k0[num_species];
	double **arr_k ;

	if(T0<1000.0)
		arr_k   = this->getMixtureObj()->getThermoObj()->getNasaPolyCoeffLowT();
	else
		arr_k   = this->getMixtureObj()->getThermoObj()->getNasaPolyCoeffHighT();

	for(int k=0;k<num_species;k++)
        Cp_k0[k] = R_sp[k]*( arr_k[k][0] + T0*(arr_k[k][1] + T0*(arr_k[k][2]
                             + T0*(arr_k[k][3] +  T0*arr_k[k][4] ) ) ) );

	//return inner_prod(num_species,&Cp_k0[0],Yk0);
    return  std::inner_product(&Cp_k0[0],&Cp_k0[0]+num_species,Yk0,0.0);

}

} /* namespace Flame */
