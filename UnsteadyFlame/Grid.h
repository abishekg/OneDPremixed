/*
 * Grid.h
 *
 *  Created on: Jun 17, 2014
 *      Author: abishekg
 */

#ifndef GRID_H_
#define GRID_H_

namespace Flame {

class Grid {
public:
	Grid(int nx,double L,bool stretch,double flame_pos_frac,double refine_len_frac,double refine_pts_frac,double rl,double rr);
	virtual ~Grid();
	double getDx() const;
	void setDx(double dx);
	double getDxPe() const;
	void setDxPe(double dxPe);
	int getNumFaces() const;
	void setNumFaces(int numFaces);
	int getNumNodes() const;
	void setNumNodes(int numNodes);
	double* getX() const;
	double* getXFaces() const;
	double* getDxPtr() const;
	double* getNodalDxPtr() const;
	double* getFacialDxPtr() const;
	double* getFacialInterpCoeffs() const;
	double getXPosition(int x_index);
	int  getXIndex(double x_position);
	double getL() const;
	void generateUniformGrid();
	void generateStretchedGrid(double flame_pos_frac,double refine_len_frac,double refine_pts_frac,double rl,double rr);
	void generateMultiBlockGrid();


private:
	int num_faces;
	int num_nodes;
	double dx_PE;
	double dx;
	double *dx_ptr;
	double *nodal_dx_ptr;
	double *facial_dx_ptr;
	double *facial_interp_coeffs;
	double L;

	double *x_node;
	double *x_face;

};

} /* namespace Flame */

#endif /* GRID_H_ */
