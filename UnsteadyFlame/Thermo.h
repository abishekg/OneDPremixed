/*
 * Thermo.h
 *
 *  Created on: Jun 11, 2014
 *      Author: abishekg
 */

#ifndef THERMO_H_
#define THERMO_H_

#include "CanteraWrapper.h"

namespace Flame {

class Thermo {
public:
	Thermo(int nx,int nk);
	Thermo(int nx,CanteraWrapper *cantera_obj);
	virtual ~Thermo();

	void initializeArrays();
	void processNasaPoly();

	double** getCp_k() const;
	double** geth_k() const;
    void     seth_k(double** k);
	double** getS_k() const;
	double** gete_k() const;
	double** getNasaPolyCoeffHighT() const;
	double** getNasaPolyCoeffLowT() const;

	double** getNasaPolyCoeffHighT_Tr() const;
	double** getNasaPolyCoeffLowT_Tr() const;

	void getThermoProperties(int num_low_T,int num_high_T,int *low_T_indices,int *high_T_indices,double *T_ptr,double *R_sp);
	//void processTemperature();
	void initializeNasaPolynomials();
	void estimateSpeciesCpfromNasaPoly(int num_low_T,int num_high_T,int *low_T_indices,int *high_T_indices,double* T_ptr,double *R_sp);
	void estimateSpeciesEnthalpyfromNasaPoly(int num_low_T,int num_high_T,int *low_T_indices,int *high_T_indices,double* T_ptr,double *R_sp);


private:

	int num_pts;
	int num_species;

	double **a_low_k;
	double **a_high_k;

	double **a_low_k_tr;
	double **a_high_k_tr;

	double **Cp_k;
	double **h_k;
	double **S_k;
	double **e_k;

	double *T_prev;

	/*int  num_low_T;
	int  num_high_T;
	double *low_T_indices;
	double *high_T_indices;
	double **a_low_k;
	double **a_high_k;*/
};

} /* namespace Flame */

#endif /* THERMO_H_ */
