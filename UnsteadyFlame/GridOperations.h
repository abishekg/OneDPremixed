/*
 * GridOperations.h
 *
 *  Created on: Jun 17, 2014
 *      Author: abishekg
 */

#ifndef GRIDOPERATIONS_H_
#define GRIDOPERATIONS_H_
#include "Grid.h"
#include "FaceData.h"
namespace Flame {

class GridOperations {
public:
	GridOperations(Grid *grid);
	GridOperations(Grid *grid,FaceData *inlet,FaceData *outlet);
	virtual ~GridOperations();
	void estimateDelQoverQ(double** Q_ptr, double** DelQ_Q_ptr,int num_dim2);
	void estimateDelXkoverXk(double** Q_ptr, double** DelQ_Q_ptr,int num_dim2);
	void estimateDelXkoverXkBoundaries(double** Q_ptr, double** DelQ_Q_ptr,double *Q_inlet,double *Q_outlet,int num_dim2);
	void estimateDAoverDx(double *A,double  *DAoverDX);
	void estimateD2AoverDx2(const double* A, double* D2AoverDX2);
	void estimateFaceVars(double *Q,double *Q_face);
	double *getInterpCoeffs();
private:
	Grid* grid_obj;
	FaceData *inlet;
	FaceData *outlet;

	double *tmp_face;
	double *tmp_node;

};

} /* namespace Flame */

#endif /* GRIDOPERATIONS_H_ */
