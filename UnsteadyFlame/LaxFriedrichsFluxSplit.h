/*
 * LaxFriedrichsFluxSplit.h
 *
 *  Created on: Jun 14, 2014
 *      Author: abishekg
 */

#ifndef LAXFRIEDRICHSFLUXSPLIT_H_
#define LAXFRIEDRICHSFLUXSPLIT_H_
#include "Numerics.h"
#include "Fluid.h"

namespace Flame {

class LaxFriedrichsFluxSplit : public Numerics{
public:
	LaxFriedrichsFluxSplit(int nx,int nk,double dx);
	virtual ~LaxFriedrichsFluxSplit();

	//void estimateFaceFluxes();
	void estimateFaceFluxes(double *rho,double *u,double *P,double *T,double *W,double *W_k,double **Xk,double **del_Xk_Xk,double *mu,double *lambda,double **D_k);
	//void estimateNodeFluxes();
	void estimateNodeData(double **qn,double *e,double *u,double *rho,double *V_c,double **V_k,double **h_k,double **Yk);
	void formRHS(double **omega_k,double **qn,double **rhs,double alpha);
	void Discretize(Fluid*,double**qn,double**rhs);
	void estimateDelXkXk(double **Xk_ptr,double **DelXk_Xk_ptr);

	double** getFlux() const;
	double** getTmpDownwind() const;
	double** getTmpFa() const;
	double** getTmpNode() const;
	double* getTmpSrc() const;
	double** getTmpUpwind() const;

private:
	double **tmp_fa;
	double **tmp_node;
	double *tmp_src;
	double **tmp_upwind;
	double **tmp_downwind;

	double **flux;

	int num_faces;
	int num_nodes;
	int num_species;
	double dx_PE;
	double dx;



};

} /* namespace Flame */

#endif /* LAXFRIEDRICHSFLUXSPLIT_H_ */
