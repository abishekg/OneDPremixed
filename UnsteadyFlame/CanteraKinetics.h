/*
 * CanteraKinetics.h
 *
 *  Created on: Jul 6, 2014
 *      Author: abishekg
 */

#ifndef CANTERAKINETICS_H_
#define CANTERAKINETICS_H_

#include "Kinetics.h"
#include "CanteraWrapper.h"

namespace Flame {

class CanteraKinetics: public Kinetics {
public:
	CanteraKinetics(int nx,CanteraWrapper *cantera_obj);
	virtual ~CanteraKinetics();
	void getProductSourceTerm_PXkT(double *P,double **Xk,double *T,double *W_k);
	void getProductSourceTerm_rhoYkT(double *rho,double **Yk,double *T,double *W_k);
	void getProductSourceTerm_rhoYkTWP(double *rho,double **Yk,double *T,double *W_k,double *W,double *P);

private:
	int num_pts;
	int num_species;
	double *Xk;
	double *Yk;
	double *src;
	CanteraWrapper *cantera;

};

} /* namespace Flame */

#endif /* CANTERAKINETICS_H_ */
