/*
 * FaceData.h
 *
 *  Created on: Jul 17, 2014
 *      Author: abishekg
 */

#ifndef FACEDATA_H_
#define FACEDATA_H_

namespace Flame {

class FaceData {
public:
	FaceData(int nk,int xi);
	virtual ~FaceData();
	int num_species;
	int x_index;
	double *Xk;
	double *Yk;
	double *hk;
	double W;
	double T;
	double P;
	double u;
	double rho;
	double e;
	double Cp;
	double c;
	double w1;
	double w2;
	double w3;
	double lambda;
};

} /* namespace Flame */

#endif /* FACEDATA_H_ */
