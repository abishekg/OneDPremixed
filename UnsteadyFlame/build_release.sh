cmake -D CMAKE_BUILD_TYPE:STRING=Release .
make
if [ "$?" = "0" ]; then
   echo "Build successful" 1>&2
else
   echo "Errors during build. Halting script." 1>&2
   exit 1
fi
