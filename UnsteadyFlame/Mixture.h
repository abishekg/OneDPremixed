/*
 * Mixture.h
 *
 *  Created on: Jun 11, 2014
 *      Author: abishekg
 */

#ifndef MIXTURE_H_
#define MIXTURE_H_
//#include "ClassDefs.h"

#include "Thermo.h"
/*#include "Transport.h"
#include "Kinetics.h"*/
#include "CanteraWrapper.h"
#include "GridOperations.h"

#include "CanteraTransport.h"
#include "CanteraKinetics.h"
#include "FaceData.h"
namespace Flame {

//typedef TransportClass CanteraTransport;
//typedef KineticsClass CanteraKinetics;

class Mixture {
public:
	Mixture();
	Mixture(int npts,int nk);
	Mixture(int npts,CanteraWrapper *cantera);

	virtual ~Mixture();

	Thermo*& getThermoObj();
	CanteraTransport*& getTransportObj();
	CanteraKinetics*& getKineticsObj();

	double** getDelXk_Xk() const;
	double* getW() const;
	double* getWFace() const;
	double* getW_k() const;
	double** getXk() const;
	double** getXkFace() const;
	double** getYk() const;
	double** getYkFace() const;
	double* getOneOverWK() const;
	double* getLambda() const;
	double* getMu() const;
	double* getCp() const;
	void setCp(double* cp);
	double* getR_sp() const;
	double* getCpMolar() const;

	void initializeArrays();
	void updateMixtureProperties(GridOperations *grid_ops,double num_low_T,double num_high_T,int *low_T_indices,int *high_T_indices,double *T_ptr,double *rho_ptr,double *P_ptr);
	void updateBoundaryProperties(GridOperations *grid_ops,FaceData *inlet,FaceData *outlet);
	void updateMixtureChemistry(double *rho,double *T);
	void updateMixtureChemistry(double *rho,double *T,double *P);
	void updateMixtureChemistry_PXk(double *P,double *T);
	void preWilkesRuleCompute();
	void estimateMixtureMolecularWeight(GridOperations *grid_ops);
	void estimateYkFace();
	void estimateXkfromYk(GridOperations *grid_ops);
	void estimateMixtureSpecificHeats();
	void estimateMixtureViscosities();
	void estimateMixtureConductivities();
	int getNumSpecies() const;

private:

	int num_pts;
	int num_species;

	double **Yk;
	double **Yk_face;
	double **Xk;
	double **Xk_face;
	double *W_k;
	double *One_over_W_k;
	double *W;
	double *W_face;
	double **del_Xk_Xk;
	double *R_sp;
	double *Le_k;

	double *Cp;
	double *Cp_molar;
	double *mu;
	double *lambda;

	double **phi;
	double **phi_tmp1;
	double **phi_tmp2;
	double **Wk_k_over_j;
	double *phi_tmp3;

	FaceData *inlet;
	FaceData *outlet;

	Thermo *thermo_obj;
	CanteraTransport *transport_obj;
	CanteraKinetics *kinetics_obj;
};

} /* namespace Flame */

#endif /* MIXTURE_H_ */
