/*
 * ArrayUtils.h
 *
 *  Created on: Jun 10, 2014
 *      Author: abishekg
 */

#ifndef ARRAYUTILS_H_
#define ARRAYUTILS_H_
#include <iostream>
#include <iterator>


namespace Flame {

class ArrayUtils {
public:
	ArrayUtils();
	virtual ~ArrayUtils();

	void getContiguousArray(double** array_in,int num_dim1,int num_dim2,double* array_out);

	void get2DArray(double* array_in,int num_dim1,int num_dim2,double** array_out);

	//void printdoubleArray(double *ptr,int n);


};

inline void printdoubleArray(double* ptr, int n) {

	std::copy(ptr,ptr+n,std::ostream_iterator<double>(std::cout,","));
	std::cout <<std::endl;

}

inline double dot5(double * x, double * y)
		{
		    return x[0]*y[0] + x[1]*y[1] + x[2]*y[2] + x[3]*y[3] +
		           x[4]*y[4];
		}




inline double inner_prod(int n,double * x, double * y)
{
	double sum = 0.0;
	//double *x_fin = x+n;
	//int i=0;
	for(int i=0;i<n;++i)
		sum += x[i]*y[i];

	/*while(x<x_fin){
		sum += *(x) * *(y);
		x = x+1;
		y = y+1;
		i=i+1;
	}*/



	return sum;
}



inline double inner_3prod(int n,double * x, double * y,double *z)
{
	double sum = 0.0;
	for(int i=0;i<n;++i)
		sum += x[i]*y[i]*z[i];

	return sum;
}

inline double divide_each(int n,double * x, double * y)
{
	double sum = 0.0;
	for(int i=0;i<n;++i)
		sum += x[i]/y[i];
	return sum;
}


inline void divide_each(int n,double * x, double c)
{

	for(int i=0;i<n;++i)
		x[i] = x[i]/c;

}



inline double accumulate(int n,double * x)
{
	double sum = 0.0;
	for(int i=0;i<n;++i)
		sum += x[i];
	return sum;
}


inline double absmax(double *x,int n)
{

    double val;
    double maxval = 0.0;
    for (int i = 0; i < n; i++) {
        val = x[i];
        if (val < 0) {
            val = -val;
        }
        if (val > maxval) {
            maxval = val;
        }
    }
    return maxval;
}


inline double absmax_xplusy(double *x,double *y, int n)
{

    double val;
    double maxval = 0.0;
    for (int i = 0; i < n; i++) {
        val = x[i]+y[i];
        if (val < 0) {
            val = -val;
        }
        if (val > maxval) {
            maxval = val;
        }
    }
    return maxval;
}



inline double absmax_xminusy(double *x,double *y, int n)
{

    double val;
    double maxval = 0.0;
    for (int i = 0; i < n; i++) {
        val = x[i]-y[i];
        if (val < 0) {
            val = -val;
        }
        if (val > maxval) {
            maxval = val;
        }
    }
    return maxval;
}


} /* namespace Flame */

#endif /* ARRAYUTILS_H_ */
