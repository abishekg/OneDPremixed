/*
 * macros.h
 *
 *  Created on: Jun 23, 2014
 *      Author: abishekg
 */

#ifndef MACROS_H_
#define MACROS_H_

#include <iostream>

     #ifndef DEBUG
        #define ASSERT(x)
     #else
        #define ASSERT(x) \
                 if (! (x)) \
                { \
                   cout << "ERROR!! Assert " << #x << " failed\n"; \
                   cout << " on line " << __LINE__  << "\n"; \
                   cout << " in file " << __FILE__ << "\n";  \
                }
    #endif



#endif /* MACROS_H_ */
